<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Meta data api: http://g.e-hentai.org/api.php
// POST Body: {"method":"gdata","gidlist":[ID, token]}
// Header: ContentType: application/json
// ID and token: /892272/98bb276b81
/*
 * 
 * {
  "method": "gdata",
  "gidlist": [
  [
  968858,
  "2244bccdbd"
  ]
  ]
  }
 */

if (isset($_POST["token"]) && isset($_POST["id"])) {
    $folderName = "meta_cache";
    $fileName = $_POST["id"] . "_" . $_POST["token"] . ".json";
    $filePath = $folderName . "/" . $fileName;
    $result = file_get_contents($filePath, TRUE);
    if ($result == FALSE) {
        $result = httpPost("http://g.e-hentai.org/api.php", 
                array('Content-Type: application/json'), 
                array(
            "method" => "gdata",
            "gidlist" => array(array($_POST["id"], $_POST["token"]))
        ));
    }
    if (count($result)) {
        // Save to data folder.
        if (!file_exists($folderName)) {
            mkdir($folderName, 0777, TRUE);
        }
        file_put_contents($filePath, $result);

        echo $result;
    } else {
        header('HTTP/1.1 500 Internal Server Error');
        header('Content-Type: application/json; charset=UTF-8');
        die();
    }
}

function httpPost($url, $headers, $params) {
    $postData = json_encode($params);

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, false);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

    curl_setopt($ch, CURLOPT_POST, count($postData));
    curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);

    $output = curl_exec($ch);

    curl_close($ch);
    return $output;
}

function httpGet($url) {
    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//	curl_setopt($ch,CURLOPT_HEADER, false); 

    $output = curl_exec($ch);

    curl_close($ch);
    return $output;
}
