<?php
//allows user to add a new entry to the blacklist database, which will help the app to identify harmful contents
//every user is allow to make new entry, and moderators can perform tasks such as approve or deny user submitted entries

	//connect to DB
	include_once 'connect_db.php';
	
	//get contents outta POST request
	//every POST request should contains the following: threadID(mondatory), reason, content, image(optional)
	$name;
	$gid = 0;
	$sourceURL;
	$reasons;

	if (isset($_POST['name'])){
		$name = $_POST['name'];
	}
	if (isset($_POST['gid'])){
		$gid = $_POST['gid'];
	}
	if (isset($_POST['sourceURL'])){
		$sourceURL = $_POST['sourceURL'];
	}
	if (isset($_POST['reasons'])){
		$reasons = $_POST['reasons'];
	}
	$name = mysqli_real_escape_string($mysqli, $name);
	$gid = mysqli_real_escape_string($mysqli, $gid);
	$sourceURL = mysqli_real_escape_string($mysqli, $sourceURL);
	$reasons = mysqli_real_escape_string($mysqli, $reasons);
	$sql = "INSERT INTO exheti_blacklist (name, gid, sourceURL, reasons) VALUES ('$name', $gid, '$sourceURL', '$reasons')";
	$result;
	
	if ($name == null && $gid == null) {
		$result = false;
	} else {
		$result = mysqli_query($mysqli, $sql);
	}
	if ($result){
		//successed
		header("HTTP/1.1 200 OK");
	}
	else {
		//failed
		header("HTTP/1.1 400 ERROR");
	}
?>