<?php	
	include_once 'connect_db.php';

	class Blacklist {
		public $id;
		public $date;
		public $name;
		public $gid;
		public $sourceURL;
		public $reasons;
	}
	
	// Return a list of blacklist.
	$sql = "SELECT * 
			FROM  `exheti_blacklist` 
			ORDER BY  `exheti_blacklist`.`ID` DESC 
			LIMIT 0 , 100";
	$entities = mysqli_query($mysqli, $sql);
	$entityArray = array();
	while ($entity = mysqli_fetch_array($entities, MYSQLI_BOTH))
	{
		$blacklist = new Blacklist();
		$blacklist->id = intval($entity['ID']);
		$blacklist->date = $entity['date'];
		$blacklist->name = $entity['name'];
		$blacklist->gid = intval($entity['gid']);
		$blacklist->sourceURL = $entity['sourceURL'];
		$blacklist->reasons = explode(';', $entity['reasons']);
		// Add to array.
		array_push($entityArray, $blacklist);
	}

	echo json_encode(array_values($entityArray));
?>