//
//  Category.swift
//  Exellency
//
//  Created by Craig Zheng on 3/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

enum Category: String {
    case doujinshi = "doujinshi"
    case manga = "manga"
    case artistcg = "artistcg"
    case gamecg = "gamecg"
    case western = "western"
    case nonh = "non-h"
    case imageset = "imageset"
    case cosplay = "cosplay"
    case asianporn = "asianporn"
    case misc = "misc"
    
    init (rawValue: String) {
        switch (rawValue.lowercased()) {
        case "doujinshi":
            self = .doujinshi
        case "manga":
            self = .manga
        case "artistcg":
            self = .artistcg
        case "gamecg":
            self = .gamecg
        case "western":
            self = .western
        case "non-h":
            self = .nonh
        case "imageset":
            self = .imageset
        case "cosplay":
            self = .cosplay
        case "asianporn":
            self = .asianporn
        default:
            self = .misc
        }
    }
    
    static var allValues: [Category] {
        var categories = [Category]()
        if Configuration.singleton.debug {
            categories = Configuration.singleton.allowedCategory.map({categoryName -> Category in
                return Category(rawValue: categoryName)
            })
        } else {
            categories = [.nonh]
        }
        
        return categories
    }
    // Get the corresponding colour.
    func colour()->UIColor {
        var color = UIColor.darkText
        var R, G, B: Double
        switch (rawValue.lowercased()) {
        case "doujinshi":
            //255	37	37
            R = 255
            G = 37
            B = 37
        case "manga":
            // 255	184	55
            R = 255
            G = 184
            B = 55
        case "artistcg":
            // 232	216	37
            R = 232
            G = 216
            B = 37
        case "gamecg":
            // 37	146	37
            R = 37
            G = 146
            B = 37
        case "western":
            // 154	255	56
            R = 154
            G = 255
            B = 56
        case "non-h":
            // 56	172	255
            R = 56
            G = 172
            B = 255
        case "imageset":
            // 37	37	255
            R = 37
            G = 37
            B = 255
        case "cosplay":
            // 113	55	156
            R = 133
            G = 55
            B = 156
        case "asianporn":
            // 243	175	243
            R = 243
            G = 175
            B = 243
        default:
            // 215	215	215
            R = 215
            G = 215
            B = 215
        }
        color = UIColor(red: CGFloat(R / 255.0), green: CGFloat(G / 255.0), blue: CGFloat(B / 255.0), alpha: 1.0)
        return color
    }
}
