//
//  Search.swift
//  Exellency
//
//  Created by Craig Zheng on 26/02/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class Search: List {
    var keyword: String?

    override func updateWithPage(_ page: Int, completionHandler:@escaping (_ success: Bool, _ error: NSError?)->Void)->Void {
        // Keep a reference of the current books.
        var currentBooks = self.books
        if let keyword = self.keyword {
            self.updateWithKeyword(keyword, page: page) { (success, error) -> Void in
                // Add the newly downloaded books to the current books.
                currentBooks.append(contentsOf: self.books)
                // Assign it back to self.
                self.books = currentBooks
                completionHandler(success, error)
            }
        } else {
            completionHandler(false, nil)
        }
    }

    func updateWithKeyword(_ keyword: String, page: Int, completionHandler:@escaping (_ success: Bool, _ error: NSError?)->Void) {
        // Example http://g.e-hentai.org/?f_doujinshi=1&f_manga=1&f_artistcg=1&f_gamecg=1&f_western=1&f_non-h=1&f_imageset=1&f_cosplay=1&f_asianporn=1&f_misc=1&f_search=chinese&f_apply=Apply+Filter
        // TODO: a subclass of List class to handle searching separately.
        self.page = page
        self.keyword = keyword
        if (self.page <= 1) {
            self.page = 1;
        }
        ListParser.booksWithKeyword(keyword, page: self.page!) { (success, books, error) -> Void in
            if success && books != nil {
                self.books = books!
            }
            completionHandler(success, error)
        }
    }

}
