//
//  List.swift
//  Exellency
//
//  Created by Craig Zheng on 21/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func <= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l <= r
  default:
    return !(rhs < lhs)
  }
}


class List: NSObject {
    var name: String?
    var category = Category.misc
    var books = [Book]()
    var page: Int?
    
    func updateWithPage(_ page: Int, completionHandler:@escaping (_ success: Bool, _ error: NSError?)->Void)->Void {
        // Keep a reference of the current books.
        var currentBooks = self.books
        self.updateWithCategory(self.category, page: page) { (success, error) -> Void in
            // Add the newly downloaded books to the current books.
            currentBooks.append(contentsOf: self.books)
            // Assign it back to self.
            self.books = currentBooks
            completionHandler(success, error)
        }
    }
    
    func updateWithCategory(_ category: Category, page: Int, completionHandler:@escaping (_ success: Bool, _ error: NSError?)->Void) {
        self.category = category;
        self.page = page;
        // Page can never be smaller than 1.
        if (self.page <= 1) {
            self.page = 1;
        }
        ListParser.booksWithCategory(self.category, page: self.page!) { (success, books, error) -> Void in
            if success && books != nil {
                self.books = books!
            }
            completionHandler(success, error)
        }
    }
    
}
