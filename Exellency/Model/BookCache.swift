//
//  BookCache.swift
//  Exellency
//
//  Created by Craig on 21/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class BookCache: NSObject {

    static let DownloadedBooksFolderName = "DownloadedBooks"
    
    static let libraryFolderURL: URL? = {
        if let folderPath = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first {
            return URL(fileURLWithPath: folderPath)
        }
        return nil
    }()
    
    static let cacheFolderURL: URL? = {
        if let cacheFolderPath = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first {
            return URL(fileURLWithPath: cacheFolderPath)
        }
        return nil
    }()
    
    static let downloadedBooksFolderURL: URL? = {
        if let libraryFolderURL = libraryFolderURL {
            let cacheFolderURL = libraryFolderURL.appendingPathComponent(DownloadedBooksFolderName)
            // Exclude this URL from icloud backup to avoid excessive storage usage.
            do {
                try (cacheFolderURL as NSURL).setResourceValue(true, forKey: URLResourceKey.isExcludedFromBackupKey)
            } catch {
                DLog(error)
            }
            return cacheFolderURL
        }
        return nil
    }()
    
    class func fileNameForBook(_ book: Book)->String? {
        if var title = book.title {
            title = "\(book.gid)-\(title).json".addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed) ?? title
            return title
        }
        return nil
    }
    
    /// Destination folder for downloaded books
    class func downloadedFolderForBook(_ book: Book)->URL? {
        var destinationFolder: URL? = nil
        if let cacheFolderURL = self.downloadedBooksFolderURL {
            destinationFolder = cacheFolderURL
                .appendingPathComponent(folderNameForBook(book))
            
        }
        return destinationFolder
    }
    
    class func folderNameForBook(_ book: Book)->String {
        var cacheFolderName = (book.title != nil ? book.title! : book.titleJpn!)
        cacheFolderName = cacheFolderName.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlHostAllowed) ?? cacheFolderName
        return cacheFolderName
    }
    
    class func contentsOfDownloadedFolderForBook(_ book: Book, shouldSort: Bool) -> [URL]? {
        return contentsOfFolderURL(BookCache.downloadedFolderForBook(book), shouldSort: shouldSort)
    }
    
    fileprivate class func contentsOfFolderURL(_ folderURL: URL?, shouldSort: Bool) -> [URL]? {
        if let folderURL = folderURL {
            let fileManager = FileManager.default
            if var contentURLs = try? fileManager.contentsOfDirectory(at: folderURL, includingPropertiesForKeys: nil, options: .skipsSubdirectoryDescendants) {
                if shouldSort {
                    contentURLs.sort(by: { (url1, url2) -> Bool in
                        var isAscending = false
                        let path1 = url1.path
                        let path2 = url2.path
                        if let date1 = FileManager.getCreationDateForFile(atPath: path1),
                            let date2 = FileManager.getCreationDateForFile(atPath: path2) {
                            isAscending = date1.compare(date2) == .orderedAscending
                        }
                        return isAscending
                    })
                }
                return contentURLs
            }
        }
        return nil
    }
    
    class func createDownloadedFolderForBook(_ book: Book)->Bool {
        var folderCreated = false
        if let folderURL = downloadedFolderForBook(book) {
            folderCreated = createFolderAtURL(folderURL)
        }
        
        return folderCreated
    }
    
    fileprivate class func createFolderAtURL(_ url: URL) -> Bool {
        var folderCreated = false
        if !FileManager.default.fileExists(atPath: url.path) {
            do {
                try FileManager.default.createDirectory(at: url,
                                                                        withIntermediateDirectories: true,
                                                                        attributes: [FileAttributeKey.protectionKey.rawValue:FileProtectionType.none])
                folderCreated = true
            } catch {
                DLog(error)
            }
        }
        return folderCreated
    }
    
    class func removeCacheFolderForBook(_ book: Book) {
        if let downloadedFolderURL = downloadedFolderForBook(book) {
            _ = try? FileManager.default.removeItem(at: downloadedFolderURL)
        }
    }
}
