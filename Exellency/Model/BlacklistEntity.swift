//
//  BlacklistEntity.swift
//  Exellency
//
//  Created by Craig Zheng on 27/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

class BlacklistEntity {
    fileprivate struct EntityKey {
        static let name = "name"
        static let gid = "gid"
        static let sourceURL = "sourceURL"
        static let reasons = "reasons"
    }
    fileprivate let reasonsSeparator = ";"
    
    var name: String?
    var gid: Int?
    var sourceURL: URL?
    var reasons = [String]()
    // Convert to POST request compatible format.
    var postString: String {
        var postString = ""
        if let name = name {
            postString += "&name=\(name)"
        }
        if let gid = gid {
            postString += "&gid=\(gid)"
        }
        if let sourceURL = sourceURL {
            postString += "&sourceURL=\(sourceURL.absoluteString)"
        }
        if !reasons.isEmpty {
            let reasons = self.reasons.joined(separator: ";")
            postString += "&reasons=\(reasons)"
        }
        return postString
    }
    
    convenience init?(jsonString: String) {
        if let jsonData = jsonString.data(using: String.Encoding.utf8),
            let jsonDict = (try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)) as? Dictionary<String, AnyObject>
        {
            self.init()
            name = jsonDict[EntityKey.name] as? String ?? ""
            gid = (jsonDict[EntityKey.gid] as? NSNumber)?.intValue ?? nil
            if let urlString = jsonDict[EntityKey.sourceURL] as? String {
                sourceURL = URL(string: urlString)
            }
            // If the incoming json cannot be parsed as string array, set it to empty string array instead. 
            reasons = jsonDict[EntityKey.reasons] as? [String] ?? [String]()
        } else {
            return nil
        }
    }
    
    convenience init?(book: Book) {
        guard book.title?.isEmpty == false || book.gid > 0 else { return nil }
        self.init()
        name = book.title
        gid = book.gid
        sourceURL = book.sourceURL as URL?
    }
    
    func isEqualToBook(_ book:Book) -> Bool {
        if gid == book.gid || name == book.title {
            return true
        }
        return false
    }
}
