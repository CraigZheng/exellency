//
//  Page.swift
//  Exellency
//
//  Created by Craig Zheng on 12/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

enum PageType {
    case thumbnail, image
}

class Page: NSObject {
    var imageURL: URL?
    var thumbnailURL: URL? // TODO: get the thumbnail image.
    var thumbnail: UIImage?
    var thumbnailStyle: CGSize?
    var pageURL: URL? /// Page to this URL, which is a HTML file that might contain an image URL.
    
    override func isEqual(_ object: Any?) -> Bool {
        guard let object = object as? Page else { return false }
        return object.imageURL == imageURL && object.thumbnailURL == thumbnailURL && object.pageURL == pageURL
    }
}

extension Page: NSCopying {
    
    func copy(with zone: NSZone? = nil) -> Any {
        let page = Page()
        page.imageURL = imageURL
        page.thumbnailURL = thumbnailURL
        page.thumbnail = thumbnail?.copy() as? UIImage
        page.thumbnailStyle = thumbnailStyle
        page.pageURL = pageURL
        return page
    }
    
}

extension Page: Jsonable {
    
    static func jsonDecode(jsonDict: Dictionary<String, AnyObject>) -> Jsonable? {
        guard !jsonDict.isEmpty else { return nil }
        let page = Page()
        page.imageURL = (jsonDict["imageURL"] as? String).flatMap(URL.init(string:))
        page.thumbnailURL = (jsonDict["thumbnailURL"] as? String).flatMap(URL.init(string:))
        page.pageURL = (jsonDict["pageURL"] as? String).flatMap(URL.init(string:))
        return page
    }
    
    func jsonEncode() -> String? {
        var jsonDictionary = Dictionary<String, Any>()
        
        jsonDictionary["imageURL"] = imageURL?.absoluteString
        jsonDictionary["thumbnailURL"] = thumbnailURL?.absoluteString
        jsonDictionary["pageURL"] = pageURL?.absoluteString
        
        if !jsonDictionary.isEmpty,
            let jsonData = try? JSONSerialization.data(withJSONObject: jsonDictionary, options: .prettyPrinted),
            let jsonString = String(data: jsonData, encoding: .utf8)
        {
            return jsonString
        } else {
            return nil
        }
    }
    
}

extension Sequence where Iterator.Element == Page {
    
    static func jsonDecode(jsonString: String) -> [Page]? {
        guard let jsonData = jsonString.data(using: .utf8),
            let jsonArray = (try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? [String]
            else { return [] }
        let jsonDicts = jsonArray.enumerated().flatMap { _, string -> [String: AnyObject]? in
            guard let jsonData = string.data(using: .utf8) else { return nil }
            return (try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? [String: AnyObject]
        }
        let pages = jsonDicts.flatMap { jsonDict -> Page? in
            return Page.jsonDecode(jsonDict: jsonDict) as? Page
        }
        return pages
    }
    
    func jsonEncode() -> String? {
        var jsonArray = [String]()
        self.forEach { page in
            if let jsonString = page.jsonEncode() {
                jsonArray.append(jsonString)
            }
        }
        if jsonArray.count > 0,
            let jsonData = try? JSONSerialization.data(withJSONObject: jsonArray, options: .prettyPrinted)
        {
            return String(data: jsonData, encoding: .utf8)
        } else {
            return nil
        }
    }
    
}
