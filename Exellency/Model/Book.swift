//
//  Book.swift
//  Exellency
//
//  Created by Craig Zheng on 18/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


enum BookCachingCompleteness {
    case notStart, inProgress, notCompleted, completed
}

typealias PageUpdateCompletion = (_ success: Bool, _ error: NSError?)-> Void
typealias PageUpdateProgression = (_ success: Bool, _ current: Int, _ error: NSError?)-> Void

class Book: NSObject {
    var gid = 0
    var token: String?
    var archiverKey: String?
    var title: String?
    var titleJpn: String?
    var category: Category?
    var thumb: URL?
    var uploader: String?
    var posted: Date?
    var fileCount = 0
    var fileSize = 0
    var expunged = false
    var rating = 0.0
    var torrentCount = 0
    var tags: [String]?
    /// pages are updated with another URL call.
    var pages = [Page]()
    var comments = [Comment]()
    // Caching status.
    var cachingStatus: BookCachingCompleteness {
        var status = BookCachingCompleteness.notStart
        if BookDownloaderManager.isBookDownloading(self) {
            status = .inProgress
        } else if BookCacheManager.isBookDownloaded(self) {
            status = .completed
        } // TODO: determine NotCompleted situation.
        return status
    }
    var sourceURL: URL? {
        var sourceURL: URL?
        if let token = token,
            let baseURLString = Configuration.singleton.entityDetailsURL
        {
            sourceURL = URL(string:baseURLString.replacingOccurrences(of: Configuration.Key.gid, with: "\(gid)")
                .replacingOccurrences(of: Configuration.Key.token, with: token))
        }
        return sourceURL
    }
    // Page load status.
    var isLoadingCompleted = false
    // Original json string.
    var jsonString: String?
    
    // Mark: Private variables.
    fileprivate var pageLoadingSessionTask: URLSessionTask?
    fileprivate var stopUpdating = false
    
    fileprivate override init() {}
    init(jsonString: String) {
        if let jsonData = jsonString.data(using: String.Encoding.utf8),
        let rawDict = try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments) as? [String: AnyObject],
        var jsonDict = rawDict
        {
            // If the incoming dictionary is a bulk response rather than individual response, unpack it before parsing.
            if let tempDict = (jsonDict["gmetadata"] as? [[String: AnyObject]])?.first {
                jsonDict = tempDict
            }
            gid = (jsonDict["gid"] as? NSNumber)?.intValue ?? 0
            token = jsonDict["token"] as? String
            archiverKey = jsonDict["archiver_key"] as? String
            title = jsonDict["title"] as? String
            titleJpn = jsonDict["title_jpn"] as? String
            category = Category(rawValue: (jsonDict["category"] as? String) ?? "")
            thumb = URL(string: (jsonDict["thumb"] as? String)?.urlEscapedString() ?? "")
            uploader = jsonDict["uploader"] as? String
            posted = Date(timeIntervalSince1970: (jsonDict["posted"] as? NSString)?.doubleValue ?? 0)
            fileCount = (jsonDict["filecount"] as? NSString)?.integerValue ?? 0
            fileSize = (jsonDict["filesize"] as? NSNumber)?.intValue ?? 0
            expunged = (jsonDict["expunged"] as? Bool) ?? false
            rating = (jsonDict["rating"] as? NSString)?.doubleValue ?? 0
            torrentCount = (jsonDict["torrentcount"] as? NSString)?.integerValue ?? 0
            tags = jsonDict["tags"] as? [String]
            // Keep a record of the original json string.
            self.jsonString = jsonString
        }
    }
    
    // Stop the currently on going page loading.
    func stopUpdatingPages() {
        DLog("stopUpdatingPages")
        stopUpdating = true
        if let pageLoadingSessionTask = pageLoadingSessionTask, pageLoadingSessionTask.state == .running {
            DLog("PageLoadingSessionTask is active, cancelling...")
            pageLoadingSessionTask.cancel()
        }
    }
    
    func updatePages(_ completionHandler:PageUpdateCompletion?) {
        updatePages(nil, completionHandler: completionHandler)
    }
    
    func updatePages(_ progressionHandler: PageUpdateProgression?, completionHandler: PageUpdateCompletion?) {
        stopUpdating = false
        if pages.count >= fileCount {
            // If the parsed pages are already equal to the number of file count, call the completion handler immediately.
            self.isLoadingCompleted = true
            if let completionHandler = completionHandler {
                completionHandler(true, nil)
            }
        } else {
            // Add a bit of delay in between page loads.
            ThreadSleeper.sleepInBackgroundThread(3, completion: {
                    // Calculate the current page, and keep calling self.
                    self.updatePage(Int(self.pages.count / Configuration.singleton.imagePerPage) + 1) { (success, error) -> Void in
                        // A page has been updated, call the progression handler.
                        if let progressionHandler = progressionHandler {
                            progressionHandler(success,
                                               Int(self.pages.count / Configuration.singleton.imagePerPage),
                                               error)
                        }
                        if self.stopUpdating == false && success == true {
                            self.updatePages(progressionHandler,
                                completionHandler: completionHandler)
                        } else {
                            print("Should no longer proceed.")
                            completionHandler?(false, error)
                        }
                }
            })
        }
    }
    
    func updatePage(_ page: Int?, completionHandler:@escaping (_ success:Bool, _ error: NSError?)->Void) {
        if var pagesURLString = Configuration.singleton.entityDetailsURL?
            .replacingOccurrences(of: Configuration.Key.gid, with: "\(gid)")
            .replacingOccurrences(of: Configuration.Key.token, with: token ?? "")
            .urlEscapedString()
        {
            if page == nil || page == 1 {
                // Loading from the first page, remove all.
                pages.removeAll()
            } else {
                let p = page! - 1;
                pagesURLString = String(format: "%@?p=%ld", pagesURLString, p)
            }
            // Should be http://g.e-hentai.org/g/893415/a8c3907d31/
            // Page 2: http://g.e-hentai.org/g/898572/0ea0abfa25/?p=1
            DLog("Book - load with URL: \(pagesURLString)")
            pageLoadingSessionTask = EntityParser.pagesWithURL(URL(string: pagesURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!), completionHandler: { (success, pages, error) -> Void in
                // A page has been loaded, nil the reference.
                self.pageLoadingSessionTask = nil
                if (success) {
                    if pages?.count >= 1 {
                        self.pages += pages!
                        // Try to update the thumbnails.
                        self.updateThumbnail(completionHandler)
                    } else {
                        completionHandler(false, nil)
                    }
                } else {
                    completionHandler(success, error)
                }
            })
        } else {
            completionHandler(false, nil)
        }
    }
    
    func updateThumbnail(_ completionHandler:@escaping (_ success:Bool, _ error: NSError?)->Void) {
        var thumbnailURLs = Set<URL>()
        for page in pages {
            if let url = page.thumbnailURL {
                thumbnailURLs.insert(url as URL)
            }
        }
        let imageManager = SDWebImageManager.shared()
        var downloadedThumbnailImages = [UIImage]()
        var completedDownloads = 0
        var errors = [NSError]()
        for thumbnailURL in thumbnailURLs {
            DLog("Thumbnail URL: " + thumbnailURL.absoluteString)
            imageManager?.downloadImage(with: thumbnailURL,
                options: SDWebImageOptions.highPriority,
                progress: nil,
                completed: { (image, error, cacheType, success, imageURL) -> Void in
                    if success && image != nil {
                        downloadedThumbnailImages.append(image!)
                    } else {
                        downloadedThumbnailImages.append(UIImage()) // Empty image.
                    }
                    if error != nil {
                        errors.append(error as! NSError)
                    }
                    completedDownloads += 1
                    if completedDownloads >= thumbnailURLs.count {
                        // All thumbnails downloaded, time to associate them with each page.
                        self.associateThumbnailImagesToPage(downloadedThumbnailImages, pages: self.pages)
                        // Return the last error in the errors array. Maybe return more in the future.
                        completionHandler(errors.count == 0, errors.last)
                    }
            })
        }
    }
    
    func associateThumbnailImagesToPage(_ thumbnailImages: [UIImage], pages: [Page]) {
        // Since the image is downloaded with SDWebImageDownloader, I should be able to fine them in the cache.
        let imageCache = SDImageCache.shared()
        var anchorThumbnailURL: URL? = nil
        var thumbnailIndex = 0.0
        for page in pages {
            if let thumbnailURL = page.thumbnailURL {
                if anchorThumbnailURL != thumbnailURL as URL {
                    anchorThumbnailURL = thumbnailURL as URL
                    thumbnailIndex = 0
                }
                if let image = imageCache?.imageFromDiskCache(forKey: thumbnailURL.absoluteString) {
                    var thumbnailWith = Configuration.singleton.thumbnailWidth;
                    var thumbnailHeight = image.size.height
                    if page.thumbnailStyle?.width != 0 {
                        thumbnailWith = Double(page.thumbnailStyle!.width)
                    }
                    if page.thumbnailStyle?.height != 0 {
                        thumbnailHeight = CGFloat(page.thumbnailStyle!.height)
                    }
                    page.thumbnail = image.crop(CGRect(x: CGFloat(thumbnailIndex * thumbnailWith),
                        y: 0,
                        width: CGFloat(thumbnailWith),
                        height: thumbnailHeight))
                } else {
                    DLog("No image has been found for key \(thumbnailURL.absoluteString)")
                }
            }
            // Move to next index
            thumbnailIndex += 1
        }
    }
    
    func updateImageForPage(_ page: Page!, completionHandler: @escaping (_ success: Bool, _ error: NSError?)->Void) {
        if page.pageURL != nil {
            EntityParser.imageWithURL(page.pageURL, completionHandler: { (success, pages, error) -> Void in
                if (pages?.count == 1) {
                    page.imageURL = pages?.first?.imageURL
                }
                DLog("Image URL updated for page.")
                completionHandler(success, error)
            })
        } else {
            completionHandler(false, nil)
        }
    }
    
    func updateComments(_ completionHandler: @escaping (_ success: Bool, _ error: NSError?)->Void)->Void {
        if let commentURLString = Configuration.singleton.entityDetailsURL?
            .replacingOccurrences(of: Configuration.Key.gid, with: "\(gid)")
            .replacingOccurrences(of: Configuration.Key.token, with: token ?? ""),
            let url = URL(string: commentURLString) {
            EntityParser.commentsWithURL(url, completionHandler: { (success, comments, error) -> Void in
                if success, let comments = comments {
                    self.comments = comments
                    completionHandler(true, nil)
                } else {
                    completionHandler(false,  error)
                }
            })
        } else {
            completionHandler(false, nil)
        }
    }
    
    override var description: String {
        var description = super.description
        if let title = title {
            description = "\(description) - \(title) - \(gid) - \(hashValue)"
        }
        return description
    }
}

// MARK: NSCopying

extension Book: NSCopying {
    
    func copy(with zone: NSZone? = nil) -> Any {
        let book = Book()
        book.gid = gid
        book.token = token
        book.archiverKey = archiverKey
        book.title = title
        book.titleJpn = titleJpn
        book.category = category
        book.thumb = thumb
        book.uploader = uploader
        book.posted = posted
        book.fileCount = fileCount
        book.fileSize = fileSize
        book.expunged = expunged
        book.rating = rating
        book.torrentCount = torrentCount
        book.tags = tags
        book.pages = pages.flatMap{ $0.copy() as? Page }
        book.comments = comments
        book.isLoadingCompleted = isLoadingCompleted
        book.stopUpdating = stopUpdating
        book.jsonString = jsonString
        return book
    }
    
}

// MAKR: Hash and isEqual
extension Book {
    override var hashValue: Int {
        return gid + fileSize
    }
    
    override func isEqual(_ object: Any?) -> Bool {
        var equal = false
        if let object = object as? Book {
            if object.title == title && object.gid == gid && object.fileSize == fileSize {
                equal = true
            }
        }
        return equal
    }
}

extension UIImage {
    func crop(_ rect: CGRect) -> UIImage {
        if let imageRef = self.cgImage!.cropping(to: rect) {
            let image = UIImage(cgImage: imageRef, scale: self.scale, orientation: self.imageOrientation)
            return image
        }
        return self
    }
}
