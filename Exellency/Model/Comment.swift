//
//  Comment.swift
//  Exellency
//
//  Created by Craig on 11/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class Comment: NSObject {
    var poster: String?
    var date: Date?
    var commentContent: String?
}
