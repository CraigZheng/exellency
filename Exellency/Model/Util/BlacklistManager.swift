//
//  BlacklistManager.swift
//  Exellency
//
//  Created by Craig Zheng on 27/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

typealias BlacklistManagerCompletion = (_ success: Bool, _ error: NSError?)->()

class BlacklistManager: NSObject {

    static let singleton = BlacklistManager()
    static let updateInterval = 10 * 60 // Update every 10 minutes.
    // Private properties.
    fileprivate var updateTimer: Timer?
    fileprivate let sessionManager = AFHTTPSessionManager.sessionManager()
    
    var blacklistEntities = [BlacklistEntity]()
    
    override init() {
        super.init()
        // Update blacklist on launch.
        updateBlacklist(nil)
        startUpdateTimer()
    }
    
    func reportBook(_ book: Book, reasons: [String]?, completion: BlacklistManagerCompletion?) {
        if let blacklistEntity = BlacklistEntity(book: book),
            var entityData = blacklistEntity.postString.data(using: String.Encoding.utf8) {
            // If there're associated reasons.
            if let reasons = reasons, !reasons.isEmpty {
                blacklistEntity.reasons = reasons
                entityData = blacklistEntity.postString.data(using: String.Encoding.utf8) ?? entityData
            }
            let mutableRequest = NSMutableURLRequest(url: Configuration.singleton.reportBlacklistURL as URL)
            mutableRequest.httpMethod = "POST"
            mutableRequest.httpBody = entityData
            sessionManager.dataTask(with: mutableRequest as URLRequest) {
                (response, responseObject, error) in
                let response = response as? HTTPURLResponse
                completion?(response?.statusCode == 200, error as NSError?)
                }.resume()
        } else {
            completion?(false, nil)
        }
    }
    
    func updateBlacklist(_ completion: BlacklistManagerCompletion?) {
        sessionManager.dataTask(with: EXConfigureURLRequest(url: Configuration.singleton.downloadBlacklistURL) as URLRequest) { (response, responseObject, error) in
            if let responseObject = responseObject as? Data,
                let jsonObject = try? JSONSerialization.jsonObject(with: responseObject, options: .mutableContainers), error == nil
            {
                if let jsonObject = jsonObject as? [NSDictionary] {
                    self.blacklistEntities.removeAll()
                    for jsonDict in jsonObject {
                        if let jsonData = try? JSONSerialization.data(withJSONObject: jsonDict, options: .prettyPrinted),
                            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
                        {
                            if let blacklistEntity = BlacklistEntity(jsonString: jsonString) {
                                self.blacklistEntities.append(blacklistEntity)
                            }
                        }
                    }
                } else {
                    DLog("\(jsonObject)")
                }
            }
            let success = error == nil
            if success {
                DLog("Completed updateBlacklist. Currently has \(self.blacklistEntities.count) entities")
            }
            if let error = error {
                DLog(error)
            }
            completion?(success, error as NSError?)
        }.resume()
    }
    
    func containsBook(_ book: Book) -> Bool {
        var isContained = false
        for entity in blacklistEntities {
            if entity.isEqualToBook(book) {
                isContained = true
                break
            }
        }
        return isContained
    }
    
    func blacklistReasonsForBook(_ book: Book) -> [String]? {
        var reasons: [String]?
        for entity in blacklistEntities {
            if entity.isEqualToBook(book) {
                reasons = entity.reasons
                break
            }
        }
        return reasons
    }
}

// MARK: App life cycle handler.
extension BlacklistManager {
    
    fileprivate func startUpdateTimer() {
        stopUpdateTimer()
        updateTimer = Timer.scheduledTimer(timeInterval: TimeInterval(BlacklistManager.updateInterval),
                                                             target: BlockOperation(block: { 
                                                                self.updateBlacklist(nil)
                                                             }),
                                                             selector: #selector(BlockOperation.main),
                                                             userInfo: nil,
                                                             repeats: true)
    }
    
    fileprivate func stopUpdateTimer() {
        updateTimer?.invalidate()
    }
}
