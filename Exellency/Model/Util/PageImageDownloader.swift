//
//  PageImageDownloader.swift
//  Exellency
//
//  Created by Craig Zheng on 15/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

/*
The purpose of this image downloader class is to download an image to the destination URL. If destination URL does not exist, create it.
*/

import UIKit

class PageImageDownloader: NSObject {
    var cancellableOperation: URLSessionTask?
    
    fileprivate let sessionManager = AFHTTPSessionManager.sessionManager()
    
    fileprivate func downloadImageWithURL(_ targetURL: URL, saveToURL destinationFolderURL: URL, completionHandler:@escaping (_ success: Bool, _ savedFileURL: URL?, _ error: NSError?)->Void)->URLSessionTask? {
        let fileManager = FileManager.default
        // Construct the actual dataTask.
        let cancellableOperation = sessionManager.dataTask(with: EXURLRequest(url: targetURL) as URLRequest) { (response, responseObject, error) in
            var fileName = response.suggestedFilename ?? targetURL.lastPathComponent
            var success = false
            var savedFilePath: URL?
            var destinationPath = destinationFolderURL.appendingPathComponent(fileName).path
            if error == nil || (error as NSError?)?.code == NSURLErrorCancelled {
                if let responseObject = responseObject as? Data {
                    if fileManager.fileExists(atPath: destinationPath) {
                        DLog("Destination is occupied, rename the new file...")
                        if let pathExtension = (URL(string: fileName))?.pathExtension {
                            // Delete the extension.
                            fileName = fileName.replacingOccurrences(of: pathExtension, with: "")
                            // Rename the file, add the current date.
                            fileName = fileName + "\(Date().description(with: nil))"
                            // Add the extension back.
                            fileName = NSString(string: fileName).appendingPathExtension(pathExtension)!
                            // Redo the appending... to get the new destinationPath.
                            destinationPath = destinationFolderURL.appendingPathComponent(fileName).path
                            DLog("New destinationPath: \(destinationPath)")
                        }
                    }
                    if fileManager.createFile(atPath: destinationPath,
                                              contents: responseObject,
                                              attributes: [FileAttributeKey.protectionKey.rawValue: FileProtectionType.none]) {
                        DLog("Downloaded image save to path: \(destinationPath)")
                        savedFilePath = URL(fileURLWithPath: destinationPath)
                    }
                }
                success = true
            }
            completionHandler(success, savedFilePath, error as NSError?)
        }
        // Determine if a new image should be downloaded by checking the expectContentSize aganist the current file size.
        sessionManager.setDataTaskDidReceiveResponseBlock {(urlSession, dataTask, response) -> URLSession.ResponseDisposition in
            var sessionResponseDisposition = URLSession.ResponseDisposition.allow
            // Check existing file, if the file content is exactly the same, there is no need to download it again.
            let fileName = response.suggestedFilename ?? targetURL.lastPathComponent
            let destinationPath = destinationFolderURL.appendingPathComponent(fileName).path
            if fileManager.fileExists(atPath: destinationPath),
                let attributes = try? fileManager.attributesOfItem(atPath: destinationPath),
                let fileSize = attributes[FileAttributeKey.size] as? NSNumber {
                let expectedContentSize = response.expectedContentLength
                if fileSize.int64Value == expectedContentSize {
                    sessionResponseDisposition = .cancel
                }
            }
            return sessionResponseDisposition
        }
        cancellableOperation.resume()
        return cancellableOperation
    }
    
    func download(_ page: URL, withBook: Book, saveToURL: URL, completionHandler:@escaping (_ success: Bool, _ saveToURL: URL?, _ error: NSError?)->Void)->URLSessionTask? {
        var cancellableOperation: URLSessionTask?
        _ = EntityParser.imageWithURL(page, completionHandler: { (success, pages, error) -> Void in
            ThreadSleeper.sleepInBackgroundThread(4) {
                if let imageURL = pages?.first?.imageURL {
                    cancellableOperation = self.downloadImageWithURL(imageURL, saveToURL: saveToURL, completionHandler: completionHandler)
                } else {
                    completionHandler(false, nil, nil)
                }
            }
        })
        return cancellableOperation
    }
}

// MARK: Start - stop.
extension PageImageDownloader {
    func stop() {
        if let operation = cancellableOperation {
            operation.cancel()
        }
    }
}
