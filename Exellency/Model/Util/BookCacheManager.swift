//
//  BookCacheManager.swift
//  Exellency
//
//  Created by Craig Zheng on 9/05/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

class BookCacheManager: NSObject {
    static let infoJsonFileName = "info.json"
    static let sharedInstance = BookCacheManager()
    
    // Downloaded books.
    fileprivate var _downloadedBooks: NSMutableOrderedSet?
    var downloadedBooks: NSMutableOrderedSet {
        get {
            if _downloadedBooks == nil {
                // Search cache for books.
                if let downloadedFolderURL = BookCache.downloadedBooksFolderURL {
                    _downloadedBooks = booksFromFolder(contentFolderURL: downloadedFolderURL as URL, completedBooks: true)
                } else {
                    _downloadedBooks = NSMutableOrderedSet()
                }
                DLog("\(_downloadedBooks!.count) books found in cache!")
            }
            return _downloadedBooks!
        }
        set {
            _downloadedBooks = nil
        }
    }
    
    fileprivate func booksFromFolder(contentFolderURL bookCacheFolderURL: URL, completedBooks: Bool) -> NSMutableOrderedSet {
        let books = NSMutableOrderedSet()
        let fileManager = FileManager.default
        do {
            // For each folder, construct a book with the info.json file, and determine if its been fully downloaded.
            var cacheFolders = try fileManager.contentsOfDirectory(at: bookCacheFolderURL, includingPropertiesForKeys: nil, options: .skipsSubdirectoryDescendants)
            // Sort the cache folders based on creation date.
            cacheFolders.sort(by: { (url1, url2) -> Bool in
                var isDescend = false
                let path1 = url1.path
                let path2 = url2.path
                if let date1 = FileManager.getCreationDateForFile(atPath: path1),
                    let date2 = FileManager.getCreationDateForFile(atPath: path2) {
                    isDescend = date1.compare(date2) == .orderedDescending
                }
                return isDescend
                
            })
            for bookCacheFolder in cacheFolders {
                let jsonFile = bookCacheFolder.appendingPathComponent(BookCacheManager.infoJsonFileName)
                if let jsonString = try? String.init(contentsOf: jsonFile),
                    let completedJson = try? String(contentsOf: bookCacheFolder.appendingPathComponent(BookDownloader.infoJsonFileName)),
                    let jsonData = completedJson.data(using: .utf8),
                    let jsonStrings = (try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? [String]
                {
                    let book = Book(jsonString: jsonString)
                    if !completedBooks {
                        books.add(book)
                    } else if jsonStrings.count >= book.fileCount {
                        books.add(book)
                    }
                }
            }
        } catch {
            DLog(error)
        }
        return books
    }
    
    func reset() {
        _downloadedBooks = nil
    }
}

// MARK: Class methods.
extension BookCacheManager {
    // Check file system to see if the book has been downloaded.
    class func isBookDownloaded(_ book: Book)->Bool {
        var downloaded = false
        // Check if the download folder already contains the book and all its contents.
        for downloadedBook in BookCacheManager.sharedInstance.downloadedBooks {
            if (downloadedBook as AnyObject).title == book.title {
                downloaded = true
                break
            }
        }
        return downloaded
    }
    
    /**
     Loop through any folder that is not completely downloaded, and construct a book object with the info.json file in the folder.
     */
    class func partiallyDownloadedBooks()->NSMutableOrderedSet {
        let partiallyDownloadedBooks = NSMutableOrderedSet()
        let fileManager = FileManager.default
        if let bookCacheFolderURL = BookCache.downloadedBooksFolderURL,
            var cacheFolders = try? fileManager.contentsOfDirectory(at: bookCacheFolderURL as URL,
                                                                    includingPropertiesForKeys: nil,
                                                                    options: .skipsSubdirectoryDescendants) {
            cacheFolders.sort(by: { (url1, url2) -> Bool in
                var isDescend = false
                do {
                    let path1 = url1.path
                    let path2 = url2.path
                    if let date1 = try fileManager.attributesOfItem(atPath: path1)[FileAttributeKey.creationDate] as? Date,
                        let date2 = try fileManager.attributesOfItem(atPath: path2)[FileAttributeKey.creationDate] as? Date {
                        isDescend = date1.compare(date2) == .orderedDescending
                    }
                } catch {
                    DLog(error)
                }
                return isDescend
                
            })
            cacheFolders.forEach({ (cacheFolder) in
                let jsonFileURL = cacheFolder.appendingPathComponent(infoJsonFileName)
                let completedFileURL = cacheFolder.appendingPathComponent(BookDownloader.infoJsonFileName)
                if let jsonString = try? String.init(contentsOf:jsonFileURL),
                    let completedJson = try? String(contentsOf: completedFileURL),
                    let completedPages = [Page].jsonDecode(jsonString: completedJson)
                {
                    // Construct a book with the info json file inside the given folder.
                    let book = Book(jsonString: jsonString)
                    if completedPages.count < book.fileCount {
                        partiallyDownloadedBooks.add(book)
                    }
                }
            })
        }
        return partiallyDownloadedBooks
    }
    
    class func reset() {
        BookCacheManager.sharedInstance.reset()
    }
    
    class func removeCacheFolderForBook(_ book: Book) {
        BookCache.removeCacheFolderForBook(book)
    }
}
