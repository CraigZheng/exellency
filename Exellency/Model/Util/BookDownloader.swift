//
//  BookDownloader.swift
//  Exellency
//
//  Created by Craig Zheng on 13/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

protocol BookDownloaderDelegate {
    func bookDownloader(_ downloader: BookDownloader, bookUpdated:Book, percentage:Double)
    func bookDownloader(_ downloader: BookDownloader, bookCompleted:Book, status: DownloaderStatus, error: NSError?)
}

enum DownloaderStatus {
    case notStart, paused, updating, downloading, completed, failed
}

fileprivate typealias PageDownloadCompletion = (_ success: Bool, _ savedPage: Page, _ savedFileURL: URL?, _ error: NSError?) -> Void

class BookDownloader: NSObject {
    static let errorThreshold = 10
    static let infoJsonFileName = "completedPageURLs.json"
    
    let sessionManager = AFHTTPSessionManager.sessionManager()
    let fileManager = FileManager.default
    
    var downloadingTask: PageImageDownloader?
    var downloadErrorCount = 0
    /// Book being downloaded.
    var downloadingBook: Book?
    var downloadingCacheFolder: URL? {
        get {
            return downloadingBook.flatMap({ BookCache.downloadedFolderForBook($0) })
        }
    }
    var destinationFolder: URL? {
        if let downloadingBook = downloadingBook {
            return BookCache.downloadedFolderForBook(downloadingBook)
        }
        return nil
    }
    var backgroundTaskIdentifier: UIBackgroundTaskIdentifier?
    var pendingPages: [Page]?
    lazy var completedPages: [Page] = {
        var pages = [Page]()
        if let completedPageFileURL = self.downloadingCacheFolder?.appendingPathComponent(BookDownloader.infoJsonFileName),
            self.fileManager.fileExists(atPath: completedPageFileURL.path),
            let completedJson = try? String(contentsOf: completedPageFileURL),
            let completedPages = [Page].jsonDecode(jsonString: completedJson)
        {
            pages += completedPages
        }
        return pages
    } ()
    var totalPage: Int {
        return downloadingBook?.pages.count ?? 0
    }
    // Others
    var delegate: BookDownloaderDelegate?
    var status = DownloaderStatus.notStart
    
    // Add a book to downloading.
    func downloadBook(_ book: Book)->Bool {
        if self.downloadingBook != nil {
            DLog("\(String(describing: downloadingBook?.title!)) is currently being downloaded!")
            // If this downloader is already busying with an active download, don't accept anymore download.
            return false
        }
        
        var addedToDownload = false
        self.downloadingBook = book
        if book.pages.count < book.fileCount {
            DLog("\(book.title!) page info incompleted, need to update its page first!")
            status = .updating
            book.updatePages({ [weak self] (success, error) in
                guard let strongSelf = self else { return }
                if success {
                    addedToDownload = strongSelf.downloadAllPagesForBook(strongSelf.downloadingBook!,
                                                                         toFolder: strongSelf.downloadingCacheFolder!)
                } else {
                    addedToDownload = false
                }
            })
            addedToDownload = true
        } else {
            if let downloadingCacheFolder = downloadingCacheFolder {
                addedToDownload = downloadAllPagesForBook(book, toFolder: downloadingCacheFolder)
            } else {
                DLog("downloading folder does not exist")
            }
        }
        // Start background task.
        backgroundTaskIdentifier = UIApplication.shared.beginBackgroundTask (expirationHandler: { [weak self] in
            self?.stop()
            // FIXME: downloading interrupt, need to experiment with the possibility of adding the current book to PendingDownloadTask.
            PendingDownloadManager.singleton.addBookToPending(book)
            self?.backgroundTaskIdentifier = UIBackgroundTaskInvalid
        })
        return addedToDownload
    }
    
    // Download all pages.
    fileprivate func downloadAllPagesForBook(_ book: Book, toFolder downloadingCacheFolder: URL)->Bool {
        if book.pages.count <= 0 {
            return false
        }
        // If necessary, create the downloading folder.
        let downloadingCacheFolderPath = downloadingCacheFolder.path
        if !downloadingCacheFolderPath.isEmpty {
            if !fileManager.fileExists(atPath: downloadingCacheFolderPath) {
                // Create the folder here.
                _ = BookCache.createDownloadedFolderForBook(book)
            }
        }
        status = .downloading
        pendingPages = book.pages
        completedPages.forEach ({ completedPage in
            if let index = pendingPages?.index(where: { pendingPage -> Bool in
                return pendingPage.pageURL == completedPage.pageURL
            }) {
                pendingPages?.remove(at: index)
            }
        })
        // Write the original json from the book to disk.
        if let jsonString = book.jsonString,
            let jsonFileURL = self.downloadingCacheFolder?.appendingPathComponent("info.json") {
            _ = try? jsonString.write(to: jsonFileURL, atomically: false, encoding: String.Encoding.utf8)
        }
        // Initiate the first download, all sequential download would be handled by self.pageDownloadCompleted method.
        downloadErrorCount = 0
        if let page = pendingPages?.first {
            download(page,
                     toFolder: downloadingCacheFolder,
                     completionHandler: {[weak self] (success, savedPage, savedFileURL, error) in
                        self?.pageDownloadCompleted(success, downloadedPage: savedPage, error: error)
            })
        }
        return true
    }
    
    fileprivate func pageDownloadCompleted(_ success:Bool, downloadedPage: Page, error: NSError?) {
        if success,
            let book = self.downloadingBook,
            let downloadingCacheFolder = self.downloadingCacheFolder {
            // The downloading task has completed.
            downloadingTask = nil
            // Notify delegate that a download has completed.
            completedPages.append(downloadedPage)
            recordCompletedPage()
            delegate?.bookDownloader(self,
                                     bookUpdated: book,
                                     percentage: Double(completedPages.count) / Double(book.pages.count))
            pendingPages?.removeFirst()
            // If the downloadedPage is not the last, go on and download the next page. Otherwise inform delegate about the completion of downloading.
            if let page = pendingPages?.first {
                if status != .paused {
                    // Download the next page.
                    // Sleep for this random seconds before loading next page, this is to ensure not to put too much pressure on the server.
                    ThreadSleeper.sleepInBackgroundThread(ThreadSleeper.randomSeconds(6),
                                                          completion: { [weak self] in
                                                            self?.download(page,
                                                                           toFolder: downloadingCacheFolder,
                                                                           completionHandler:
                                                                {[weak self] (success: Bool, savedPage: Page, savedFileURL: URL?, error: NSError?) -> Void in
                                                                    self?.pageDownloadCompleted(success,
                                                                                                downloadedPage: savedPage,
                                                                                                error: error)
                                                            })
                    })
                }
            } else {
                // All pages are downloaded.
                DLog("All download completed")
                status = .completed
                delegate?.bookDownloader(self,
                                         bookCompleted: book,
                                         status: status,
                                         error: nil)
                terminateBackgroundTask()
            }
        } else {
            DLog("Download of page: \(String(describing: downloadedPage)) failed! - \(String(describing: error))")
            if let downloadingCacheFolder = downloadingCacheFolder,
                downloadErrorCount < BookDownloader.errorThreshold,
                let page = pendingPages?.first
            {
                DLog("Retrying...")
                downloadErrorCount += 1
                download(page,
                         toFolder: downloadingCacheFolder,
                         completionHandler: { [weak self] (success, savedPage, savedFileURL, error) in
                            self?.pageDownloadCompleted(success,
                                                        downloadedPage: savedPage,
                                                        error: error)
                })
            } else {
                DLog("Cannot retry, download failed.")
                status = .failed
                if let book = self.downloadingBook {
                    delegate?.bookDownloader(self,
                                             bookCompleted: book,
                                             status: status,
                                             error: nil)
                }
                terminateBackgroundTask()
            }
        }
    }
    
    fileprivate func download(_ page: Page,
                              toFolder downloadingCacheFolder: URL,
                              completionHandler: PageDownloadCompletion?) {
        if let pageURL = page.pageURL {
            let pageDownloader = PageImageDownloader()
            downloadingTask = pageDownloader
            _ = pageDownloader.download(pageURL,
                                        withBook: self.downloadingBook!,
                                        saveToURL: downloadingCacheFolder,
                                        completionHandler: {(success: Bool, savedFileURL: URL?, error: NSError?) -> Void in
                                            completionHandler?(success, page, savedFileURL, error)
            })
        } else {
            completionHandler?(false, page, nil, nil)
        }
    }
    
    fileprivate func recordCompletedPage() {
        if let completedPageFileURL = downloadingCacheFolder?.appendingPathComponent(BookDownloader.infoJsonFileName) {
            try? completedPages.jsonEncode()?.write(to: completedPageFileURL,
                                                    atomically: true,
                                                    encoding: .utf8)
        }
    }
    
    fileprivate func terminateBackgroundTask() {
        // Terminate backgrond task.
        if UIApplication.shared.applicationState == .background {
            DLog("Downloading of \(String(describing: downloadingBook?.title)) is terminated while the app in background.");
        }
        if let backgroundTaskIdentifier = backgroundTaskIdentifier, backgroundTaskIdentifier != UIBackgroundTaskInvalid {
            UIApplication.shared.endBackgroundTask(backgroundTaskIdentifier)
            self.backgroundTaskIdentifier = UIBackgroundTaskInvalid
        }
    }
}

// MARK: Start and stop.
extension BookDownloader {
    func stop() {
        if let downloadingTask = downloadingTask {
            DLog("\(String(describing: downloadingBook!.title)) downloading stopped...")
            downloadingTask.stop()
        }
        if status == .updating {
            self.downloadingBook?.stopUpdatingPages()
        }
        status = .paused
        // If there really is a book that is being downloaded.
        if let downloadingBook = downloadingBook {
            delegate?.bookDownloader(self,
                                     bookCompleted: downloadingBook,
                                     status: status,
                                     error: nil)
        }
        terminateBackgroundTask()
    }
}
