//
//  ThreadSleeper.swift
//  Exellency
//
//  Created by Craig Zheng on 28/05/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class ThreadSleeper: NSObject {
    class func randomSeconds(_ maximum: Int?)->Int {
        if let maximum = maximum {
            return Int(UInt32(arc4random_uniform(UInt32(maximum))))
        } else {
            return Int(UInt32(arc4random_uniform(6)))
        }
    }
    
    class func sleepInMainThread(_ seconds: Int, completion:(()->())?) {
        DispatchQueue.main.async { 
            sleep(UInt32(seconds))
            if let completion = completion {
                completion()
            }
        }
    }
    
    class func sleepInBackgroundThread(_ seconds:Int, completion:(()->())?) {
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(UInt64(seconds) * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
            if let completion = completion {
                completion()
            }
        })
    }
}
