//
//  Configuration.swift
//  Exellency
//
//  Created by Craig Zheng on 12/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

class Configuration: NSObject {
    // MARK: constants
    struct Key {
        static let keyword = "[KEYWORD]"
        static let page = "[PAGE]"
        static let pageID = "[PAGE_ID]"
        static let category = "[CATEGORY]"
        static let gid = "[GID]"
        static let token = "[TOKEN]"
    }
    fileprivate let defaultConfiguration = "defaultConfiguration"
    fileprivate let remoteConfigurationURL = URL(string: "http://civ.atwebpages.com/exheti/exheti_remote_configuration.php?bundleVersion=\(Configuration.bundleVersion)")! // 100% sure not optional.
    fileprivate var updateTimer: Timer?
    static let debugChangedNotification = "debugChangedNotification"
    static let updatedNotification = "Configuration.updatedNotification"
    static var bundleVersion: String {
        // Won't be nil.
        let bundleIdentifier = Bundle.main.object(forInfoDictionaryKey: "CFBundleIdentifier")!
        let versionString = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString")!
        return "\(bundleIdentifier)-\(versionString)"
    }

    fileprivate var _entityListURL: String?
    fileprivate var _entityDetailsURL: String?
    fileprivate var _entityPageURL: String?
    fileprivate var _metaDataURL: String?
    fileprivate var _imageKeyID: String?
    fileprivate var _searchIndexURL: String?
    fileprivate var _debugSearchIndexURL: String?
    fileprivate var _searchURL: String?
    fileprivate var _debugSearchURL: String?
    
    fileprivate let sessionManager = AFHTTPSessionManager.sessionManager()
    
    // MARK: public properties.
    
    let reportBlacklistURL = URL(string: "http://civ.atwebpages.com/exheti/exheti_report_blacklist.php")!
    let downloadBlacklistURL = URL(string: "http://civ.atwebpages.com/exheti/exheti_download_blacklist.php")!
    var detailBaseURL: String?
    var pageBaseURL: String?
    var remoteDebug = false
    var allowedCategory = [String]()
    var remoteActions = [[String: String]]()
    var warningKeyword = ""
    var numberOfDownloaders: Int {
        var number = 5
        if AdConfiguration.singleton.shouldDisplayAds {
            number = 2
        }
        return number
    }
    var announcement: String?
    var proxyMetaDataURL: String?
    var updatedWithServer = false
    
    var entityListURL: String? {
        get {
            if let entityListURL = _entityListURL {
                return entityListURL
            }
            return nil
        }
        set {
            _entityListURL = newValue
        }
    }
    var entityDetailsURL: String? {
        get {
            if let entityDetailsURL = _entityDetailsURL {
                return entityDetailsURL
            }
            return nil
        }
        set {
            _entityDetailsURL = newValue
        }
    }
    var entityPageURL: String? {
        get {
            if let entityPageURL = _entityPageURL {
                return entityPageURL
            }
            return nil
        }
        set {
            _entityPageURL = newValue
        }
    }
    var metaDataURL: String? {
        get {
            if let metaDataURL = _metaDataURL {
                return metaDataURL
            }
            return nil
        }
        set {
            _metaDataURL = newValue
        }
    }
    var searchIndexURL: String? {
        get {
            if let searchIndexURL = _searchIndexURL {
                return searchIndexURL
            }
            return nil
        }
        set {
            _searchIndexURL = newValue
        }
    }
    var debugSearchIndexURL: String? {
        get {
            if let debugSearchIndexURL = _debugSearchIndexURL {
                return debugSearchIndexURL
            }
            return nil
        }
        set {
            _debugSearchIndexURL = newValue
        }
    }
    var searchURL: String? {
        get {
            if let searchURL = _searchURL {
                return searchURL
            }
            return nil
        }
        set {
            _searchURL = newValue
        }
    }
    var debugSearchURL: String? {
        get {
            if let debugSearchURL = _debugSearchURL {
                return debugSearchURL
            }
            return nil
        }
        set {
            _debugSearchURL = newValue
        }
    }
    var imageKeyID: String? {
        get {
            if let imageKeyID = _imageKeyID {
                return imageKeyID.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            }
            return nil
        }
        set {
            _imageKeyID = newValue
        }
    }
    var imagePerPage = 40
    var timeout: TimeInterval = 20
    var thumbnailWidth = 50.0
    var debug: Bool {
        get {
            if UserDefaults.standard.object(forKey: "DEBUG") != nil {
                return UserDefaults.standard.bool(forKey: "DEBUG")
            }
            return false
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "DEBUG")
            UserDefaults.standard.synchronize()
            NotificationCenter.default.post(name: Notification.Name(rawValue: Configuration.debugChangedNotification), object: nil)
        }
    }

    // MARK: Util methods.
    
    func searchURL(_ keyword:String, page: Int)->URL? {
        // Search URL: http://g.e-hentai.org/?f_doujinshi=1&f_manga=1&f_artistcg=1&f_gamecg=1&f_western=1&f_non-h=1&f_imageset=1&f_cosplay=1&f_asianporn=1&f_misc=1&f_search=chinese&f_apply=Apply+Filter
        // Another: http://g.e-hentai.org/?f_doujinshi=1&f_manga=0&f_artistcg=0&f_gamecg=0&f_western=0&f_non-h=1&f_imageset=0&f_cosplay=0&f_asianporn=0&f_misc=0&f_search=chinese&f_apply=Apply+Filter
        
        var searchURLString: String
        // Default - unlikely to happen.
        // Non-h
        searchURLString = "http://g.e-hentai.org/?f_doujinshi=0&f_manga=0&f_artistcg=0&f_gamecg=0&f_western=0&f_non-h=1&f_imageset=0&f_cosplay=0&f_asianporn=0&f_misc=0&f_search=\(keyword)&f_apply=Apply+Filter"

        if Configuration.singleton.debug {
            if let debugSearchIndexURL = debugSearchIndexURL {
                searchURLString = debugSearchIndexURL.replacingOccurrences(of: Key.keyword, with: keyword)
            }
        } else {
            // Non-h only.
            if let searchIndexURL = searchIndexURL {
                searchURLString = searchIndexURL.replacingOccurrences(of: Key.keyword, with: keyword)
            }
        }
        // For page 2 and above.
        if page > 1 {
            // All.
            if Configuration.singleton.debug {
                if let debugSearchURL = debugSearchURL {
                    searchURLString = debugSearchURL.replacingOccurrences(of: Key.keyword, with: keyword)
                        .replacingOccurrences(of: Key.page, with: "\(page - 1)")
                }
            } else {
                if let searchURL = searchURL {
                    searchURLString = searchURL.replacingOccurrences(of: Key.keyword, with: keyword)
                    .replacingOccurrences(of: Key.page, with: "\(page - 1)")
                }
            }
        }
        return URL(string: searchURLString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!)
    }
    
    // MARK: JSON parsing.
    
    func parseJSONData(_ jsonData: Data)->Bool {
        var parseSuccessful = false
        if let jsonDictionary = (try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers)) as? NSDictionary {
            parseJSONDictionary(jsonDictionary)
            parseSuccessful = true
        }
        return parseSuccessful
    }
    
    func parseJSONDictionary(_ jsonDictionary: NSDictionary) {
        if let entityListURL = jsonDictionary["entityListURL"] as? String, !entityListURL.isEmpty {
            self.entityListURL = entityListURL
        }
        if let entityDetailsURL = jsonDictionary["entityDetailsURL"] as? String, !entityDetailsURL.isEmpty {
            self.entityDetailsURL = entityDetailsURL
        }
        if let entityPageURL = jsonDictionary["entityPageURL"] as? String, !entityPageURL.isEmpty {
            self.entityPageURL = entityPageURL
        }
        if let searchIndexURL = jsonDictionary["searchIndexURL"] as? String, !searchIndexURL.isEmpty {
            self.searchIndexURL = searchIndexURL
        }
        if let debugSearchIndexURL = jsonDictionary["debugSearchIndexURL"] as? String, !debugSearchIndexURL.isEmpty {
            self.debugSearchIndexURL = debugSearchIndexURL
        }
        if let searchURL = jsonDictionary["searchURL"] as? String, !searchURL.isEmpty {
            self.searchURL = searchURL
        }
        if let debugSearchURL = jsonDictionary["debugSearchURL"] as? String, !debugSearchURL.isEmpty {
            self.debugSearchURL = debugSearchURL
        }
        if let metaDataURL = jsonDictionary["metaDataURL"] as? String, !metaDataURL.isEmpty {
            self.metaDataURL = metaDataURL
        }
        if let imageKeyID = jsonDictionary["imageKeyID"] as? String, !imageKeyID.isEmpty {
            self.imageKeyID = imageKeyID
        }
        if let imagePerPage = jsonDictionary["imagePerPage"] as? NSNumber {
            self.imagePerPage = Int(imagePerPage.int32Value)
        }
        if let timeout = jsonDictionary["timeout"] as? NSNumber {
            self.timeout = TimeInterval(timeout.doubleValue)
        }
        if let thumbnailWidth = jsonDictionary["thumbnailWidth"] as? NSNumber {
            self.thumbnailWidth = Double(thumbnailWidth.doubleValue)
        }
        if let remoteDebug = jsonDictionary["remoteDebug"] as? NSNumber {
            self.remoteDebug = remoteDebug.boolValue
            self.debug = self.remoteDebug
        }
        if let allowedCategory = jsonDictionary["allowedCategory"] as? Array<String>, !allowedCategory.isEmpty {
            self.allowedCategory = allowedCategory
        }
        if let warningKeyword = jsonDictionary["warningKeyword"] as? String, !warningKeyword.isEmpty {
            self.warningKeyword = warningKeyword
        }
        if let announcement = jsonDictionary["announcement"] as? String, !announcement.isEmpty {
            self.announcement = announcement
        }
        if let remoteActions = jsonDictionary["remoteActions"] as? [[String: String]], !remoteActions.isEmpty {
            self.remoteActions = remoteActions
        }
        if let proxyMetaDataURL = jsonDictionary["proxyMetaDataURL"] as? String, !proxyMetaDataURL.isEmpty {
            self.proxyMetaDataURL = proxyMetaDataURL
        } else {
            self.proxyMetaDataURL = nil
        }
        if let entityDetailsBaseURL = jsonDictionary["entityDetailsBaseURL"] as? String, !entityDetailsBaseURL.isEmpty {
            self.detailBaseURL = entityDetailsBaseURL
        }
        if let pageBaseURL = jsonDictionary["pageBaseURL"] as? String, !pageBaseURL.isEmpty {
            self.pageBaseURL = pageBaseURL
        }
    }
    
    // MARK: Update.
    
    func updateWithCompletion(_ completion: (()->())?) {
        DLog("Updating configuration.")
        sessionManager.dataTask(with: EXConfigureURLRequest(url: remoteConfigurationURL) as URLRequest) { response, responseObject, error in
            DLog("Updating completed.")
            if error == nil, let responseObject = responseObject as? Data {
                self.parseJSONData(responseObject)
                self.updatedWithServer = true
            }
            if let completion = completion {
                completion()
            }
            // Configuration updated notification.
            NotificationCenter.default.post(name: Notification.Name(rawValue: Configuration.updatedNotification), object: nil)
        }.resume()
    }
    
    // MARK: Init method.
    
    override init() {
        super.init()
        // Init with defaultConfiguration.json file.
        if let defaultJsonURL = Bundle.main.url(forResource: defaultConfiguration, withExtension: "json"),
            let defaultJsonData = try? Data(contentsOf: defaultJsonURL)
        {
            parseJSONData(defaultJsonData)
        }
        
        // Schedule a timer to update once in a while.
        // This solution is copied from http://stackoverflow.com/questions/14924892/nstimer-with-anonymous-function-block
        updateTimer = Timer.scheduledTimer(timeInterval: 60 * 10,
                                                             target: BlockOperation(block: {
                                                                self.updateWithCompletion(nil)
                                                             }),
                                                             selector: #selector(Operation.main),
                                                             userInfo: nil,
                                                             repeats: true)
        NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive,
                                                                object: nil,
                                                                queue: OperationQueue.main) { _ in
                                                                    if !self.updatedWithServer {
                                                                        // Update with remote configuration.
                                                                        // Update with remote configuration.
                                                                        self.updateWithCompletion({
                                                                            DLog("Remote notification updates.")
                                                                            if let announcement = self.announcement, !announcement.isEmpty {
                                                                                DispatchQueue.main.async(execute: {
                                                                                    ProgressHUD.showMessage(announcement)
                                                                                })
                                                                            }
                                                                        })
                                                                    }
        }
    }

    // Singleton.
    static var singleton = Configuration()
}
