//
//  ListParser.swift
//  Exellency
//
//  Created by Craig Zheng on 21/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

@objc class ListParser: NSObject {
    static let sessionManager = AFHTTPSessionManager.sessionManager()
    
    class func parseURL(_ url: URL, completionHandler:@escaping (_ success: Bool, _ links:[String]?, _ error: NSError?)->Void)->URLSessionTask? {
        let request = EXURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Configuration.singleton.timeout)
        let sessionTask = sessionManager.dataTask(with: request as URLRequest, completionHandler: { (urlResponse, responseObject, error) -> Void in
            // Ignore cancelled error.
            if (error as? NSError)?.code == NSURLErrorCancelled {
                return
            }
            if error == nil,
                let data = responseObject as? Data,
                let htmlString = String(data: data, encoding: String.Encoding.utf8) {
                    let links = self.parseHtml(htmlString)
                    completionHandler(true, links, nil)
            } else {
                completionHandler(false, nil, error as NSError?)
            }
        })
        sessionTask.resume()
        return sessionTask
    }
    
    class func parseHtml(_ htmlString: String)->[String] {
        var bookLinks = [String]()
        let parsedLinks = ObjectiveGumbo.parseDocument(with: htmlString).elements(with: GUMBO_TAG_A) as! [OGElement];
        for element in parsedLinks {
            for key in (element.attributes as NSDictionary).allKeys {
                if (key as AnyObject).lowercased == "href" {
                    if let href = element.attributes[key as! String] as? String {
                        if let detailHost = Configuration.singleton.detailBaseURL,
                            let targetString = URL(string: href)?.absoluteString, targetString.contains(detailHost) {
                            bookLinks.append(href)
                        }
                    }
                }
            }
        }
        return bookLinks
    }
    
    class func bookWithURL(_ url: URL, completionHandler:@escaping (_ success: Bool, _ book: Book?, _ error: NSError?)->Void)->URLSessionTask? {
        // Return only 1 book.
        return booksWithURLs([url], completionHandler: { (success, books, error) in
            completionHandler(success, books?.first, error)
        })
    }
    
    class func booksWithURLs(_ urls: [URL], completionHandler:@escaping (_ success: Bool, _ books: [Book]?, _ error: NSError?)->Void)->URLSessionTask? {
        return EntityParser.metaDataWithURLs(urls) { (success, jsonString, error) -> Void in
            // 2rd: Construct a book object with the json.
            if success,
                let jsonData = jsonString?.data(using: String.Encoding.utf8),
                let jsonDict = (try? JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)) as? [String: AnyObject],
                let dictArray = jsonDict["gmetadata"] as? [[String: AnyObject]]
            {
                let books = dictArray.map({ (jsonDict) -> Book? in
                    if let jsonData = try? JSONSerialization.data(withJSONObject: jsonDict, options: []),
                        let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)
                    {
                        let book = Book(jsonString: jsonString)
                        return book
                    }
                    return nil
                })
                completionHandler(success, books as? [Book], error)
            } else {
                completionHandler(success, nil, error)
            }
        }
    }
    
    class func booksWithCategory(_ category: Category, page: Int, completionHandler: @escaping (_ success: Bool, _ books:[Book]?, _ error: NSError?)->Void)-> URLSessionTask? {
        if let rawURL = Configuration.singleton.entityListURL,
            let entityListURL = rawURL.replacingOccurrences(of: Configuration.Key.category, with: category.rawValue)
                .replacingOccurrences(of: Configuration.Key.page, with: "\(page)")
                .urlEscapedString()
        {
            let categoryURL = URL(string: entityListURL)
            let request = EXURLRequest(url: categoryURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Configuration.singleton.timeout)
            DLog("BooksWithCategory: \(categoryURL)")
            let sessionTask = sessionManager.dataTask(with: request as URLRequest) { (urlResponse, responseObject, error) -> Void in
                // Ignore cancelled error.
                if (error as? NSError)?.code == NSURLErrorCancelled {
                    return
                }
                if let data = responseObject as? Data,
                    let responseString = String(data: data, encoding: String.Encoding.utf8) {
                    self.booksWithHtml(responseString, completionHandler: completionHandler)
                } else {
                    completionHandler(false, nil, error as NSError?)
                }
            }
            sessionTask.resume()
            return sessionTask
        } else {
            completionHandler(false, nil, nil)
            return nil;
        }
    }
    
    class func booksWithKeyword(_ keyword: String, page: Int, completionHandler:@escaping (_ success: Bool, _ books:[Book]?, _ error: NSError?)->Void)->URLSessionTask? {
        let categoryURL = Configuration.singleton.searchURL(keyword, page: page)
        let request = EXURLRequest(url: categoryURL!, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Configuration.singleton.timeout)
        DLog("BooksWithKeyword: \(categoryURL)")
        let sessionTask = sessionManager.dataTask(with: request as URLRequest) { (urlResponse, responseObject, error) -> Void in
            // Ignore cancelled error.
            if (error as? NSError)?.code == NSURLErrorCancelled {
                return
            }
            if let data = responseObject as? Data,
                let responseString = String(data: data, encoding: String.Encoding.utf8) {
                self.booksWithHtml(responseString, completionHandler: completionHandler)
            } else {
                completionHandler(false, nil, error as NSError?)
            }
        }
        sessionTask.resume()
        return sessionTask
    }
    
    class func booksWithHtml(_ htmlString: String, completionHandler:@escaping (_ success: Bool, _ books: [Book]?, _ error: NSError?)->Void) {
        let links = self.parseHtml(htmlString)
        if links.isEmpty {
            // Empty input, successfully parsed but there is nothing to display.
            completionHandler(true, nil, nil)
        } else {
            let urls = links.map({ (string) -> URL? in
                if let string = string.urlEscapedString() {
                    return URL(string: string)
                }
                return nil
            })
            if let urls = urls as? [URL] {
                self.booksWithURLs(urls, completionHandler: { (success, books, error) in
                    completionHandler(success, books, error)
                })
            } else {
                completionHandler(false, nil, nil)
            }
        }
    }
}
