//
//  FavouriteManager.swift
//  Exellency
//
//  Created by Craig on 4/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class FavouriteManager: NSObject {
    static let singleton = FavouriteManager()
    static let favouriteFolderURL: URL? = {
        return BookCache.libraryFolderURL?.appendingPathComponent("Favourite")
    }()
    var favouriteBooks = NSMutableOrderedSet()
    lazy var favouriteFolderURL: URL? = {
        let fileManager = FileManager.default
        if let favouriteFolderURL = FavouriteManager.favouriteFolderURL {
            let favouriteFolderPath = favouriteFolderURL.path
            // If the destination folder has not been created, create it.
            if !fileManager.fileExists(atPath: favouriteFolderPath) {
                do {
                    try fileManager.createDirectory(at: favouriteFolderURL,
                                                         withIntermediateDirectories: true,
                                                         attributes: [FileAttributeKey.protectionKey.rawValue: FileProtectionType.none])
                } catch {
                    DLog(error)
                }
            }
            return favouriteFolderURL
        }
        return nil
    } ()
    
    override init() {
        super.init()
        restore()
    }
}

// MARK: Util methods.
extension FavouriteManager {
    func addBookToFavourite(_ book: Book) {
        favouriteBooks.add(book)
        save()
    }
    
    func removeBookFromFavourite(_ book: Book) {
        favouriteBooks.remove(book)
        save()
    }
    
    func isBookInFavourite(_ book: Book)->Bool {
        return favouriteBooks.contains(book)
    }
    
    func save() {
        // Save the json files to favourite folder.
        let fileManager = FileManager.default
        if let favouriteFolderURL = favouriteFolderURL {
            // Remove old files within the favourite folder.
            _ = try? fileManager.contentsOfDirectory(at: favouriteFolderURL, includingPropertiesForKeys: nil, options: .skipsSubdirectoryDescendants).forEach({ fileURL in
                _ = try? fileManager.removeItem(at: fileURL)
            })
            favouriteBooks.forEach({ book in
                if let book = book as? Book {
                    if let fileName = BookCache.fileNameForBook(book),
                        let jsonString = book.jsonString {
                        let filePath = favouriteFolderURL.appendingPathComponent(fileName).path
                        if !filePath.isEmpty {
                            fileManager.createFile(atPath: filePath,
                                contents: jsonString.data(using: String.Encoding.utf8),
                                attributes: [FileAttributeKey.protectionKey.rawValue: FileProtectionType.none])
                            DLog("Favourite manager saved file to: \(filePath)")
                        }
                    }
                }
            })
        }
    }

    /**
     Restore the FavouriteManager.favouriteBooks set, also return the restored set.
     */
    func restore()->NSMutableOrderedSet? {
        var restoredSet: NSMutableOrderedSet?
        let fileManager = FileManager.default
        if let favouriteFolderURL = favouriteFolderURL,
            let jsonFiles = try? fileManager.contentsOfDirectory(at: favouriteFolderURL, includingPropertiesForKeys: nil, options: .skipsSubdirectoryDescendants) {
            restoredSet = NSMutableOrderedSet()
            for jsonFile in jsonFiles {
                if let jsonString = try? String(contentsOf: jsonFile) {
                    let book = Book(jsonString: jsonString)
                    restoredSet?.add(book)
                }
            }
        }
        if restoredSet?.count > 0 {
            self.favouriteBooks = restoredSet!
        }
        return restoredSet
    }
}
