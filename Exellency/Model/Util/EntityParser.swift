//
//  EntityParser.swift
//  Exellency
//
//  Created by Craig Zheng on 12/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

class EntityParser: NSObject {
    static let sessionManager = AFHTTPSessionManager.sessionManager()
    
    @objc class func metaDataWithURLs(_ urls: [URL], completionHandler: @escaping ((_ success: Bool, _ jsonString: String?, _ error: NSError?)-> Void))-> URLSessionTask? {
        var sessionTask: URLSessionTask?
        //http://g.e-hentai.org/g/892272/98bb276b81 example
        if !urls.isEmpty {
            let gidlist = NSMutableArray()
            urls.forEach({ (url) in
                let components = url.absoluteString.components(separatedBy: "/") as [String]
                if components.count >= 6,
                    let galleryID = Int32(components[4])
                    
                {
                    let galleryToken = components[5]
                    let gid = [NSNumber(value: galleryID as Int32), galleryToken] as [Any]
                    gidlist.add(gid)
                }
            })
            if gidlist.count > 0 {
                let dictionary = ["method":"gdata","gidlist":gidlist] as [String : Any]
                // Construct the URL request.
                var request: EXURLRequest!
                var requestURL: URL!
                // FIXME: correct this later.
                if let proxyServer = Configuration.singleton.proxyMetaDataURL, Configuration.singleton.proxyMetaDataURL != nil
                {
                    // With proxy.
                    requestURL = URL(string: proxyServer)!
                } else {
                    // No proxy.
                    requestURL = URL(string: Configuration.singleton.metaDataURL!)!
                }
                request = EXURLRequest(url: requestURL, cachePolicy: .reloadIgnoringCacheData, timeoutInterval: Configuration.singleton.timeout)
                request.httpMethod = "POST"
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                do {
                    let jsonData = try JSONSerialization.data(withJSONObject: dictionary, options: JSONSerialization.WritingOptions.prettyPrinted)
                    request.httpBody = jsonData
                } catch {
                    DLog(error)
                }
                // Constrcut and start the data task.
                sessionTask = sessionManager.dataTask(with: request as URLRequest, completionHandler: { (urlResponse, responseObject, error) -> Void in
                    // Ignore cancelled error.
                    if (error as? NSError)?.code == NSURLErrorCancelled {
                        return
                    }
                    if let data = responseObject as? Data,
                        let responseString = String(data: data, encoding: String.Encoding.utf8) {
                        completionHandler(error == nil, responseString, error as NSError?)
                    } else {
                        completionHandler(error == nil, responseObject as? String, error as NSError?)
                    }
                })
                sessionTask!.resume()
            } else {
                completionHandler(false, nil, nil)
            }
        } else {
            completionHandler(false, nil, nil)
        }
        return sessionTask
    }
    
    // Parse URL for links to each page.
    class func pagesWithURL(_ url: URL?, completionHandler: @escaping (_ success: Bool, _ pages:[Page]?, _ error: NSError?)->Void)->URLSessionTask? {
        // http://g.e-hentai.org/g/893415/a8c3907d31/ example
        var sessionTask: URLSessionTask?
        if let url = url {
            let request = EXURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Configuration.singleton.timeout)
            sessionTask = self.htmlWithRequest(request as URLRequest, completionHandler: { (success, htmlString, error) -> Void in
                if success, let htmlString = htmlString {
                    // If the returned htmlString contains warningKeyword, try again with some additional modifications.
                    if htmlString.contains(Configuration.singleton.warningKeyword) {
                        // Same method, only the URL is slightly different - added skipping warning for the current session.
                        if var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false) {
                            let queryItem = URLQueryItem(name: "nw", value: "session")
                            urlComponents.queryItems = [queryItem]
                            self.pagesWithURL(urlComponents.url, completionHandler: completionHandler)
                        } else {
                            completionHandler(false, nil, nil)
                        }
                    } else {
                        let links = self.pageLinksWithHtml(htmlString)
                        completionHandler(true, links, nil)
                    }
                } else {
                    completionHandler(false, nil, error)
                }
            })
        } else {
            completionHandler(false, nil, nil)
        }
        return sessionTask
    }
    
    // Parse HTML for links to each page.
    class func pageLinksWithHtml(_ htmlString: String)->[Page] {
        var pages = Array<Page>()
        let parsedDocument = ObjectiveGumbo.parseDocument(with: htmlString)
        let elements = parsedDocument?.elements(withClass: "gdtm") as! [OGElement];
        // For each div element with the specified class, get all anchor links.
        for divElement in elements {
            for linkElement in divElement.elements(with: GUMBO_TAG_A) {
                // Parse each anchor link, if it contains the keyword, add it as a page.
                for key in ((linkElement as AnyObject).attributes as NSDictionary).allKeys as! [String] {
                    if key.lowercased() == "href" {
                        if let href = ((linkElement as AnyObject).attributes[key] as? NSString)?.removingPercentEncoding, href.contains(Configuration.singleton.pageBaseURL!)
                        {
                            let page = Page()
                            let rawURL = URL(string: href)
                            if var urlComponents = rawURL?.pathComponents, urlComponents.count >= 2,
                                let rawPageURLString = Configuration.singleton.entityPageURL,
                                let pageID = urlComponents.popLast(),
                                let token = urlComponents.popLast() // Second to the last.
                            {
                                let pageURLString = rawPageURLString.replacingOccurrences(of: Configuration.Key.pageID, with: pageID)
                                    .replacingOccurrences(of: Configuration.Key.token, with: token).urlEscapedString() ?? ""
                                page.pageURL = URL(string: pageURLString)
                            }
                            
                            // Try to get the thumbnail URL.
                            let thumbnailURLs = thumbnailsWithHtml(divElement.html())
                            if thumbnailURLs.count > 0 {
                                page.thumbnailURL = thumbnailURLs.first
                            }
                            // Thumbnail style, regex: width(.*?)px height(.*?)px
                            let html = divElement.html()
                            var width = 0
                            var height = 0
                            if let widthRegex = try? NSRegularExpression(pattern: "width(.*?)px", options: .caseInsensitive) {
                                let matches = widthRegex.matches(in: html!,
                                    options: [],
                                    range: NSMakeRange(0, (html?.characters.count)!))
                                if let match = matches.first {
                                    var widthString = html?.substring(with: html!.range(from: match.range)!)
                                    if (widthString?.characters.count)! > 0 {
                                        widthString = widthString?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                                        width = Int(widthString!)!
                                    }
                                }
                            }
                            if let heightRegex = try? NSRegularExpression(pattern: "height(.*?)px", options: .caseInsensitive) {
                                let matches = heightRegex.matches(in: html!,
                                    options: [],
                                    range: NSMakeRange(0, (html?.characters.count)!))
                                if let match = matches.first {
                                    var heightString = html?.substring(with: html!.range(from: match.range)!)
                                    if (heightString?.characters.count)! > 0 {
                                        heightString = heightString?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                                        height = Int(heightString!)!
                                    }
                                }
                            }
                            if width != 0 && height != 0 {
                                let size = CGSize(width: CGFloat(width), height: CGFloat(height))
                                page.thumbnailStyle = size
                            }

                            pages.append(page)
                        }
                    }
                }
            }
        }
        
        return pages
    }
    
    // Parse URL for full size image URL.
    class func imageWithURL(_ url: URL?, completionHandler: @escaping (_ success: Bool, _ pages:[Page]?, _ error: NSError?)->Void)->URLSessionTask? {
        var sessionTask: URLSessionTask?
        if (url != nil) {
            let url = url!
            let request = EXURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Configuration.singleton.timeout)
            sessionTask = self.htmlWithRequest(request as URLRequest, completionHandler: { (success, htmlString, error) -> Void in
                if success, let htmlString = htmlString {
                    let links = self.imagesWithHtml(htmlString)
                    completionHandler(true, links, nil)
                } else {
                    completionHandler(false, nil, nil)
                }
            })
        }
        return sessionTask
    }
    
    // Parse HTML for full size image URLs, and construct an array of pages with these URLs.
    class func imagesWithHtml(_ htmlString: String)->[Page] {
        // http://g.e-hentai.org/s/7ba358d58b/893415-1 example URL
        // <img id="img" style="height: 1733px; width: 1200px; max-width: 921px; max-height: 1330px;" src="http://36.224.197.240:21732/h/49946bbeb6d45b9ee774ad20abe5cf…-342112-1200-1733-jpg/keystamp=1453035600-c5adea6b65/003.jpg"></img> // actual block.

        // http://221.149.240.224:1024/h/8d4ed2b0e3e30504520a4d482ed69549a0ea0efb-415654-1200-1711-jpg/keystamp=1453035300-eadf237f06/IMG_0000.jpg actual link.
        let parsedDocument = ObjectiveGumbo.parseDocument(with: htmlString)
        var pages = [Page]()
        // Find all <img> tags, within them find the ones with <id = "img">.
        let parsedLinks = parsedDocument?.elements(with: GUMBO_TAG_IMG) as! [OGElement];
        for imageElement in parsedLinks {
            
            if let rawString = (imageElement.attributes["src"] as? String),
                let src = (rawString as NSString).removingPercentEncoding
            {
                //\d+\.+?\d+\.+?\d+\.+?\d+
                if let urlRegex = try? NSRegularExpression(pattern: "\\d+\\.+?\\d+\\.+?\\d+\\.+?\\d+", options: .caseInsensitive) {
                    let matches = urlRegex.matches(in: src, options: [], range: NSMakeRange(0, src.characters.count))
                    // If the all digits host can be found.
                    if let range = matches.first?.range,
                        range.location != NSNotFound{
                        let page = Page()
                        page.imageURL = URL(string: src.urlEscapedString()!)
                        pages.append(page)
                    }
                }
/*
                if src.containsString(Configuration.singleton.imageKeyID!) {
                    let page = Page()
                    let src: String = imageElement.attributes["src"] as! String
                    page.imageURL = NSURL(string: src.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!)
                    pages.append(page)
                }
*/
            }
        }
        
        return pages
    }
    
    // Parse HTML for thumbnail URL.
    class func thumbnailsWithHtml(_ htmlString: String)->[URL] {
        var links = Set<URL>()
        let parsedDocument = ObjectiveGumbo.parseDocument(with: htmlString)
        do {
            // Regex: url\((.*?)\)
            let urlRegex = try NSRegularExpression(pattern: "url\\((.*?)\\)", options: .caseInsensitive)
            for divElement in (parsedDocument?.elements(withClass: "gdtm"))!{
                for thumbnailDiv in (divElement as! OGElement).elements(with: GUMBO_TAG_DIV) {
                    let style = ((thumbnailDiv as AnyObject).attributes["style"] as? NSString)?.removingPercentEncoding
                    // Find matches with urlRegex in style.
                    let matches = urlRegex.matches(in: style!,
                        options: [],
                        range: NSMakeRange(0,
                            style!.characters.count))
                    if let match = matches.first {
                        if match.range.location != NSNotFound {
                            let matchedString = style?.substring(with: style!.range(from: match.range)!)
                            // Put link detector to use.
                            let types: NSTextCheckingResult.CheckingType = [.link]
                            let linkDetector = try NSDataDetector(types: types.rawValue)
                            let matches = linkDetector.matches(in: matchedString!, options: [], range: NSMakeRange(0, matchedString!.characters.count))
                            if matches.first!.range.location != NSNotFound {
                                let detectedURLString = matchedString?.substring(with: matchedString!.range(from: matches.first!.range)!)
                                if let detectedURLString = detectedURLString?.replacingOccurrences(of: "&b=4", with: ""),
                                    var urlComponenets = URLComponents(string: detectedURLString)
                                {
                                    urlComponenets.queryItems = nil
                                    if let url = urlComponenets.url {
                                        links.insert(url)
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch {
            DLog(error)
        }
        return Array(links);
    }
    
    // Parse URL for comment objects.
    class func commentsWithURL(_ url: URL, completionHandler: @escaping (_ success: Bool, _ comments: [Comment]?, _ error: NSError?)->Void)->URLSessionTask? {
        let request = EXURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Configuration.singleton.timeout)
        return self.htmlWithRequest(request as URLRequest) { (success, htmlString, error) -> Void in
            if success, let htmlString = htmlString {
                let comments = self.commentsWithHtml(htmlString)
                completionHandler(true, comments, nil)
            } else {
                completionHandler(false, nil, error)
            }
        }
    }
    
    // Parse HTML for comment objects.
    class func commentsWithHtml(_ htmlString: String)->[Comment] {
        var comments = [Comment]()
        /*
        <div class="c6" id="comment_1166977">&gt;Destroyer sex<br /><br />Yes please</div>
        <div class="c7" id="cvotes_1166977" style="display:none">Base +9, <span>ghsh -8</span>, <span>FeiFongWong +6</span></div>

        */
        // This regex would be good to pick up all "comment_<id>" strings. \"comment\_[0-9]+\"
        do {
            let commentIDMatches = try NSRegularExpression(pattern: "\"comment\\_[0-9]+\"", options: .caseInsensitive)
            .matches(in: htmlString, options: [], range: NSRange(location: 0, length: htmlString.characters.count))
            // Get all comments with ID.
            var commentIDs = [String]()
            for match in commentIDMatches {
                var matchedString = htmlString.substring(with: htmlString.range(from: match.range)!)
                // Remove double quotes.
                matchedString = matchedString.replacingOccurrences(of: "\"", with: "")
                DLog("Matched string: \(matchedString)")
                commentIDs.append(matchedString)
            }
            for commentID in commentIDs {
                var combinedComment = ""
                var combinedPoster = ""
                for comment in ObjectiveGumbo.parseDocument(with: htmlString).elements(withID: commentID) as! [OGElement] {
                    DLog(comment.text())
                    combinedComment += comment.text()
                    for posterDivs in (comment.parent.elements(withClass: "c3")) as! [OGElement] {
                        combinedPoster += posterDivs.text()
                    }
                }
                // Got comment content, create a new comment object to host it.
                if combinedComment.characters.count > 0 {
                    let newComment = Comment()
                    newComment.commentContent = combinedComment
                    comments.append(newComment)
                    if combinedPoster.characters.count > 0 {
                        newComment.poster = combinedPoster
                    }
                }
            }
        } catch {
            // Cannot parse comment, do nothing.
        }
        return comments
    }
    
    // Method to get HTML page.
    class func htmlWithURLString(_ urlString: String, completionHandler: @escaping (_ success: Bool, _ htmlString: String?, _ error: NSError?)->Void)->URLSessionTask? {
        var sessionTask: URLSessionTask?
        if let url = URL(string: urlString) {
            let request = EXURLRequest(url: url, cachePolicy: .useProtocolCachePolicy, timeoutInterval: Configuration.singleton.timeout)
            sessionTask = self.htmlWithRequest(request as URLRequest, completionHandler: completionHandler)
        } else {
            completionHandler(false, nil, nil)
        }
        return sessionTask
    }
    
    class func htmlWithRequest(_ request: URLRequest, completionHandler: @escaping (_ success: Bool, _ htmlString: String?, _ error: NSError?)->Void)->URLSessionTask? {
        let sessionTask = sessionManager.dataTask(with: request, completionHandler: { (urlResponse, responseObject, error) -> Void in
            // Ignore cancelled error.
            if (error as? NSError)?.code == NSURLErrorCancelled {
                return
            }
            completionHandler(error == nil,
                responseObject != nil ? String(data: (responseObject as! Data), encoding: String.Encoding.utf8) : nil,
                error as NSError?)
        })
        sessionTask.resume()
        return sessionTask
    }
}

extension String {
    func nsRange(from range: Range<String.Index>) -> NSRange {
        let from = range.lowerBound.samePosition(in: utf16)
        let to = range.upperBound.samePosition(in: utf16)
        return NSRange(location: utf16.distance(from: utf16.startIndex, to: from),
                       length: utf16.distance(from: from, to: to))
    }
}

extension String {
    func range(from nsRange: NSRange) -> Range<String.Index>? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(from16, offsetBy: nsRange.length, limitedBy: utf16.endIndex),
            let from = String.Index(from16, within: self),
            let to = String.Index(to16, within: self)
            else { return nil }
        return from ..< to
    }
}
