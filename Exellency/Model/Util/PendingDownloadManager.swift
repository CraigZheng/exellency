//
//  PendingDownloadManager.swift
//  Exellency
//
//  Created by Craig on 20/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class PendingDownloadManager: NSObject {
    
    static let singleton = PendingDownloadManager()
    var pendingDownloads = [Book]()
    let pendingDownloadsFolderURL: URL? = {
        return BookCache.libraryFolderURL?.appendingPathComponent(pendingDownloadsFolderName)
    }()

    // MARK: Private properties.
    fileprivate static let pendingDownloadsFolderName = "PendingDownloads"
    
    override init() {
        super.init()
        // Create the pendingDownloads folder if it does not exist.
        let fileManager = FileManager.default
        if let pendingDownloadsFolderURL = pendingDownloadsFolderURL,
            !pendingDownloadsFolderURL.path.isEmpty
        {
            let pendingDownloadsFolderPath = pendingDownloadsFolderURL.path
            if !fileManager.fileExists(atPath: pendingDownloadsFolderPath) {
                _ = try? fileManager.createDirectory(at: pendingDownloadsFolderURL,
                                                          withIntermediateDirectories: true,
                                                          attributes: [FileAttributeKey.protectionKey.rawValue: FileProtectionType.none])
            }
        }
        restore()
    }
    
    func addBookToPending(_ book: Book) {
        if !pendingDownloads.contains(book) {
            pendingDownloads.append(book)
            save()
        }
    }
    
    func removePendingBook(_ book: Book) {
        if let index = pendingDownloads.index(of: book) {
            pendingDownloads.remove(at: index)
            save()
        }
    }
    
    func nextBook()->Book? {
        var nextBook: Book?
        if let book = pendingDownloads.first {
            // If the nextBook object is not nil, remove it from pendingDownloads set.
            pendingDownloads.removeFirst()
            nextBook = book
            save()
        }
        return nextBook
    }
    
    func save() {
        // Remove everything within the pending downloads folder.
        if let pendingDownloadsFolderURL = pendingDownloadsFolderURL {
            _ = try? FileManager.default.contentsOfDirectory(at: pendingDownloadsFolderURL, includingPropertiesForKeys: nil, options: .skipsSubdirectoryDescendants).forEach({ fileURL in
                _ = try? FileManager.default.removeItem(at: fileURL)
            })
            pendingDownloads.forEach { book in
                if let jsonFileName = BookCache.fileNameForBook(book)
                {
                    let filePath = pendingDownloadsFolderURL.appendingPathComponent(jsonFileName).path
                    // Write everything to storage.
                    FileManager.default.createFile(atPath: filePath,
                        contents: book.jsonString?.data(using: String.Encoding.utf8),
                        attributes: [FileAttributeKey.protectionKey.rawValue: FileProtectionType.none])
                }
            }
        }
    }
    
    func restore()->Bool {
        let fileManager = FileManager.default
        var restored = false
        // Restore the pendingDownloads array by reading the json files in the designated folder.
        if let folderPath = pendingDownloadsFolderURL?.path,
            let contents = try? fileManager.contentsOfDirectory(atPath: folderPath)
        {
            // Convert the contents to contentURLs, which would be the complete URL to the file.
            var contentURLs = contents.map({ urlString -> URL? in
                return pendingDownloadsFolderURL?.appendingPathComponent(urlString)
            })
            // Sort with the latest objects at the end of the array.
            contentURLs.sort(by: { (url1, url2) -> Bool in
                var isAscending = false
                let path1 = url1?.path
                let path2 = url2?.path
                if let date1 = FileManager.getCreationDateForFile(atPath: path1),
                    let date2 = FileManager.getCreationDateForFile(atPath: path2)
                {
                    isAscending = date1.compare(date2) == .orderedAscending
                }
                return isAscending
            })
            // Construct book objects from the content of contentURLs.
            let books = contentURLs.map({ (fileURL) -> Book? in
                var book: Book?
                if let fileURL = fileURL,
                    let jsonString = try? String(contentsOf: fileURL)
                {
                    book = Book(jsonString: jsonString)
                }
                return book
            })
            if !books.isEmpty, let books  = books as? [Book] {
                pendingDownloads = books
                restored = true
            }
        }
        return restored
    }
}
