//
//  BookDownloaderManager.swift
//  Exellency
//
//  Created by Craig Zheng on 13/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

protocol BookDownloaderManagerDelegate {
    func bookDownloaderManager(_ manager: BookDownloaderManager, bookUpdated: Book, percentage: Double)
    func bookDownloaderManager(_ manager: BookDownloaderManager, bookCompleted: Book, success: Bool, error: NSError?)
}

struct BookDownloaderManagerNotification {
    static let UpdatedNotification = "BookDownloaderManagerUpdatedNotification"
    static let CompletedNotification = "BookDownloaderManagerCompletedNotification"
}

class BookDownloaderManager: NSObject {
    // Its a singleton object.
    static let singleton = BookDownloaderManager()
    // Downloading books
    var downloadingBooks = Set<Book>()
    var downloadingTasks = [BookDownloader]()
    // Failed downloads.
    var failedDownloads = Set<Book>()
    // Others
    var delegate: BookDownloaderManagerDelegate?
    
    func downloadBook(_ book: Book)->Bool {
        var addedToDownload = false
        // Make a copy of the incoming book.
        let book = book.copy() as! Book
        // If the book is not already downloading
        if !BookDownloaderManager.isBookDownloading(book) ||
            !BookCacheManager.isBookDownloaded(book) {
            // If the current number of downloaders has exceed the designated maximum concurrent downloads, add the incoming book to pending downloads.
            if downloadingTasks.count >= Configuration.singleton.numberOfDownloaders {
                PendingDownloadManager.singleton.addBookToPending(book)
            } else {
                // Create and retain a book downloader object.
                let bookDownloader = BookDownloader()
                bookDownloader.delegate = self
                addedToDownload = bookDownloader.downloadBook(book)
                if addedToDownload {
                    // If BookDownloader is happy with this book, retain the book downloader.
                    downloadingTasks.append(bookDownloader)
                    // Reset BookCacheManager and other properties.
                    downloadingBooks.insert(book)
                    BookCacheManager.reset()
                    // Remove from failedDownloads.
                    failedDownloads.remove(book)
                }
            }
        }
        return addedToDownload
    }
    
    @discardableResult
    func stopDownloadingBook(_ book: Book)->Bool {
        var stoppedDownloading = false
        if BookDownloaderManager.isBookDownloading(book) {
            let downloaders = downloadingTasks.filter({ (downloader) -> Bool in
                downloader.downloadingBook?.title == book.title
            })
            for downloader in downloaders {
                downloader.stop()
                stoppedDownloading = true
            }
        }
        // Remove books from all sources.
        downloadingBooks.remove(book)
        return stoppedDownloading
    }
    
    func startNext() {
        if PendingDownloadManager.singleton.pendingDownloads.count > 0 {
            for _ in 0 ..< (Configuration.singleton.numberOfDownloaders - downloadingTasks.count) {
                if let nextBook = PendingDownloadManager.singleton.nextBook() {
                    downloadBook(nextBook)
                }
            }
        }
    }
}

extension BookDownloaderManager {
    class func isBookDownloading(_ book: Book)->Bool {
        var downloading = false
        if BookDownloaderManager.singleton.downloadingBooks.contains(book) ||
            PendingDownloadManager.singleton.pendingDownloads.contains(book)
        {
            downloading = true
        }
        return downloading
    }
}

// MARK: BookDownloaderDelegate
extension BookDownloaderManager: BookDownloaderDelegate {
    
    func bookDownloader(_ downloader: BookDownloader, bookCompleted: Book, status: DownloaderStatus, error: NSError?) {
        // The downloading of this book is completed, remove it from self.downloadingBooks.
        downloadingBooks.remove(bookCompleted)
        if let index = downloadingTasks.index(of: downloader) {
            downloadingTasks.remove(at: index)
        }
        // Reset BookCacheManager.
        BookCacheManager.reset()
        // If not successful, add to failed downloads array.
        if status == DownloaderStatus.failed {
            failedDownloads.insert(bookCompleted)
        }
        // Inform delegate.
        DispatchQueue.main.async {
            self.delegate?.bookDownloaderManager(self,
                                                 bookCompleted: bookCompleted,
                                                 success: status == DownloaderStatus.completed,
                                                 error: error)
            NotificationCenter.default.post(name: Notification.Name(rawValue: BookDownloaderManagerNotification.CompletedNotification),
                                                                      object: nil)
        }
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
            self.startNext()
            ProgressHUD.showMessage("\(bookCompleted.title ?? "A book")\nis \(status == .completed ? "completed" : "failed")")
        })
    }
    
    func bookDownloader(_ downloader: BookDownloader, bookUpdated: Book, percentage: Double) {
        // Inform delegate.
        DispatchQueue.main.async { 
            self.delegate?.bookDownloaderManager(self,
                                                 bookUpdated: bookUpdated,
                                                 percentage: percentage)
            NotificationCenter.default.post(name: Notification.Name(rawValue: BookDownloaderManagerNotification.UpdatedNotification),
                                                                      object: nil)
        }
    }
    
}
