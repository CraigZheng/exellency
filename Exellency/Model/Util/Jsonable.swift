//
//  File.swift
//  Exellency
//
//  Created by Craig Zheng on 24/7/17.
//  Copyright © 2017 cz. All rights reserved.
//

import Foundation

protocol Jsonable {
    
    static func jsonDecode(jsonDict: Dictionary<String, AnyObject>) -> Jsonable?
    func jsonEncode() -> String?
    
}
