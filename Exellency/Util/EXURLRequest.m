//
//  EXURLRequest.m
//  Exellency
//
//  Created by Craig Zheng on 6/02/2016.
//  Copyright © 2016 cz. All rights reserved.
//

#import "EXURLRequest.h"

#import "Exellency-Swift.h"

@implementation EXURLRequest

#pragma mark - Getters
//http://civ.atwebpages.com/glype/browse.php?u=

- (instancetype)initWithURL:(NSURL *)URL {
    return [self initWithURL:URL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:20];
}

- (instancetype)initWithURL:(NSURL *)URL cachePolicy:(NSURLRequestCachePolicy)cachePolicy timeoutInterval:(NSTimeInterval)timeoutInterval {
    self = [super initWithURL:URL cachePolicy:cachePolicy timeoutInterval:timeoutInterval];
    [self setValue:@"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:40.0) Gecko/20100101 Firefox/40.1" forHTTPHeaderField:@"User-Agent"];
    DDLogInfo(@"%@", URL.absoluteString);
    return self;
}

@end
