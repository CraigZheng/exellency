//
//  EXConfigureURLRequest.swift
//  Exellency
//
//  Created by Craig on 4/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class EXConfigureURLRequest: NSURLRequest {
    
    convenience init(url URL: URL) {
        var URL = URL
        if var urlComponents = URLComponents(url: URL, resolvingAgainstBaseURL: false)
        {
            urlComponents.queryItems = [URLQueryItem(name: "bundleVersion", value: UIApplication.bundleVersion()),
                                        URLQueryItem(name: "bundleIdentifier", value: UIApplication.bundleIdentifier())]
            URL = urlComponents.url ?? URL
            DLog("\(URL.absoluteString)")
        }
        self.init(url: URL, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 20)
    }
    
    fileprivate override init(url URL: URL, cachePolicy: NSURLRequest.CachePolicy, timeoutInterval: TimeInterval) {
        super.init(url: URL, cachePolicy: cachePolicy, timeoutInterval: timeoutInterval)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
