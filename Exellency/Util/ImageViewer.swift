//
//  ImageViewer.swift
//  Exellency
//
//  Created by Craig Zheng on 28/05/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

class ImageViewer: NSObject {
    private lazy var photoBrowser: MWPhotoBrowser = {
        let photoBrowser = MWPhotoBrowser(delegate: self)
        photoBrowser.displayNavArrows = true; // Whether to display left and right nav arrows on toolbar (defaults to false)
        photoBrowser.displaySelectionButtons = false; // Whether selection buttons are shown on each image (defaults to false)
        photoBrowser.zoomPhotosToFill = false; // Images that almost fill the screen will be initially zoomed to fill (defaults to true)
        photoBrowser.alwaysShowControls = false; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to false)
        photoBrowser.enableGrid = false; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to true)
        photoBrowser.startOnGrid = false; // Whether to start on the grid of thumbnails instead of the first photo (defaults to false)
        photoBrowser.delayToHideElements = 4
        photoBrowser.enableSwipeToDismiss = false; // dont dismiss
        photoBrowser.displayActionButton = true;
        photoBrowser.hidesBottomBarWhenPushed = true;

        return photoBrowser
    }()
    
    private var sourceBook: Book?
    private var pagesInProgress = [Page]()
    
    func showBook(book: Book, withIndex index: Int) {
        sourceBook = book
        let photoBrowserNavigationController = UINavigationController(rootViewController:photoBrowser)
        UIApplication.sharedApplication().keyWindow?.rootViewController?
            .presentViewController(photoBrowserNavigationController, animated: true, completion: nil)
        photoBrowser.reloadData()
        photoBrowser.setCurrentPhotoIndex(UInt(index))
    }
}

extension ImageViewer: MWPhotoBrowserDelegate {
    func photoBrowser(photoBrowser: MWPhotoBrowser!, photoAtIndex index: UInt) -> MWPhotoProtocol! {
        var photo: MWPhoto?
        if let sourceBook = sourceBook {
            let page = sourceBook.pages[Int(index)]
            if let imageURL = page.imageURL {
                for fileURL in BookCache.contentsOfFolderForBook(sourceBook, shouldSort: true) ?? [] {
                    if fileURL.lastPathComponent == imageURL.lastPathComponent,
                        let filePath = fileURL.path {
                        if let image = UIImage(contentsOfFile: filePath) {
                            photo = MWPhoto(image: image )
                        }
                        break
                    }
                }
                // If the BookCache has been searched and a photo object cannot be created.
                if photo == nil {
                    photo = MWPhoto(URL: imageURL)
                }
            } else {
                if !pagesInProgress.contains(page) {
                    pagesInProgress.append(page)
                    sourceBook.updateImageForPage(page, completionHandler: { [weak weakSelf = self](success, error) in
                        if let weakSelf = weakSelf {
                            weakSelf.pagesInProgress.removeObject(page)
                            if success {
                                if weakSelf.photoBrowser.currentIndex == index {
                                    weakSelf.photoBrowser.reloadData()
                                } else {
                                    DLog("No need to reload")
                                }
                            }
                        } else {
                            DLog(error)
                        }
                    })
                }
            }
        }
        return photo ?? MWPhoto()
    }
    
    func photoBrowser(photoBrowser: MWPhotoBrowser!, actionButtonPressedForPhotoAtIndex index: UInt) {
        if let photo = self.photoBrowser(photoBrowser, photoAtIndex: index),
            let tempFileURL = BookCache.libraryFolderURL?.URLByAppendingPathComponent("tempImage.jpg") {
            if let data = UIImageJPEGRepresentation(photo.underlyingImage, 0.99) {
                data.writeToURL(tempFileURL, atomically: false)
                let documentInteractionController = UIDocumentInteractionController(URL:tempFileURL)
                if let presentingView = UIApplication.sharedApplication().keyWindow?.rootViewController?.presentedViewController?.view {
                    documentInteractionController.presentOptionsMenuFromRect(presentingView.frame, inView:presentingView, animated:true)
                }
            }
        }

    }
    
    func photoBrowserDidFinishModalPresentation(photoBrowser: MWPhotoBrowser!) {
        photoBrowser.dismissViewControllerAnimated(true, completion:nil)
    }
    
    func numberOfPhotosInPhotoBrowser(photoBrowser: MWPhotoBrowser!) -> UInt {
        if let sourceBook = sourceBook {
            return UInt(sourceBook.pages.count)
        }
        return 0
    }
}