//
//  NSFileManager+DateUtil.m
//  Exellency
//
//  Created by Craig Zheng on 3/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

#import "NSFileManager+DateUtil.h"
#import "sys/stat.h"

@implementation NSFileManager (DateUtil)

// Copied from http://stackoverflow.com/questions/1523793/get-directory-contents-in-date-modified-order and slightly modified to adapt ARC.
+ (NSDate*) getModificationDateForFileAtPath:(NSString*)path {
    struct tm* date; // create a time structure
    struct stat attrib; // create a file attribute structure
    
    stat([path UTF8String], &attrib);   // get the attributes of afile.txt
    
    date = gmtime(&(attrib.st_mtime));  // Get the last modified time and put it into the time structure
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setSecond:   date->tm_sec];
    [comps setMinute:   date->tm_min];
    [comps setHour:     date->tm_hour];
    [comps setDay:      date->tm_mday];
    [comps setMonth:    date->tm_mon + 1];
    [comps setYear:     date->tm_year + 1900];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *modificationDate = [[cal dateFromComponents:comps] addTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT]];
    
    return modificationDate;
}

+ (NSDate*) getCreationDateForFileAtPath:(NSString*)path {
    struct tm* date; // create a time structure
    struct stat attrib; // create a file attribute structure
    
    stat([path UTF8String], &attrib);   // get the attributes of afile.txt
    
    date = gmtime(&(attrib.st_birthtime));  // Get the last modified time and put it into the time structure
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setSecond:   date->tm_sec];
    [comps setMinute:   date->tm_min];
    [comps setHour:     date->tm_hour];
    [comps setDay:      date->tm_mday];
    [comps setMonth:    date->tm_mon + 1];
    [comps setYear:     date->tm_year + 1900];
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *modificationDate = [[cal dateFromComponents:comps] addTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMT]];
    
    return modificationDate;
}

@end
