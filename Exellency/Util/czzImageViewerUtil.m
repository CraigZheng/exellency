//
//  czzImageViewerUtil.m
//  CustomImageBoardViewer
//
//  Created by Craig on 7/11/2014.
//  Copyright (c) 2014 Craig. All rights reserved.
//

#import "czzImageViewerUtil.h"
#import "MWPhotoBrowser.h"
#import "SDImageCache.h"
#import "Exellency-Swift.h"
#import <CocoaLumberjack/CocoaLumberjack.h>

@interface czzImageViewerUtil () <MWPhotoBrowserDelegate>
@property (nonatomic, strong) Book *sourceBook;
@property (nonatomic, strong) NSMutableOrderedSet<Page *> *pagesInProgress;
@end

@implementation czzImageViewerUtil
@synthesize photoBrowser;
@synthesize photoBrowserDataSource;
@synthesize documentInteractionController;
@synthesize photoBrowserNavigationController;

-(instancetype)init {
    self = [super init];
    if (self) {
        photoBrowserDataSource = [NSMutableArray new];
        [self prepareMWPhotoBrowser];
    }
    return self;
}

-(void)showPhoto:(NSURL *)photoPath {
    [self prepareMWPhotoBrowser];
    if (!photoBrowserDataSource)
        photoBrowserDataSource = [NSMutableArray new];
    if (![photoBrowserDataSource containsObject:photoPath])
        [photoBrowserDataSource addObject:photoPath];
    [photoBrowser setCurrentPhotoIndex: [photoBrowserDataSource indexOfObject:photoPath]];
    [self show];
}

-(void)showPhotoWithImage:(UIImage *)image {
    if (image) {
        [self prepareMWPhotoBrowser];
        photoBrowserDataSource = [@[image] mutableCopy];
        [photoBrowser setCurrentPhotoIndex:0];
        [self show];
    }
}

-(void)showPhotos:(NSArray *)photos withIndex:(NSInteger)index {
    if (photos.count > 0) {
        [self prepareMWPhotoBrowser];
        photoBrowserDataSource = [NSMutableArray arrayWithArray:photos];
        [photoBrowser setCurrentPhotoIndex:index];
        [self show];
    }
    else {
        DLog(@"Either photos or view controller is nil");
    }
}

- (void)showBook:(Book *)book withIndex:(NSInteger)index {
    if (book.pages.count > 0 || [BookCacheManager isBookDownloaded:book]) {
        self.sourceBook = book;
        [self prepareMWPhotoBrowser];
        [self prepareBookPages];
        [photoBrowser setCurrentPhotoIndex:index];
        [self show];
    }
    else {
        DLog(@"Either photos or view controller is nil");
    }
}

-(void)show {
    UIViewController *rootViewController = [UIApplication topViewController];
    if (rootViewController.navigationController) {
        [rootViewController.navigationController pushViewController:photoBrowser animated:YES];
    } else {
        photoBrowserNavigationController = [[UINavigationController alloc] initWithRootViewController:photoBrowser];
        photoBrowserNavigationController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
        [rootViewController presentViewController:photoBrowserNavigationController animated:YES completion:nil];
    }
}

-(void)prepareMWPhotoBrowser {
    photoBrowser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    //    browser.displayActionButton = NO; // Show action button to allow sharing, copying, etc (defaults to YES)
    photoBrowser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    photoBrowser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    photoBrowser.zoomPhotosToFill = NO; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    photoBrowser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    photoBrowser.enableGrid = YES; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    photoBrowser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    photoBrowser.delayToHideElements = 8.0;
    photoBrowser.enableSwipeToDismiss = NO; // dont dismiss
    photoBrowser.displayActionButton = YES;
    photoBrowser.hidesBottomBarWhenPushed = YES;

}

#pragma mark - Book pages.

- (void)prepareBookPages {
    // Retrieve already downloaded files.
    NSMutableArray *pageURLs = [NSMutableArray new];
    for (NSURL *fileURL in [BookCache contentsOfDownloadedFolderForBook:self.sourceBook shouldSort:YES]) {
        if (![fileURL.pathExtension isEqualToString:@"json"] && ![fileURL.pathExtension isEqualToString:@"txt"]) {
            [pageURLs addObject:fileURL];
        }
    }
    photoBrowserDataSource = pageURLs;
    // For the remaining pages, get straint from self.sourceBook.
    if (self.sourceBook.pages.count > photoBrowserDataSource.count) {
        NSArray *pagesArray = [self.sourceBook.pages subarrayWithRange:NSMakeRange(photoBrowserDataSource.count,
                                                                                   self.sourceBook.pages.count - photoBrowserDataSource.count)];
        [photoBrowserDataSource addObjectsFromArray:pagesArray];
    }
}

#pragma mark - Getter

- (NSMutableOrderedSet<Page *> *)pagesInProgress {
    if (!_pagesInProgress) {
        _pagesInProgress = [NSMutableOrderedSet new];
    }
    return _pagesInProgress;
}

#pragma mark - MWPhotoBrowserDelegate
-(id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    @try {
        id source = [photoBrowserDataSource objectAtIndex:index];
        MWPhoto *photo;
        if ([source isKindOfClass:[UIImage class]]) {
            photo = [MWPhoto photoWithImage:source];
        }
        else if ([source isKindOfClass:[Page class]]) {
            DLog(@"%s: photo at index %ld is an instance of Page.", __PRETTY_FUNCTION__, (long)index);
            Page *page = source;
            if (page.imageURL) {
                // Construct a MWPhoto object with the imageURL.
                photo = [MWPhoto photoWithURL:page.imageURL];
            } else {
                // Use weak reference - in case self is dealloced during the update image stage.
                __weak typeof(self) weakSelf = self;
                if (![weakSelf.pagesInProgress containsObject:page]) {
                    [weakSelf.pagesInProgress addObject:page];
                    [weakSelf.sourceBook updateImageForPage:page
                                      completionHandler:^(BOOL success, NSError * _Nullable error) {
                                          if (!weakSelf) {
                                              DLog(@"%s: completed but self is no longer in memeory.", __PRETTY_FUNCTION__);
                                          }
                                          // Remove the current page from the in progress array.
                                          [weakSelf.pagesInProgress removeObject:page];
                                          DLog(@"%s: page.imageURL updated.", __PRETTY_FUNCTION__);
                                          if (success) {
                                              if (weakSelf.photoBrowser.currentIndex == index) {
                                                  [weakSelf.photoBrowser reloadData];
                                              } else {
                                                  DLog(@"no need to reload");
                                              }
                                          } else {
                                              DLog(@"%s: update failed.", __PRETTY_FUNCTION__);
                                          }
                                      }];
                    DLog(@"%s: need to download page.imageURL", __PRETTY_FUNCTION__);
                } else {
                    DLog(@"%s: page.imageURL is currently being downloaded.", __PRETTY_FUNCTION__);
                }
            }
        }
        else {
            photo = [MWPhoto photoWithURL:([source isKindOfClass:[NSURL class]] ? source : [NSURL fileURLWithPath:source])];
        }
        return photo;
    }
    @catch (NSException *exception) {
        DLog(@"%@", exception);
    }
    return nil;
}

- (id<MWPhoto>)photoBrowser:(MWPhotoBrowser *)browser thumbPhotoAtIndex:(NSUInteger)index {
    MWPhoto *thumbnailPhoto;
    if (index < self.sourceBook.pages.count) {
        Page *page = self.sourceBook.pages[index];
        thumbnailPhoto = [MWPhoto photoWithImage:page.thumbnail];
    } else {
        // Thumbnail image not available, get the full size image instead.
        thumbnailPhoto = [self photoBrowser:browser photoAtIndex:index];
    }
    return thumbnailPhoto;
}

-(void)photoBrowser:(MWPhotoBrowser *)browser actionButtonPressedForPhotoAtIndex:(NSUInteger)index {
    id source = [photoBrowserDataSource objectAtIndex:index];
    if ([source isKindOfClass:[NSURL class]]){
        documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath: [[SDImageCache sharedImageCache] defaultCachePathForKey:[source absoluteString]]]];
    }
    else if ([source isKindOfClass:[Page class]]) {
        documentInteractionController = [UIDocumentInteractionController interactionControllerWithURL:[NSURL fileURLWithPath: [[SDImageCache sharedImageCache] defaultCachePathForKey:[(Page*)source imageURL].absoluteString]]];
    }
    else {
        DLog(@"%@ is not a URL", source);
        return;
    }
    UIView *viewToShowDocumentInteractionController;
    if (photoBrowserNavigationController)
        viewToShowDocumentInteractionController = photoBrowserNavigationController.view;
    else
        viewToShowDocumentInteractionController = photoBrowser.view;
    //ipad
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad )
    {
        [documentInteractionController presentOptionsMenuFromRect:CGRectZero inView:self.photoBrowserNavigationController.view animated:YES];
    }
    //iphone
    else {
        [documentInteractionController presentOptionsMenuFromRect:self.photoBrowserNavigationController.view.frame inView:viewToShowDocumentInteractionController animated:YES];
    }
}

-(void)photoBrowserDidFinishModalPresentation:(MWPhotoBrowser *)photoBrowser {
    [photoBrowserNavigationController dismissViewControllerAnimated:YES completion:nil];
}

-(NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return photoBrowserDataSource.count;
}

#pragma mark - Book updated handler.

- (void)notifyBookUpdated:(Book *)updatedBook {
    DLog(@"Book updated.");
    if (updatedBook == self.sourceBook) {
        [self prepareBookPages];
        [self.photoBrowser reloadData];
    }
}

@end
