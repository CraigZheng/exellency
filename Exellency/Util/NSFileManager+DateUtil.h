//
//  NSFileManager+DateUtil.h
//  Exellency
//
//  Created by Craig Zheng on 3/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSFileManager (DateUtil)

+ (nullable NSDate*) getModificationDateForFileAtPath:(NSString*)path;
+ (nullable NSDate*) getCreationDateForFileAtPath:(NSString*)path;

@end
