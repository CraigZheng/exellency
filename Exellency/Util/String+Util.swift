//
//  String+Util.swift
//  Exellency
//
//  Created by Craig Zheng on 8/09/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import Foundation

extension String {
    
    func urlEscapedString() -> String? {
        return self.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    }
    
}
