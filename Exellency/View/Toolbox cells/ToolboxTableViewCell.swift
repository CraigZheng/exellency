//
//  ToolboxTableViewCell.swift
//  Exellency
//
//  Created by Craig Zheng on 3/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

enum ToolboxCellAction: Int {
    case startReading = 1;
    case delete = 2;
    case stopDownload = 3;
    case restartDownload = 4
}

class ToolboxTableViewCell: UITableViewCell {
    static let favouriteCellIdentifier = "toolbox_favourite_cell_identifier"
    static let downloadingCellIdentifier = "toolbox_downloading_cell_identifier"
    static let downloadedCellIdentifier = "toolbox_downloaded_cell_identifier"
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    
    var book: Book! {
        didSet {
            if let thumb = book.thumb {
                previewImageView?.sd_setImage(with: thumb as URL!)
            } else {
                previewImageView.image = nil
            }
            nameLabel?.text = book.title
            categoryLabel?.text = Configuration.singleton.debug ? book.category?.rawValue : ""
            categoryLabel?.textColor = book.category?.colour()
        }
    }

}
