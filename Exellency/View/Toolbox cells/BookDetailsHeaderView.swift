//
//  BookDetailsHeaderView.swift
//  Exellency
//
//  Created by Craig Zheng on 9/7/17.
//  Copyright © 2017 cz. All rights reserved.
//

import UIKit

class BookDetailsHeaderView: UICollectionReusableView {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var artistLabel: UILabel?
    @IBOutlet weak var uploaderLabel: UILabel!
    @IBOutlet weak var ratingLabel: UILabel?
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var fileCountLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var star1ImageView: UIImageView!
    @IBOutlet weak var star2ImageView: UIImageView!
    @IBOutlet weak var star3ImageView: UIImageView!
    @IBOutlet weak var star4ImageView: UIImageView!
    @IBOutlet weak var star5ImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
