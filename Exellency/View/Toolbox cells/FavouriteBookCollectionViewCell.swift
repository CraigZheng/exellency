//
//  FavouriteBookCollectionViewCell.swift
//  Exellency
//
//  Created by Craig Zheng on 8/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class FavouriteBookCollectionViewCell: UICollectionViewCell {
    
    var book: Book? {
        didSet {
            if let book = book {
                if let thumb = book.thumb {
                    previewImageView.sd_setImage(with: thumb as URL!)
                } else {
                    previewImageView.image = nil
                }
                titleLabel.text = book.title
                // The selected/unselected state of the button.
                favouriteButton.isSelected = FavouriteManager.singleton.isBookInFavourite(book) ? true : false
            }
        }
    }
    
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var favouriteButton: UIButton! {
        didSet {
            favouriteButton.setImage(UIImage(named: "star_outlined.png")!.withRenderingMode(.alwaysTemplate), for: UIControlState())
            favouriteButton.setImage(UIImage(named: "star_filled.png"), for: .selected)
        }
    }
    
}
