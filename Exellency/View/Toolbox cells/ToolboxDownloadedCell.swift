//
//  ToolboxDownloadedCell.swift
//  Exellency
//
//  Created by Craig Zheng on 3/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class ToolboxDownloadedCell: ToolboxTableViewCell {

    @IBOutlet weak var startReadingButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
}
