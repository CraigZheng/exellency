//
//  ToolboxDownloadingCell.swift
//  Exellency
//
//  Created by Craig Zheng on 3/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class ToolboxDownloadingCell: ToolboxTableViewCell {

    enum DownloadingCellStatus {
        case downloading
        case updating
        case pending
        case stopped
    }
    
    @IBOutlet weak var restartButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    
    var downloadStatus: DownloadingCellStatus = .downloading {
        didSet {
            // Set all UI elements to be visible.
            restartButton.isHidden = false
            stopButton.isHidden = false
            progressView.isHidden = false
            progressLabel.isHidden = false
            switch downloadStatus {
            case .downloading, .updating:
                restartButton.isHidden = true
                progressView.progressTintColor = UIColor.green
            case .pending:
                progressView.progressTintColor = UIColor.yellow
                restartButton.isHidden = true
            case .stopped:
                // Pretty much the default case.
                progressView.progressTintColor = UIColor.red
                stopButton.isHidden = true
                break
            }
        }
    }
    
}
