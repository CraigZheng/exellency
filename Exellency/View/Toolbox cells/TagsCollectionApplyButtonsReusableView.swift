//
//  TagsCollectionApplyButtonsReusableView.swift
//  Exellency
//
//  Created by Craig on 10/9/17.
//  Copyright © 2017 cz. All rights reserved.
//

import UIKit

class TagsCollectionApplyButtonsReusableView: UICollectionReusableView {
        
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var applyButton: RoundCornerBorderedButton!
    @IBOutlet weak var resetButton: UIButton!
    
}
