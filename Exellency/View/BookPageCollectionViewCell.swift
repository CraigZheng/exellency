//
//  BookPageCollectionViewCell.swift
//  Exellency
//
//  Created by Craig Zheng on 14/02/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit


class BookPageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    class var identifier: String {
        return "BookPageCollectionViewCellIdentifier"
    }
}
