//
//  DownloadBarButtonItem.swift
//  Exellency
//
//  Created by Craig on 22/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class DownloadBarButtonItem: UIBarButtonItem {
    
    class func loadingButtonItem()-> DownloadBarButtonItem {
        let animatingGifData = try? Data(contentsOf: Bundle.main.url(forResource: "loading", withExtension: "gif")!)
        let animatedGif = UIImage.animatedImage(withAnimatedGIFData: animatingGifData)
        // Assign the image to a button.
        let imageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageButton.setImage(animatedGif, for: UIControlState())
        let barButton = DownloadBarButtonItem(customView: imageButton)
        return barButton
    }
    
    class func standbyButtonItem()-> DownloadBarButtonItem {
        let image = UIImage(named: "download.png")?.withRenderingMode(.alwaysTemplate)
        let imageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageButton.setImage(image, for: UIControlState())
        let barButton = DownloadBarButtonItem(customView: imageButton)
        return barButton
    }
    
    class func loadedButtonItem()-> DownloadBarButtonItem {
        let image = UIImage(named: "ok.png")?.withRenderingMode(.alwaysTemplate)
        let imageButton = UIButton(frame: CGRect(x: 0, y: 0, width: 30, height: 30))
        imageButton.setImage(image, for: UIControlState())
        let barButton = DownloadBarButtonItem(customView: imageButton)
        return barButton
    }
    
    // Assign the targets and actions to the custom view - if its a button.
    override var target: AnyObject? {
        didSet {
            if let button = customView as? UIButton {
                if action != nil {
                    button.removeTarget(nil, action: nil, for: .touchUpInside)
                    button.addTarget(target, action: action!, for: .touchUpInside)
                }
            }
        }
    }
    
    override var action: Selector? {
        didSet {
            if let button = customView as? UIButton {
                if action != nil {
                    button.removeTarget(nil, action: nil, for: .touchUpInside)
                    button.addTarget(target, action: action!, for: .touchUpInside)
                }
            }
        }
    }
}
