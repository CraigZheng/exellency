//
//  CategoryTableViewCell.swift
//  Exellency
//
//  Created by Craig Zheng on 21/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class CategoryTableViewCell: UITableViewCell {
    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var artistLabel: UILabel?
    @IBOutlet weak var uploaderLabel: UILabel?
    @IBOutlet weak var ratingLabel: UILabel?
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var fileCountLabel: UILabel?
    @IBOutlet weak var dateLabel: UILabel!
    
    
    static let cellIdentifier = "categoryTableViewCellIdentifier"
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
