//
//  AppDelegate.h
//  Exellency
//
//  Created by Craig Zheng on 4/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

