//
//  LeftCategoryPickerTableViewController.swift
//  Exellency
//
//  Created by Craig Zheng on 2/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

protocol LeftCategoryPickerTableViewControllerDelegate {
    func categoryPicker(_ picker: LeftCategoryPickerTableViewController, pickedCategory: Category)
}

class LeftCategoryPickerTableViewController: UITableViewController {
    let cellIdentifier = "category_cell_identifier"
    var list: List?
    var delegate: LeftCategoryPickerTableViewControllerDelegate?
    
    var availableCategory: [Category] {
        if Configuration.singleton.debug {
            return Category.allValues
        } else {
            return [Category.nonh]
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.scrollsToTop = false
        // Make the tableview appear below the status bar.
        tableView.contentInset = UIEdgeInsetsMake(UIApplication.shared.statusBarFrame.height, 0, 0, 0)
        if list != nil {
            title = list!.category.rawValue
        }
        // Added Category debug did change notification handler.
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Configuration.debugChangedNotification), object: nil, queue: OperationQueue.main) { notification in
            self.tableView.reloadData()
        }
    }
    
    // MARK: - Table view data source
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return availableCategory.count
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.textLabel?.text = availableCategory[indexPath.row].rawValue
        cell.textLabel?.backgroundColor = UIColor.clear
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // If delegate exists and the selected section is category section.
        if let delegate = delegate {
            delegate.categoryPicker(self, pickedCategory: availableCategory[indexPath.row])
        }
        tableView.reloadData()
    }

    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0.5;
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        // Return a white separator style footer view.
        let footerView = UIView()
        footerView.frame = CGRect(x: 0, y: 0, width: tableView.frame.width, height: self.tableView(tableView, heightForFooterInSection: section))
        footerView.backgroundColor = UIColor.white
        return footerView
    }
}
