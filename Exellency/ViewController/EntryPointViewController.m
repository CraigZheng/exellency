//
//  EntryPointViewController.m
//  Exellency
//
//  Created by Craig Zheng on 2/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

#import "EntryPointViewController.h"

#import "Exellency-Swift.h"

@interface EntryPointViewController ()

@end

@implementation EntryPointViewController

- (instancetype)initWithCoder:(NSCoder *)coder
{
    UIViewController *centreViewController = [[UIStoryboard storyboardWithName:@"Main"
                                                                        bundle:[NSBundle mainBundle]]
                                              instantiateViewControllerWithIdentifier:@"centre_navigation_controller"];
    centreViewController.view.tintColor = [UIColor whiteColor]; // This would set the navigation bar colour to white.
    if (centreViewController) {
        self = [self initWithCenterViewController:centreViewController];
    }
    assert(self);
    return self;
}

@end
