//
//  ToolSelectionTableViewController.swift
//  Exellency
//
//  Created by Craig Zheng on 9/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

import GoogleMobileAds

class ToolSelectionTableViewController: UITableViewController {
    fileprivate let remoteActionTableViewDelegate = RemoteActionTableViewDelegate()

    @IBOutlet weak var favouriteCell: UITableViewCell!
    @IBOutlet weak var downloadingCell: UITableViewCell!
    @IBOutlet weak var downloadedCell: UITableViewCell!
    @IBOutlet weak var appVersionLabel: UILabel!
    @IBOutlet weak var remoteActionTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var remoteActionTableView: UITableView! {
        didSet {
            remoteActionTableView.delegate = remoteActionTableViewDelegate
            remoteActionTableView.dataSource = remoteActionTableViewDelegate
            remoteActionTableView.reloadData()
        }
    }
    @IBOutlet weak var tableFooterView: UIView! {
        didSet {
            tableFooterView.frame.size.height = 30
        }
    }
    @IBOutlet weak var adBannerView: GADBannerView! {
        didSet {
            // Initially hidden.
            adBannerView.isHidden = true
            adBannerView.adUnitID = "ca-app-pub-2081665256237089/6678731257"
            adBannerView.rootViewController = self
            adBannerView.delegate = self
            
            let request = GADRequest()
            request.testDevices = ["4fa1b332e0290930b2ae511c65ff8947"]
            #if DEBUG
                request.testDevices = [kGADSimulatorID]
            #endif
            adBannerView.load(request)
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Modified the transparentFooterView a bit, so it shows the current app version.
        let dictionary = Bundle.main.infoDictionary!
        if let version = dictionary["CFBundleShortVersionString"] as? String,
            let build = dictionary["CFBundleVersion"] as? String
        {
            appVersionLabel.text = "Version: \(version)(\(build))"
        } else {
            // Cannot read the version info, set the label text to nil.
            appVersionLabel.text = ""
        }
        // Disable ad banner.
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: AdConfiguration.adConfigurationUpdatedNotification),
                                                                object: nil,
                                                                queue: OperationQueue.main) { (notification) in
                                                                    if !AdConfiguration.singleton.shouldDisplayAds {
                                                                        self.adBannerView.isHidden = true
                                                                    } else {
                                                                        self.adBannerView.isHidden = false
                                                                    }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        // Manager details.
        favouriteCell.detailTextLabel?.text = "\(FavouriteManager.singleton.favouriteBooks.count) object(s)"
        downloadedCell.detailTextLabel?.text = "\(BookCacheManager.sharedInstance.downloadedBooks.count) object(s)"
        // DownloadingBooks includes not only the active downloading books, also the partially downloaded books and the books in pending download.
        var downloadingBooks = Array(BookDownloaderManager.singleton.downloadingBooks)
        let pendingDownloads = PendingDownloadManager.singleton.pendingDownloads
        if var partiallyDownloadedBooks = BookCacheManager.partiallyDownloadedBooks().array as? [Book] {
            partiallyDownloadedBooks = partiallyDownloadedBooks.filter({ book -> Bool in
                // Not being included in both Downloading and Pending managers.
                return !BookDownloaderManager.isBookDownloading(book) && !PendingDownloadManager.singleton.pendingDownloads.contains(book)
            })
            downloadingBooks = Array(downloadingBooks + pendingDownloads + partiallyDownloadedBooks)
        } else {
            downloadingBooks = Array(downloadingBooks + pendingDownloads)
        }
        downloadingCell.detailTextLabel?.text = "\(downloadingBooks.count) object(s)"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var selectedCollection = BookCollection.favourite
        if let sender = sender as? UITableViewCell {
            if sender === favouriteCell {
                selectedCollection = .favourite
            } else if sender === downloadingCell {
                selectedCollection = .downloading
            } else if sender === downloadedCell {
                selectedCollection = .downloaded
            }
        }
        if let bookCollectionTableViewController = segue.destination as? BookCollectionTableViewController {
            bookCollectionTableViewController.selectedCollection = selectedCollection
        }
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            return UITableViewAutomaticDimension
        } else {
            return CGFloat(Configuration.singleton.remoteActions.count * 44)
        }
    }
}

// MARK: GADBannerViewDelegate
extension ToolSelectionTableViewController: GADBannerViewDelegate {
    
    func adViewWillLeaveApplication(_ bannerView: GADBannerView!) {
        AdConfiguration.singleton.clickedAd()
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!) {
        tableFooterView.frame.size.height = 80
        adBannerView.isHidden = false
    }
}

// MARK: UITableViewDataSource, UITableViewDelegate 

class RemoteActionTableViewDelegate: NSObject, UITableViewDataSource, UITableViewDelegate {
    let actionCellIdentifier = "actionCellIdentifier"
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let count = Configuration.singleton.remoteActions.count
        return count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: actionCellIdentifier, for: indexPath)
        let dict = Configuration.singleton.remoteActions[indexPath.row]
        cell.textLabel?.text = dict.keys.first
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let dict = Configuration.singleton.remoteActions[indexPath.row]
        if let key = dict.keys.first,
            let object = dict[key]
        {
            if let actionURL = URL(string: object), UIApplication.shared.canOpenURL(actionURL) {
                UIApplication.shared.openURL(actionURL)
            }
        }
    }
    
}
