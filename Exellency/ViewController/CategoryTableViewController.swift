//
//  CategoryTableViewController.swift
//  Exellency
//
//  Created by Craig Zheng on 21/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

import GoogleMobileAds
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func >= <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l >= r
  default:
    return !(lhs < rhs)
  }
}


// Banner 1: ca-app-pub-2081665256237089/9625524450

class CategoryTableViewController: UITableViewController, LeftCategoryPickerTableViewControllerDelegate, UISearchBarDelegate {
    // MARK: UI elements.
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet var leftBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var adDescriptionLabel: UILabel! {
        didSet {
            adDescriptionLabel.text = ""
        }
    }
    @IBOutlet weak var tableHeaderView: UIView? {
        didSet {
            // Initially hide the ad banner.
            toggleAdBanner(false)
        }
    }
    @IBOutlet weak var adBannerView: GADBannerView! {
        didSet {
            adBannerView.adUnitID = "ca-app-pub-2081665256237089/9625524450"
            adBannerView.rootViewController = self
            adBannerView.delegate = self
        }
    }
    
    // MARK: properties.
    fileprivate var bottomPullManager: MNMBottomPullToRefreshManager!
    fileprivate var contentInset: UIEdgeInsets!
    let showBookDetailSegueIdentifier = "showBookDetail"
    var selectedIndexPath: IndexPath?
    static var postDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    }()
    var list = List()
    // Source: if search mode, return the search object.
    var source: List {
        if searchMode {
            return search
        } else {
            return list
        }
    }
    var search = Search()
    var searchMode = false {
        didSet {
            if !searchMode {
                searchBar.resignFirstResponder()
            }
            if Configuration.singleton.debug {
                title = source.category.rawValue
            } else {
                title = ""
            }
            tableView.reloadData()
        }
    }
    var predefinedSearchKeyword: String?
    var books: [Book] {
        get {
            return source.books
        }
        set {
            source.books = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Set height of tableview to automatic. 160 is the estimatedRow height.
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 140
        
        // Register Nib.
        tableView.register(UINib(nibName: "CategoryTableViewCell", bundle: Bundle.main), forCellReuseIdentifier: CategoryTableViewCell.cellIdentifier)
        // Register UIRefreshControl target and action.
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(CategoryTableViewController.refreshSource), for: .valueChanged)
        self.refreshControl = refreshControl
        // Update with server.
        list.category = Category.nonh // The default value.
        refreshSource()
        // Title of the list.
        if Configuration.singleton.debug {
            title = source.category.rawValue
        }
        // Refresh control for the bottom.
        bottomPullManager = MNMBottomPullToRefreshManager(pullToRefreshViewHeight: 60, tableView: self.tableView, with: self)

        // AdConfiguration notification handler.
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: AdConfiguration.adConfigurationUpdatedNotification),
                                                                object: nil,
                                                                queue: OperationQueue.main) { (notification) in
                                                                    self.toggleAdBanner(AdConfiguration.singleton.shouldDisplayAds)
                                                                    self.attemptLoadRequest()
                                                                    if !AdConfiguration.singleton.shouldDisplayAds {
                                                                        ProgressHUD.showMessage("Advertisement is disabled.")
                                                                    }
        }
        // Configuration update handler.
        NotificationCenter.default.addObserver(forName: NSNotification.Name(rawValue: Configuration.updatedNotification),
                                                                object: nil,
                                                                queue: OperationQueue.main) { (notification) in
                                                                    if Configuration.singleton.updatedWithServer {
                                                                        if self.source.books.isEmpty {
                                                                            self.refreshSource()
                                                                        }
                                                                    } else {
                                                                        UIAlertView.init(title: "Server Not Responding", message: "The server is not responding properly, please check your network connection, and try again later.", delegate: nil, cancelButtonTitle: "OK").show()
                                                                    }
                                                                    self.disableLeftMenu()
                                                                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(0.2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                                                                        // If self is visible.
                                                                        if Configuration.singleton.debug && Configuration.singleton.updatedWithServer {
                                                                            if self.navigationController?.viewControllers.last == self {
                                                                                self.enableLeftMenu()
                                                                            }
                                                                        }
                                                                    })
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        disableLeftMenu()
        // Remove observer.
        NotificationCenter.default.removeObserver(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        // Record the initial contentInset.
        if contentInset == nil {
            contentInset = tableView.contentInset
        }
        if Configuration.singleton.debug && Configuration.singleton.updatedWithServer {
            enableLeftMenu()
        } else {
            disableLeftMenu()
        }
        // If predefinedSearchKeyword is present, consume it.
        if let predefinedSearchKeyword = predefinedSearchKeyword, !predefinedSearchKeyword.isEmpty {
            searchBar.text = predefinedSearchKeyword
            searchWithKeyword(predefinedSearchKeyword)
        }
        predefinedSearchKeyword = nil
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        bottomPullManager.relocatePullToRefreshView()
    }
    
    // MARK: Refresh.
    func refreshSource() {
        DLog("")
        if Configuration.singleton.updatedWithServer {
            showLoading()
            source.updateWithCategory(source.category, page: 1) { (success, error) -> Void in
                self.tableView.reloadData()
                self.hideLoading()
            }
        }
    }
    
    func searchWithKeyword(_ keyword: String) {
        if Configuration.singleton.updatedWithServer {
            showLoading()
            // Turn on search mode.
            searchMode = true
            search.updateWithKeyword(keyword,
                                     page: 1,
                                     completionHandler: { (success, error) -> Void in
                                        DLog("Completed searching with: \(keyword)")
                                        self.title = keyword
                                        self.tableView.reloadData()
                                        self.hideLoading()
            })
        }
    }
    
    func loadMore() {
        DLog("")
        if Configuration.singleton.updatedWithServer {
            // Trigger list to load more.
            showLoading()
            if let page = source.page {
                source.updateWithPage(page + 1) { (success, error) -> Void in
                    self.refreshControl?.endRefreshing()
                    self.tableView.reloadData()
                    self.hideLoading()
                    self.bottomPullManager.tableViewReloadFinished()
                    self.tableView.contentInset = self.contentInset
                    DLog("loadMore completed")
                }
            } else {
                refreshSource()
            }
        }
    }
        
    // MARK: Navigation
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool {
        var shouldPerform = true
        if identifier == showBookDetailSegueIdentifier,
            let selectedIndexPath = selectedIndexPath
        {
            if let reasons = BlacklistManager.singleton.blacklistReasonsForBook(books[selectedIndexPath.row]) {
                shouldPerform = false
                UIAlertView(title: "Cannot Open",
                            message: "Community has blacklisted this title.\nReasons:\n\(reasons.joined(separator: ", "))",
                            delegate: nil,
                            cancelButtonTitle: "OK").show()
            }
        }
        return shouldPerform
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let selectedIndexPath = selectedIndexPath,
            let destinationViewController = segue.destination as? BookDetailViewController {
            let book = books[selectedIndexPath.row]
            destinationViewController.book = book;
        }
    }
    
    // MARK: UI actions.
    
    @IBAction func menuButtonAction(_ sender: AnyObject) {
        viewDeckController.toggleLeftView()
    }
    
    @IBAction func unwindToCategory(_ sender: Any) {
        // Unwind segue.
    }
    
}

    // MARK: - Table view data source
    extension CategoryTableViewController {
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return books.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if indexPath.row < books.count {
            let book = books[indexPath.row]
            let categoryCell = tableView.dequeueReusableCell(withIdentifier: CategoryTableViewCell.cellIdentifier, for: indexPath) as! CategoryTableViewCell
            categoryCell.nameLabel.text = book.title!
            categoryCell.artistLabel?.text = nil
            categoryCell.uploaderLabel?.text = "Uploader: \(book.uploader!)"
            categoryCell.ratingLabel?.text = "Rating: \(book.rating)"
            var tagString = String()
            if var tags = book.tags {
                if tags.count >= 10 {
                    tags = Array(tags[0..<10])
                }
                for tag in tags {
                    if tag == book.tags?.last {
                        tagString += ("\(tag)")
                    } else {
                        tagString += ("\(tag), ")
                    }
                }
                if book.tags?.count >= 10 {
                    // Indicate there're more tags in the details page.
                    tagString += "..."
                }
            }
            categoryCell.tagsLabel.text = tagString
            categoryCell.categoryLabel.text = Configuration.singleton.debug ? book.category?.rawValue : ""
            categoryCell.categoryLabel.textColor = book.category?.colour()
            categoryCell.fileCountLabel?.text = "\(book.fileCount) file(s)"
            categoryCell.dateLabel.text = CategoryTableViewController.postDateFormatter.string(from: book.posted! as Date)
            if (book.thumb != nil) {
                categoryCell.previewImageView.sd_setImage(with: book.thumb! as URL, placeholderImage: nil)
            } else {
                categoryCell.previewImageView.image = nil
            }
            
            cell = categoryCell
        } else {
            cell = UITableViewCell()
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedIndexPath = indexPath
        if shouldPerformSegue(withIdentifier: showBookDetailSegueIdentifier, sender: tableView) {
            performSegue(withIdentifier: showBookDetailSegueIdentifier, sender: tableView)
        }
    }
}

// MARK: UISearchBarDelegate
extension CategoryTableViewController {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        if let pickedKeyword = searchBar.text, !pickedKeyword.isEmpty {
            searchWithKeyword(pickedKeyword)
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchMode = false
        tableView.reloadData()
    }
}

// MARK: LeftCategoryPickerTableViewController
extension CategoryTableViewController {
    
    func categoryPicker(_ picker: LeftCategoryPickerTableViewController, pickedCategory: Category) {
        viewDeckController.closeLeftView()
        searchMode = false // No longer searching
        DLog("Category picked: \(pickedCategory.rawValue)")
        source.category = pickedCategory
        title = source.category.rawValue
        refreshSource()
        navigationController?.popToViewController(self, animated: true)
        // Scroll back to top.
        tableView.scrollRectToVisible(CGRect(x: 0, y: 0, width: 10, height: 10), animated: true)
    }
}

// MARK: Life cycle.
extension CategoryTableViewController {

    func enableLeftMenu() {
        DLog("")
        let leftMenuViewController = UIStoryboard.init(name: "LeftMenu", bundle: Bundle.main).instantiateInitialViewController()
        if let leftMenuViewController = leftMenuViewController as? LeftCategoryPickerTableViewController {
            leftMenuViewController.delegate = self
            viewDeckController.centerhiddenInteractivity = .notUserInteractiveWithTapToClose
            viewDeckController.leftSize = UIScreen.main.bounds.width * 0.5
            viewDeckController.leftController = leftMenuViewController
            navigationItem.leftBarButtonItem = leftBarButtonItem
        }
    }
    
    func disableLeftMenu() {
        DLog("")
        viewDeckController.leftController = nil
        navigationItem.leftBarButtonItem = nil
    }
}

// MARK: Ad handlers.
extension CategoryTableViewController: GADBannerViewDelegate {
    func adViewWillLeaveApplication(_ bannerView: GADBannerView!) {
        // User clicked on the ad.
        AdConfiguration.singleton.clickedAd()
    }
    
    func adViewDidReceiveAd(_ bannerView: GADBannerView!) {
        // Make it visible again.
        toggleAdBanner(AdConfiguration.singleton.shouldDisplayAds)
    }
    
    func toggleAdBanner(_ show: Bool) {
        if show {
            tableHeaderView?.frame.size.height = 94
            adBannerView?.isHidden = false
            adDescriptionLabel.text = AdConfiguration.singleton.adDescription
            adDescriptionLabel.isHidden = false
            adDescriptionLabel.layoutIfNeeded()
            tableHeaderView?.frame.size.height += adDescriptionLabel.frame.height
        } else {
            tableHeaderView?.frame.size.height = 44
            adBannerView?.isHidden = true
            adDescriptionLabel.text = ""
            adDescriptionLabel.isHidden = true
        }
    }
    
    func attemptLoadRequest() {
        if AdConfiguration.singleton.shouldDisplayAds {
            let request = GADRequest()
            #if DEBUG
                request.testDevices = [kGADSimulatorID]
            #endif
            adBannerView.load(request)
        }
    }
    
}

extension CategoryTableViewController: MNMBottomPullToRefreshManagerClient {
    
    func bottomPull(toRefreshTriggered manager: MNMBottomPullToRefreshManager) {
        loadMore()
    }
    
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        bottomPullManager.tableViewScrolled()
    }
    
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate:Bool) {
        bottomPullManager.tableViewReleased()
    }

}
