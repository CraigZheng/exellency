//
//  FavouriteBookCollectionViewController.swift
//  Exellency
//
//  Created by Craig Zheng on 8/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

private let reuseIdentifier = "bookCellIdentifier"

class FavouriteBookCollectionViewController: UICollectionViewController {

    fileprivate let favouriteBooks = {
        return Array(FavouriteManager.singleton.favouriteBooks)
    }()
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.reloadData()
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let sender = sender as? UICollectionViewCell {
            if let indexPath = collectionView?.indexPath(for: sender),
                let book = favouriteBooks[indexPath.row] as? Book,
                let destinationViewController = segue.destination as? BookDetailViewController {
                destinationViewController.book = book
            }
        }
    }
}

// MARK: UIActions.
extension FavouriteBookCollectionViewController {
    @IBAction func favouriteButtonAction(_ sender: UIButton) {
        if let collectionViewCell = sender.superCollectionViewCell() as? FavouriteBookCollectionViewCell,
            let book = collectionViewCell.book
        {
            if FavouriteManager.singleton.isBookInFavourite(book) {
                FavouriteManager.singleton.removeBookFromFavourite(book)
            } else {
                FavouriteManager.singleton.addBookToFavourite(book)
            }
            collectionView?.reloadData()
        }
    }
}

// MARK: UICollectionViewDataSource
extension FavouriteBookCollectionViewController {

    override func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }


    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return favouriteBooks.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        if let cell = cell as? FavouriteBookCollectionViewCell,
            let book = favouriteBooks[indexPath.row] as? Book
        {
            cell.book = book
        }
        return cell
    }
    
}
