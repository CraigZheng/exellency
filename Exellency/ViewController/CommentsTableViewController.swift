//
//  CommentsTableViewController.swift
//  Exellency
//
//  Created by Craig on 10/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class CommentsTableViewController: UITableViewController {
    var book: Book!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Configure the tableview
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 44
        // Manipulate the book object.
        if book.comments.count <= 0 {
            showLoading()
            book.updateComments({ (success, error) -> Void in
                self.tableView.reloadData()
                self.hideLoading()
            })
        } else {
            tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.book.comments.count + 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row < self.book.comments.count {
            let cell = tableView.dequeueReusableCell(withIdentifier: "commentCellIdentifier", for: indexPath)
            let comment = book.comments[indexPath.row]
            let contentLabel = cell.viewWithTag(1) as! UILabel
            let posterLabel = cell.viewWithTag(2) as! UILabel
            // Assign properties.
            contentLabel.text = comment.commentContent
            posterLabel.text = comment.poster
            return cell
        } else {
            return tableView.dequeueReusableCell(withIdentifier: "noMoreCommentCellIdentifier", for: indexPath)
        }
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == .Delete {
            // Delete the row from the data source
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
        } else if editingStyle == .Insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
