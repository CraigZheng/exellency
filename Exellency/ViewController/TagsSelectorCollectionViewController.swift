//
//  TagsSelectorCollectionViewController.swift
//  Exellency
//
//  Created by Craig on 10/9/17.
//  Copyright © 2017 cz. All rights reserved.
//

import UIKit

protocol TagsSelectorCollectionViewControllerDelegate {
    
    func dismissedViewController(_ viewController: TagsSelectorCollectionViewController, selected: [String])
    
}

class TagsSelectorCollectionViewController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UISearchBarDelegate {
    
    enum Selection {
        case single, multiple
    }
    
    var selection = Selection.multiple
    var tags: [String] = []
    var enabledTags: [String] = []
    var delegate: TagsSelectorCollectionViewControllerDelegate?
    fileprivate lazy var filteredTags: [String] = {
        return self.tags
    }()
    fileprivate var tagsFilter: String? {
        didSet {
            if let tagsFilter = tagsFilter, !tagsFilter.isEmpty {
                filteredTags = tags.filter { return $0.contains(tagsFilter.lowercased()) }
            } else {
                filteredTags = tags
            }
            collectionView?.reloadData()
        }
    }
    
    private enum TagColor {
        static let standard = UIColor.hex("007AFF")
        static let highlight = UIColor.blue
    }

    private enum CellIdentifier: String {
        case tagCell
    }
    
    private enum ReusuableViewIdentifier: String {
        case applyButtons
    }
    
    private var resetButton: UIButton?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        (collectionView?.collectionViewLayout as? UICollectionViewFlowLayout)?.sectionHeadersPinToVisibleBounds = true
        if selection == .single {
            (collectionView?.collectionViewLayout as? UICollectionViewFlowLayout)?.headerReferenceSize = CGSize.zero
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        collectionView?.setNeedsLayout()
        collectionView?.layoutIfNeeded()
        preferredContentSize = collectionView?.contentSize ?? CGSize.zero
    }

    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filteredTags.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CellIdentifier.tagCell.rawValue, for: indexPath)
        if let tagLabel = cell.contentView.viewWithTag(1) as? UILabel {
            tagLabel.text = filteredTags[indexPath.row]
            tagLabel.backgroundColor = enabledTags.contains(filteredTags[indexPath.row]) ? TagColor.highlight : TagColor.standard
        }
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let reusableView = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                            withReuseIdentifier: ReusuableViewIdentifier.applyButtons.rawValue,
                                                                            for: indexPath)
        if let reusableView = reusableView as? TagsCollectionApplyButtonsReusableView {
            resetButton = reusableView.resetButton
            resetButton?.isHidden = enabledTags.count == 0
            reusableView.searchBar.delegate = self
        }
        return reusableView
    }
    
    // MARK: UICollectionViewDelegateFlowLayout
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height: CGFloat = 30
        let width = filteredTags[indexPath.row].width(withConstraintedHeight: height, font: UIFont.systemFont(ofSize: 15))
        return CGSize(width: width, height: height)
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedTag = filteredTags[indexPath.row]
        if selection == .single {
            dismiss(animated: true, completion: { 
                self.delegate?.dismissedViewController(self, selected: [selectedTag])
            })
        } else {
            enabledTags.contains(selectedTag) ? enabledTags.removeObject(selectedTag) : enabledTags.append(selectedTag)
            resetButton?.isHidden = enabledTags.count == 0
            collectionView.reloadData()
        }
    }
    
    // MARK: IBActions
    
    @IBAction func tappedApplyButton(_ sender: UIButton) {
        dismiss(animated: true) { 
            self.delegate?.dismissedViewController(self, selected: self.enabledTags)
        }
    }
    
    @IBAction func tappedResetButton(_ sender: UIButton) {
        enabledTags.removeAll()
        collectionView?.reloadData()
    }
    
}

extension TagsSelectorCollectionViewController {
    
    // MARK: UITextFieldDelegate
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.text = nil
        tagsFilter = nil
        searchBar.resignFirstResponder()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        tagsFilter = searchBar.text
        searchBar.resignFirstResponder()
    }

}
