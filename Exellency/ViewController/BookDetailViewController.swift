//
//  BookDetailViewController.swift
//  Exellency
//
//  Created by Craig Zheng on 21/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class BookDetailViewController: UIViewController, BookDownloaderManagerDelegate, TagsSelectorCollectionViewControllerDelegate {
    
    fileprivate enum SegueIdentifier: String {
        case toolbox = "pushToolboxSegue"
        case report = "pushReportBook"
        case tags = "tags"
    }
    
    @IBOutlet weak var previewCollectionView: UICollectionView!
    @IBOutlet weak var downloadBarButtonItem: DownloadBarButtonItem?
    @IBOutlet weak var favouriteBarButtonItem: UIBarButtonItem?
    @IBOutlet weak var detailToolbar: UIToolbar!
    
    var confirmDownloadAlertView: UIAlertView?
    let refreshControl = UIRefreshControl()
    var currentPage = 1
    let imageViewerUtil = czzImageViewerUtil()
    var book: Book! {
        didSet {
            toggleFavourite()
        }
    }
    var pages: [Page] {
        get {
            return book.pages
        }
        set {
            book.pages = newValue
        }
    }
    var bookDownloader: BookDownloaderManager {
        let bookDownloader = BookDownloaderManager.singleton
        bookDownloader.delegate = self
        return bookDownloader
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register Nib.
        self.previewCollectionView.register(UINib(nibName: "BookPageCollectionViewCell", bundle: Bundle.main),
                                            forCellWithReuseIdentifier: BookPageCollectionViewCell.identifier)
        previewCollectionView.register(UINib(nibName:"BookDetailsHeaderView", bundle: nil),
                                       forSupplementaryViewOfKind: UICollectionElementKindSectionHeader,
                                       withReuseIdentifier: String(describing: BookDetailsHeaderView.self))
        if let collectionViewFlow = previewCollectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            collectionViewFlow.headerReferenceSize = CGSize(width: view.frame.size.width, height: 190)
        }
        // Load pages when the book is not completed.
        if book.pages.count >= book.fileCount {
            previewCollectionView.reloadData()
        } else {
            showLoading()
            // Before updating, reset pages.
            self.book.pages = [Page]()
            self.book.updatePages({ [weak weakSelf = self](success, current, error) in
                guard weakSelf != nil else { return }
                // Progress update but not complete, reload the previewCollectionView.
                DLog("Progression update.")
                weakSelf?.previewCollectionView.reloadData()
                weakSelf?.imageViewerUtil.notifyBookUpdated(weakSelf?.book)
            }) { [weak weakSelf = self] (success, error) in
                // After pages are downloaded, reload the data.
                weakSelf?.previewCollectionView?.reloadData()
                weakSelf?.refreshControl.endRefreshing()
                weakSelf?.toggleDownloading()
                weakSelf?.hideLoading()
            }
        }
        
        // UI adjustments.
        self.title = book.title
    }
    
    deinit {
        book.stopUpdatingPages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        toggleDownloading()
        toggleFavourite()
    }
    
    // MARK: Segue
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? CommentsTableViewController {
            destinationViewController.book = book
        } else if segue.identifier == SegueIdentifier.report.rawValue,
            let destinationViewController = segue.destination as? ReportBookViewController
        {
            destinationViewController.book = book
        } else if let destinationViewController = segue.destination as? TagsSelectorCollectionViewController {
            destinationViewController.popoverPresentationController?.delegate = self
            destinationViewController.tags = book.tags ?? []
            destinationViewController.delegate = self
            destinationViewController.selection = .single
        }
    }
    
}

// MARK: UICollectionViewDelegate and UICollectionViewDataSource
extension BookDetailViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.pages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        imageViewerUtil.show(self.book, with: indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let collectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: BookPageCollectionViewCell.identifier,
                                                                                          for: indexPath) as? BookPageCollectionViewCell {
            let page = self.pages[indexPath.row]
            
            collectionViewCell.previewImageView.image = page.thumbnail
            collectionViewCell.titleLabel.text = page.thumbnailURL?.lastPathComponent
            return collectionViewCell
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                   withReuseIdentifier: String(describing: BookDetailsHeaderView.self),
                                                                   for: indexPath)
        if let view = view as? BookDetailsHeaderView {
            // Got a book, update UI elements.
            view.nameLabel?.text = book.title!
            view.uploaderLabel?.text = "Uploader: \(book.uploader!)"
            view.ratingLabel?.text = "Rating: \(book.rating)"
            view.tagsLabel?.text = book.tags?.joined(separator: ", ")
            view.categoryLabel?.text = Configuration.singleton.debug ? book.category?.rawValue : ""
            view.categoryLabel?.textColor = book.category?.colour()
            view.fileCountLabel?.text = "\(book.fileCount) file(s)"
            view.dateLabel?.text = CategoryTableViewController.postDateFormatter.string(from: book.posted!)
            
            if let thumbURL = book.thumb {
                view.previewImageView?.sd_setImage(with: thumbURL as URL!)
            } else {
                view.previewImageView?.image = nil
            }
            // The rating stars
            let stars = [view.star1ImageView,
                         view.star2ImageView,
                         view.star3ImageView,
                         view.star4ImageView,
                         view.star5ImageView]
            for star in stars {
                star?.isHidden = true
            }
            // For each rating, give it a star
            let fullStar = UIImage(named: "star_filled.png")
            let halfStar = UIImage(named: "star_half_filled.png")
            for i in 0 ..< Int(book.rating) {
                // The last half star
                if i == Int(book.rating) {
                    // If the decimal part of the rating is bigger than 0.3.
                    let decimal = book.rating - Double(Int(book.rating))
                    if decimal > 0.3 {
                        stars[i]?.image = halfStar
                        stars[i]?.isHidden = false
                    }
                    // If bigger than 0.8, then give it a full star
                    if decimal >= 0.8 {
                        stars[i]?.image = fullStar
                        stars[i]?.isHidden = false
                    }
                } else {
                    // The first few stars
                    stars[i]?.image = fullStar
                    stars[i]?.isHidden = false
                }
            }
        }
        return view
    }
    
}

// MARK: UI actions.
extension BookDetailViewController: UIAlertViewDelegate {
    
    @IBAction func downloadAction(_ sender: AnyObject) {
        // Attempt to download the given book - is its neither downloading or downloaded.
        if !BookDownloaderManager.isBookDownloading(book) &&
            !BookCacheManager.isBookDownloaded(book) {
            if book.pages.count <= 0 {
                // Loading not complete, don't proceed to download.
                UIAlertView(title: "", message: "This book is still being loaded, please wait momentarily",
                            delegate: nil,
                            cancelButtonTitle: "OK").show()
            } else {
                confirmDownloadAlertView = UIAlertView(title: "",
                                                       message: "\(UIApplication.appName()!) will start downloading this book",
                                                       delegate: self,
                                                       cancelButtonTitle: "CANCEL",
                                                       otherButtonTitles: "CONFIRM")
                confirmDownloadAlertView?.show()
            }
        } else {
            // The book is already in the downloading or already downloaded, show toolbox view instead.
            performSegue(withIdentifier: SegueIdentifier.toolbox.rawValue, sender: nil)
        }
    }
    
    @IBAction func favouriteAction(_ sender: AnyObject) {
        // If the book is already in favourite, remove it, else add it to favourite.
        if FavouriteManager.singleton.isBookInFavourite(book) {
            FavouriteManager.singleton.removeBookFromFavourite(book)
        } else {
            FavouriteManager.singleton.addBookToFavourite(book)
        }
        toggleFavourite()
    }
    
    @IBAction func tappedTagsCollection(_ sender: UIButton) {
        performSegue(withIdentifier: SegueIdentifier.tags.rawValue, sender: nil)
    }
    
    // MARK: UIAlertViewDelegate
    func alertView(_ alertView: UIAlertView, didDismissWithButtonIndex buttonIndex: Int) {
        if buttonIndex != alertView.cancelButtonIndex {
            if alertView == confirmDownloadAlertView {
                if book == nil {
                    DLog("error")
                }
                if bookDownloader.downloadBook(book) {
                    // Download has begin.
                } else {
                    // Download has not begin, inform error.
                }
                toggleDownloading()
            }
        }
    }
}

// MARK: BookDownloaderManagerDelegate and util methods.
extension BookDetailViewController {
    func bookDownloaderManager(_ manager: BookDownloaderManager, bookCompleted: Book, success: Bool, error: NSError?) {
        // What to do?
        DLog("Book download completed!")
        toggleDownloading()
    }
    
    func bookDownloaderManager(_ manager: BookDownloaderManager, bookUpdated: Book, percentage: Double) {
        // What to do?
        DLog("Book download updated!")
    }
    
    // In this method, if BookDownloaderManager singleturn returns true, would replace the download bar button with loading bar button.
    func toggleDownloading() {
        var downloadButton: UIBarButtonItem?
        if BookDownloaderManager.isBookDownloading(book) {
            downloadButton = (DownloadBarButtonItem.loadingButtonItem())
        } else if BookCacheManager.isBookDownloaded(book) {
            downloadButton = (DownloadBarButtonItem.loadedButtonItem())
        } else {
            downloadButton = (DownloadBarButtonItem.standbyButtonItem())
        }
        // Assign the button to the toolbar.
        if let downloadButton = downloadButton {
            // Assign action, then assign this button to the toolbar.
            downloadButton.target = self
            downloadButton.action = #selector(BookDetailViewController.downloadAction(_:))
            var buttons = detailToolbar.items
            buttons?.removeLast()
            buttons?.append(downloadButton)
            detailToolbar.setItems(buttons, animated: true)
        }
    }
    
    // Toggle the icon for favourite bar item button.
    func toggleFavourite() {
        if FavouriteManager.singleton.isBookInFavourite(book) {
            favouriteBarButtonItem?.image = UIImage(named: "star_filled.png")
        } else {
            favouriteBarButtonItem?.image = UIImage(named: "star_outlined.png")
        }
    }
}

// MARK: TagsSelectorCollectionViewControllerDelegate

extension BookDetailViewController: UIPopoverPresentationControllerDelegate {
    
    func dismissedViewController(_ viewController: TagsSelectorCollectionViewController, selected: [String]) {
        guard !selected.isEmpty,
            let categoryTableViewController = navigationController?.viewControllers.first as? CategoryTableViewController
            else { return }
        categoryTableViewController.predefinedSearchKeyword = selected.first
        navigationController?.popToRootViewController(animated: true)
    }
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}
