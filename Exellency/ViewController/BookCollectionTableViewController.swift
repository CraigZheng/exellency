//
//  BookCollectionTableViewController.swift
//  Exellency
//
//  Created by Craig on 10/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

enum BookCollection: Int {
    case favourite = 0
    case downloading = 1
    case downloaded = 2
}

class BookCollectionTableViewController: UITableViewController {
    
    @IBOutlet weak var searchTagsButton: UIBarButtonItem!
    
    var selectedBook: Book?
    var selectedCollection = BookCollection.favourite {
        didSet {
            // Set self.title.
            switch selectedCollection {
            case .favourite:
                title = "Favourite"
            case .downloaded:
                title = "Downloaded"
            case .downloading:
                title = "Downloading"
            }
        }
    }
    var enabledTags: [String] = []
    
    // MARK: Private properties.
    fileprivate let showBookDetailIdentifier = "showBookDetailIdentifier"
    fileprivate let imageViewer = czzImageViewerUtil()
    fileprivate var sourceBooks: [Book] {
        // Filter out books without enabled tags.
        if enabledTags.isEmpty {
            return unfilteredSourceBooks
        } else {
            return unfilteredSourceBooks.filter({ book -> Bool in
                guard let tags = book.tags else { return false }
                let tagsSet = Set(tags)
                // If the union has the same number of items as the original set, then there is no additional tags added.
                return tagsSet.count == tagsSet.union(enabledTags).count
            })
        }
    }
    fileprivate var unfilteredSourceBooks: [Book] {
        // Return book array depends on the current selectedCollection.
        let sourceBooks: [Book]
        switch selectedCollection {
        case .downloading:
            sourceBooks = downloadingBooks
        case .downloaded:
            sourceBooks = downloadedBooks
        case .favourite:
            sourceBooks = favouriteBooks
        }
        return sourceBooks
    }
    fileprivate var downloadedBooks: Array<Book> {
        get {
            if _downloadedBooks == nil {
                if let bookArray = BookCacheManager.sharedInstance.downloadedBooks.array as? [Book] {
                    _downloadedBooks = bookArray
                }
            }
            return _downloadedBooks ?? []
        }
        set {
            _downloadedBooks = newValue
        }
    }
    fileprivate var downloadingBooks: Array<Book> {
        get {
            if _downloadingBooks == nil {
                let downloadingBooks = Array(BookDownloaderManager.singleton.downloadingBooks)
                let pendingDownloads = PendingDownloadManager.singleton.pendingDownloads
                if var partiallyDownloadedBooks = BookCacheManager.partiallyDownloadedBooks().array as? [Book] {
                    partiallyDownloadedBooks = partiallyDownloadedBooks.filter({ book -> Bool in
                        // Not being included in both Downloading and Pending managers.
                        return !BookDownloaderManager.isBookDownloading(book) && !PendingDownloadManager.singleton.pendingDownloads.contains(book)
                    })
                    _downloadingBooks = Array(downloadingBooks + pendingDownloads + partiallyDownloadedBooks)
                } else {
                    _downloadingBooks = Array(downloadingBooks + pendingDownloads)
                }
            }
            return _downloadingBooks ?? []
        }
        set {
            _downloadingBooks = newValue
        }
    }
    fileprivate var favouriteBooks: Array<Book> {
        get {
            if _favouriteBooks == nil, let books = FavouriteManager.singleton.favouriteBooks.array as? [Book] {
                _favouriteBooks = books
            }
            return _favouriteBooks ?? []
        }
        set {
            _favouriteBooks = newValue
        }
    }
    fileprivate var _downloadedBooks: Array<Book>?
    fileprivate var _downloadingBooks: Array<Book>?
    fileprivate var _favouriteBooks: Array<Book>?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Register nib files.
        tableView.register(UINib.init(nibName: "ToolboxDownloadingCell", bundle: Bundle.main),
                              forCellReuseIdentifier: ToolboxTableViewCell.downloadingCellIdentifier)
        tableView.register(UINib.init(nibName: "ToolboxFavouriteCell", bundle: Bundle.main),
                              forCellReuseIdentifier: ToolboxTableViewCell.favouriteCellIdentifier)
        tableView.register(UINib.init(nibName: "ToolboxDownloadedCell", bundle: Bundle.main),
                              forCellReuseIdentifier: ToolboxTableViewCell.downloadedCellIdentifier)
        // Register BookDownloaderManagerNotifications.
        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(BookCollectionTableViewController.handleBookDownloaderManagerUpdatedNotification(_:)),
                                                         name: NSNotification.Name(rawValue: BookDownloaderManagerNotification.UpdatedNotification),
                                                         object: nil)
        NotificationCenter.default.addObserver(self,
                                                         selector: #selector(BookCollectionTableViewController.handlerBookDownloaderManagerCompletedNotification(_:)),
                                                         name: NSNotification.Name(rawValue: BookDownloaderManagerNotification.CompletedNotification),
                                                         object: nil)
        // FIXME: A bug if presenting the edit bar button item to function correctly.
        // Must fix.
        if selectedCollection == BookCollection.favourite {
            navigationItem.rightBarButtonItems = []
        }
        searchTagsButton.isEnabled = selectedCollection == .downloaded
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetSources()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? BookDetailViewController,
            let selectedBook = selectedBook {
            destinationViewController.book = selectedBook
        } else if let destinationViewController = segue.destination as? TagsSelectorCollectionViewController {
            let tags = unfilteredSourceBooks.flatMap{ $0.tags }.flatMap {$0}
            destinationViewController.tags = Set(tags).flatMap{ $0 }.sorted()
            destinationViewController.enabledTags = enabledTags
            destinationViewController.delegate = self
            destinationViewController.popoverPresentationController?.delegate = self
        }
    }
}

// MARK: UI actions.

extension BookCollectionTableViewController {
    
    @IBAction func editButtonAction(_ sender: AnyObject) {
        tableView.isEditing = !tableView.isEditing
    }
    
    @IBAction func startReadingAction(_ sender: UIButton) {
        if let cell = sender.superCell() as? ToolboxTableViewCell {
            let selectedBook = cell.book
            imageViewer.show(selectedBook, with: 0)
        }
    }
    
    @IBAction func restartDownloadingAction(_ sender: UIButton) {
        if let cell = sender.superCell() as? ToolboxTableViewCell {
            if !BookDownloaderManager.isBookDownloading(cell.book) && !BookCacheManager.isBookDownloaded(cell.book) {
                // Neither downloading nor downloaded, restart the download.
                BookDownloaderManager.singleton.downloadBook(cell.book)
                resetSources()
            }
        }
    }
    
    @IBAction func stopDownloadingAction(_ sender: UIButton) {
        if let cell = sender.superCell() as? ToolboxTableViewCell {
            if BookDownloaderManager.isBookDownloading(cell.book) {
                BookDownloaderManager.singleton.stopDownloadingBook(cell.book)
            }
            PendingDownloadManager.singleton.removePendingBook(cell.book)
            BookDownloaderManager.singleton.startNext()
            resetSources()
        }
    }
    
    func resetSources() {
        // Reset tableview data source.
        _favouriteBooks = nil
        _downloadingBooks = nil
        _downloadedBooks = nil
        tableView.reloadData()
    }
}

// MARK: UITableViewDataSource + UITableViewDelegate
extension BookCollectionTableViewController {
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sourceBooks.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = UITableViewCell()
        let book = sourceBooks[indexPath.row]
        switch selectedCollection {
        case .favourite:
            if let cell = tableView.dequeueReusableCell(withIdentifier: ToolboxTableViewCell.favouriteCellIdentifier,
                                                                      for: indexPath) as? ToolboxTableViewCell {
                cell.book = book
            }
        case .downloading:
            if let downloadingCell = tableView.dequeueReusableCell(withIdentifier: ToolboxTableViewCell.downloadingCellIdentifier,
                                                                                 for: indexPath) as? ToolboxDownloadingCell {
                downloadingCell.book = book
                // How many files have been downloaded?
                if let completedFiles = BookCache.contentsOfDownloadedFolderForBook(downloadingCell.book, shouldSort: false) {
                    let nonImageFiles = completedFiles.filter {
                        return $0.path.hasSuffix("json") || $0.path.hasSuffix("txt")
                    }
                    downloadingCell.progressLabel.text = "\(completedFiles.count - nonImageFiles.count) / \(downloadingCell.book.fileCount)"
                    downloadingCell.progressView.progress = Float((completedFiles.count - nonImageFiles.count)) / Float(downloadingCell.book.fileCount)
                } else {
                    downloadingCell.progressView.progress = 0.5
                }
                // Cell status.
                if BookDownloaderManager.singleton.downloadingBooks.contains(downloadingCell.book) {
                    if BookDownloaderManager.singleton.downloadingTasks.flatMap ({
                        return $0.status == DownloaderStatus.updating ? $0.downloadingBook : nil
                    }).contains(downloadingCell.book) {
                        downloadingCell.downloadStatus = .updating
                        downloadingCell.progressLabel.text = "Updating"
                    } else {
                        downloadingCell.downloadStatus = .downloading
                    }
                } else if PendingDownloadManager.singleton.pendingDownloads.contains(book) {
                    downloadingCell.downloadStatus = .pending
                    downloadingCell.progressLabel.text = "Pending"
                } else {
                    downloadingCell.downloadStatus = .stopped
                }
                cell = downloadingCell
            }
        case .downloaded:
            if let downloadedCell = tableView.dequeueReusableCell(withIdentifier: ToolboxTableViewCell.downloadedCellIdentifier,
                                                                                for: indexPath) as? ToolboxDownloadedCell {
                downloadedCell.book = book
                cell = downloadedCell
            }
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = 120
        if selectedCollection == .favourite {
            height = 100
        }
        return CGFloat(height)
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedBook = sourceBooks[indexPath.row]
        performSegue(withIdentifier: showBookDetailIdentifier,
                                   sender: tableView)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        if editingStyle == UITableViewCellEditingStyle.delete, let cell = cell as? ToolboxTableViewCell {
            switch selectedCollection {
            case .favourite:
                FavouriteManager.singleton.removeBookFromFavourite(cell.book)
            case .downloaded:
                BookCacheManager.removeCacheFolderForBook(cell.book)
            case .downloading:
                if BookDownloaderManager.isBookDownloading(cell.book) {
                    BookDownloaderManager.singleton.stopDownloadingBook(cell.book)
                }
                // Remove the cache folder.
                BookCacheManager.removeCacheFolderForBook(cell.book)
            }
            BookCacheManager.reset()
            resetSources()
        }
    }
    
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
}

// MARK: Handlers for BookDownloaderManager notification.
extension BookCollectionTableViewController {
    func handleBookDownloaderManagerUpdatedNotification(_ notification: Notification) {
        resetSources()
    }
    
    func handlerBookDownloaderManagerCompletedNotification(_ notification: Notification) {
        resetSources()
    }
}

// MARK: UIPopoverPresentationControllerDelegate

extension BookCollectionTableViewController: UIPopoverPresentationControllerDelegate {
    
    func adaptivePresentationStyle(for controller: UIPresentationController) -> UIModalPresentationStyle {
        return .none
    }
    
}

// MARK: TagsSelectorCollectionViewControllerProtocol

extension BookCollectionTableViewController: TagsSelectorCollectionViewControllerDelegate {
    
    func dismissedViewController(_ viewController: TagsSelectorCollectionViewController, selected: [String]) {
        enabledTags = selected
        tableView.reloadData()
    }
    
}
