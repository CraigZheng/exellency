//
//  ReportBookViewController.swift
//  Exellency
//
//  Created by Craig Zheng on 3/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import UIKit

class ReportBookViewController: UIViewController {
    
    var book: Book!

    @IBOutlet weak var bookTitleLabel: UILabel!
    @IBOutlet weak var reasonsTableView: UITableView!
    @IBOutlet weak var reportButton: RoundCornerBorderedButton!

    // MARK: Private properties.
    fileprivate let selectionCellIdentifier = "cellIdentifier"
    fileprivate let textfieldCellIdentifier = "textFieldCellIdentifier"
    fileprivate let localReasons = ["Advertisement", "Copyright content", "Drug reference", "Nudity and porn", "Racist", "Religious", "Violent", "War"]
    fileprivate let remoteReasonsURL = URL(string: "http://civ.atwebpages.com/exheti/exheti_remote_blacklist_reasons.php")!
    fileprivate var selectedReasons = [String]()
    fileprivate var otherReasons: String?
    fileprivate var remoteReasons: [String]?
    fileprivate var reasons: [String] {
        // If remoteReasons is available.
        if let remoteReasons = remoteReasons {
            return remoteReasons
        } else {
            return localReasons
        }
    }
    fileprivate let sessionManager = AFHTTPSessionManager.sessionManager()

    // MARK: Life cycle.
    override func viewDidLoad() {
        super.viewDidLoad()
        bookTitleLabel.text = book.title
        reasonsTableView.rowHeight = UITableViewAutomaticDimension
        reasonsTableView.estimatedRowHeight = 44
        NotificationCenter.default.addObserver(self, selector: #selector(ReportBookViewController.handleKeyboardAppearNotification(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ReportBookViewController.handleKeyboardDisappearNotification(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        // Update remoteReasons array.
        showLoading()
        reportButton.isEnabled = false
        sessionManager.dataTask(with: EXConfigureURLRequest(url: remoteReasonsURL) as URLRequest, completionHandler: { (response, responseObject, error) in
            if let jsonData = responseObject as? Data,
                let jsonArray = try? JSONSerialization.jsonObject(with: jsonData, options: .mutableContainers) as? [String]
            {
                self.remoteReasons = jsonArray
            }
            self.hideLoading()
            self.reportButton.isEnabled = true
            self.reasonsTableView.reloadData()
        }).resume()
    }

    // MARK: UI actions.
    @IBAction func reportAction(_ sender: AnyObject) {
        if selectedReasons.isEmpty && otherReasons?.isEmpty ?? true {
            UIAlertView(title: "No Reason?",
                        message: "Please select at least one reason!",
                        delegate: nil,
                        cancelButtonTitle: "OK").show()
        } else {
            var reasons = selectedReasons
            if let otherReasons = otherReasons, !otherReasons.isEmpty {
                reasons.append(otherReasons)
            }
            let senderButton = sender as? UIControl
            senderButton?.isEnabled = false
            showLoading()
            BlacklistManager.singleton.reportBook(book, reasons: reasons) { (success, error) in
                senderButton?.isEnabled = true
                self.hideLoading()
                if success {
                    self.navigationController?.popToRootViewController(animated: true)
                }
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
                    let message = success ? "We've received and will evaluate your report, thanks!" : "Failed to report, please check your network."
                    ProgressHUD.showMessage(message)
                    BlacklistManager.singleton.updateBlacklist(nil)
                })
            }
        }
    }
    
}

// MARK: UITableViewDataSource, UITableViewDelegate
extension ReportBookViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return reasons.count
        } else {
            return 1
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: UITableViewCell
        if indexPath.section == 0 {
            cell = tableView.dequeueReusableCell(withIdentifier: selectionCellIdentifier, for: indexPath)
            cell.textLabel?.text = reasons[indexPath.row]
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: textfieldCellIdentifier, for: indexPath)
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let reason = reasons[indexPath.row]
            selectedReasons.append(reason)
            tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let reason = reasons[indexPath.row]
            selectedReasons.removeObject(reason)
            tableView.cellForRow(at: indexPath)?.accessoryType = .none
        }
    }
    
}

// MARK: Keyboard notification handlers.
extension ReportBookViewController {
    
    func handleKeyboardAppearNotification(_ notification: Notification) {
        // Avoid keyboard overlapping with the textfield cell.
        if let userInfo = notification.userInfo,
            let keyboardFrameValue = userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue
            {
                // Minus the height for the reportButton and its top, bottom paddings.
                reasonsTableView.contentInset = UIEdgeInsetsMake(0, 0, keyboardFrameValue.cgRectValue.height - 76, 0)
        }
    }
    
    func handleKeyboardDisappearNotification(_ notification: Notification) {
        reasonsTableView.contentInset = UIEdgeInsets.zero
    }
    
}

// MARK: UITextFieldDelegate
extension ReportBookViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        // Record other reasons.
        otherReasons = textField.text
        return false
    }
}
