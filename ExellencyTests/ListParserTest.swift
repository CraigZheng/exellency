//
//  ListParserTest.swift
//  Exellency
//
//  Created by Craig Zheng on 17/02/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

@testable import Exellency

class ListParserTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testBooksWithURL() {
        let expection = self.expectation(description: "")
        ListParser.booksWithKeyword("chinese", page: 1) { (success, books, error) -> Void in
            XCTAssert(success)
            XCTAssert(books?.count == 25)
            expection.fulfill()
        }
        self.waitForExpectations(timeout: 20, handler: nil)
    }
    
    func testBookWithURL() {
        let expection = self.expectation(description: "")
        ListParser.bookWithURL(URL(string: "http://g.e-hentai.org/g/893415/a8c3907d31/")!) { (success, book, error) -> Void in
            if let newBook = book {
                XCTAssert(success)
                XCTAssert(newBook.gid == 893415);
                XCTAssert(newBook.posted!.timeIntervalSince1970 == 1452581377);
                XCTAssert(!newBook.expunged);
                XCTAssert(newBook.fileCount == 39);
                XCTAssert(newBook.fileSize == 133794779);
                XCTAssert(newBook.rating > 0);
                XCTAssert(newBook.tags!.count == 14);
            } else {
                XCTAssert(false)
            }

            expection.fulfill()
        }
        self.waitForExpectations(timeout: 20, handler: nil)
    }
    
    /*
    
    - (void)testBookWithURL {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Full filled"];
    [ListParser bookWithURL:[NSURL URLWithString:@"http://g.e-hentai.org/g/893415/a8c3907d31/"]
    completionHandler:^(BOOL success, Book *_Nullable newBook, NSError *_Nullable error) {
    XCTAssert(success);
    XCTAssert(newBook.gid == 893415);
    XCTAssert([[newBook posted] timeIntervalSince1970] == 1452581377);
    XCTAssert(!newBook.expunged);
    XCTAssert(newBook.fileCount == 39);
    XCTAssert(newBook.fileSize == 133794779);
    XCTAssert(newBook.rating > 0);
    XCTAssert(newBook.tags.count == 14);
    
    [expectation fulfill];
    }];
    [self waitForExpectationsWithTimeout:20 handler:nil];
    }
*/

}
