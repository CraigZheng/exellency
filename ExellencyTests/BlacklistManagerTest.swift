//
//  BlacklistManagerTest.swift
//  Exellency
//
//  Created by Craig Zheng on 28/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

class BlacklistManagerTest: XCTestCase {
    var expectation: XCTestExpectation?
    
    let dummyBookJson: String = {
        let jsonString = try? String(contentsOfFile: NSBundle.init(forClass: BlacklistEntityTest.self).pathForResource("MetaDataSample-1", ofType: "json")!)
        return jsonString!
    }()

    func testReportBook() {
        let book = Book(jsonString: dummyBookJson)
        BlacklistManager.singleton.reportBook(book, reasons: ["chinese", "translated", "love plus", "multi-work series"]) { (success, error) in
            XCTAssert(success)
            self.expectation?.fulfill()
        }
        
        expectation = self.expectationWithDescription("")
        self.waitForExpectationsWithTimeout(20, handler: nil)
    }
    
    func testUpdateBlacklist() {
        BlacklistManager.singleton.updateBlacklist { (success, error) in
            XCTAssert(success)
            XCTAssert(error == nil)
            BlacklistManager.singleton.blacklistEntities.count > 0
            var reasonsPassed = false
            BlacklistManager.singleton.blacklistEntities.forEach({ (entity) in
                if !entity.reasons.isEmpty {
                    reasonsPassed = true
                }
            })
            XCTAssert(reasonsPassed)
            self.expectation?.fulfill()
        }
        
        expectation = self.expectationWithDescription("")
        self.waitForExpectationsWithTimeout(20, handler: nil)
        
    }
    
}
