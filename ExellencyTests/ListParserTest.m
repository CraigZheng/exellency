//
//  ListParserTest.m
//  Exellency
//
//  Created by Craig Zheng on 8/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "Exellency-Swift.h"

@interface ListParserTest : XCTestCase
@property (nonatomic, readonly) NSString *listHtmlString;
@end

@implementation ListParserTest

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBookWithURL {
    XCTestExpectation *expectation = [self expectationWithDescription:@"Full filled"];
    [ListParser bookWithURL:[NSURL URLWithString:@"http://g.e-hentai.org/g/893415/a8c3907d31/"]
          completionHandler:^(BOOL success, Book *_Nullable newBook, NSError *_Nullable error) {
              XCTAssert(success);
              XCTAssert(newBook.gid == 893415);
              XCTAssert([[newBook posted] timeIntervalSince1970] == 1452581377);
              XCTAssert(!newBook.expunged);
              XCTAssert(newBook.fileCount == 39);
              XCTAssert(newBook.fileSize == 133794779);
              XCTAssert(newBook.rating > 0);
              XCTAssert(newBook.tags.count == 14);

              [expectation fulfill];
          }];
    [self waitForExpectationsWithTimeout:20 handler:nil];
}

- (void)testParsingHtml {
    XCTAssert([ListParser parseHtml:self.listHtmlString].count == 55);
}


- (void)testParsingPerformance {
    [self measureBlock:^{
        [ListParser parseHtml:self.listHtmlString];
    }];
}

#pragma mark - Accessors 

- (NSString *)listHtmlString {
    NSError *error;
    NSString *filePath = [[NSBundle bundleForClass:[self class]] pathForResource:@"List-1"
                                                                          ofType:@"html"];
    NSString *htmlString = [NSString stringWithContentsOfFile:filePath
                                                     encoding:NSUTF8StringEncoding
                                                        error:&error];
    if (error) {
        NSLog(@"%@", error);
    }
    return htmlString;
}

@end
