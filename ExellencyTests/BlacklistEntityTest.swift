//
//  BlacklistEntityTest.swift
//  Exellency
//
//  Created by Craig Zheng on 27/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

class BlacklistEntityTest: XCTestCase {
    
    let dummyJson: String = {
        let jsonString = try? String(contentsOfFile: NSBundle.init(forClass: BlacklistEntityTest.self).pathForResource("BlacklistEntity", ofType: "json")!)
        return jsonString!
    }()
    
    let dummyBookJson: String = {
        let jsonString = try? String(contentsOfFile: NSBundle.init(forClass: BlacklistEntityTest.self).pathForResource("MetaDataSample-1", ofType: "json")!)
        return jsonString!
    }()

    func testInitWithJson() {
        let entity = BlacklistEntity(jsonString: dummyJson)
        XCTAssert(entity?.name == "(C89) [Shinjugai (Takeda Hiromitsu)] Mana Tama Plus 3 (Love Plus) [Chinese] [空気系☆漢化]")
        XCTAssert(entity?.gid == 890273)
        XCTAssert(entity?.sourceURL?.absoluteString == "http://g.e-hentai.org/g/932906/b4873b4193")
        XCTAssert(entity?.reasons.count == 4)
    }
    
    func testInitWithBook() {
        let book = Book(jsonString: dummyBookJson)
        let entity = BlacklistEntity(book: book)
        XCTAssert(entity?.name == "(C89) [Shinjugai (Takeda Hiromitsu)] Mana Tama Plus 3 (Love Plus) [Chinese] [空気系☆漢化]")
        XCTAssert(entity?.gid == 890273)
        XCTAssert(entity?.sourceURL?.absoluteString == "http://g.e-hentai.org/g/890273/8079b34da7")
    }
}
