//
//  AdConfigurationTest.swift
//  Exellency
//
//  Created by Craig Zheng on 7/08/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

class AdConfigurationTest: XCTestCase {
    var expectation: XCTestExpectation?
    
    func testUpdate() {
        AdConfiguration.singleton.updateAdConfiguration {
            XCTAssert(AdConfiguration.singleton.enableAd)
            XCTAssert(AdConfiguration.singleton.dailyAdClickLimit != 3)
            XCTAssert(AdConfiguration.singleton.weeklyAdClickLimit != 9)
            XCTAssert(AdConfiguration.singleton.monthlyAdClickLimit != 18)
            XCTAssert(AdConfiguration.singleton.adDescription != nil)
            self.expectation?.fulfill()
        }
        expectation = self.expectation(description: "")
        waitForExpectations(timeout: 20, handler: nil)
    }
    
    func testHistory() {
        let dates = AdConfiguration.singleton.historyOfClicks
        AdConfiguration.singleton.clickedAd()
        let newDates = AdConfiguration.singleton.historyOfClicks
        XCTAssert(newDates.count > dates.count)
        // Test restoring function.
        let newAdConfiguration = AdConfiguration.init()
        XCTAssert(newAdConfiguration.historyOfClicks.count == newDates.count)
    }
    
    func testShouldDisplayAd() {
        for _ in 0 ..< AdConfiguration.singleton.dailyAdClickLimit {
            AdConfiguration.singleton.clickedAd()
        }
        XCTAssert(!AdConfiguration.singleton.shouldDisplayAds)
        
    }
    
}
