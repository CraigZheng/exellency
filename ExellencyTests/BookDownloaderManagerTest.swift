//
//  BookDownloaderManagerTest.swift
//  Exellency
//
//  Created by Craig on 18/03/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

class BookDownloaderManagerTest: XCTestCase, BookDownloaderManagerDelegate {
    let downloaderManager = BookDownloaderManager.singleton
    var expectation: XCTestExpectation?
    var expectedDownloadResult: Bool!
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        downloaderManager.delegate = self
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDownloadBook() {
        expectation = self.expectation(description: "")
        expectedDownloadResult = true
        if let url = URL(string: "https://e-hentai.org/g/932673/601c895149/") {
            ListParser.bookWithURL(url) { (success, book, error) -> Void in
                XCTAssert(success)
                if success, let book = book {
                    book.updatePages({ (success, error) -> Void in
                        XCTAssert(book.pages.count > 0)
                        self.downloaderManager.downloadBook(book)
                    })
                }
            }
        }
        
        self.waitForExpectations(timeout: 600, handler: nil)
    }
    
    func testStopUpdatingPages() {
        expectation = self.expectation(description: "")
        expectedDownloadResult = false
        if let url = URL(string: "http://g.e-hentai.org/g/932906/b4873b4193/") {
            ListParser.bookWithURL(url, completionHandler: { (success, book, error) in
                XCTAssert(success)
                if success, let book = book {
                    self.downloaderManager.downloadBook(book)
                    DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * NSEC_PER_SEC)) / Double(NSEC_PER_SEC), execute: {
                        XCTAssert(self.downloaderManager.stopDownloadingBook(book))
                    })
                }
            })
        }
        self.waitForExpectations(timeout: 600, handler: nil)
    }
    
    // MARK: BookDownloaderDelegate
    func bookDownloaderManager(_ manager: BookDownloaderManager, bookCompleted: Book, success: Bool, error: NSError?) {
        XCTAssert(success == expectedDownloadResult)
        XCTAssert(bookCompleted.title == "[Takano Itsuki] Ika Manga 2 (Splatoon) [English]"
            || bookCompleted.title == "(Nanoha Festival) [D-Cube (Misril)] Fate in Stay Night (Mahou Shoujo Lyrical Nanoha) [Spanish] [Yuri Hen Trads]", bookCompleted.title!)
        // Is book downloaded?
        XCTAssert(BookCacheManager.isBookDownloaded(bookCompleted) == expectedDownloadResult)
        expectation?.fulfill()
    }
    
    func bookDownloaderManager(_ manager: BookDownloaderManager, bookUpdated: Book, percentage: Double) {
        // Nothing, just to suppress the warnings.
        DLog("Downloader manager updated: \(percentage)")
        XCTAssert(percentage != 0)
        XCTAssert(bookUpdated.title == "[Takano Itsuki] Ika Manga 2 (Splatoon) [English]"
            || bookUpdated.title == "(Nanoha Festival) [D-Cube (Misril)] Fate in Stay Night (Mahou Shoujo Lyrical Nanoha) [Spanish] [Yuri Hen Trads]", bookUpdated.title!)
        XCTAssert(BookDownloaderManager.isBookDownloading(bookUpdated))
    }
}
