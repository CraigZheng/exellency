//
//  FavouriteManagerTest.swift
//  Exellency
//
//  Created by Craig on 4/04/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

class FavouriteManagerTest: XCTestCase {
    lazy var dummyBook: Book = {
        if let jsonString = try? String.init(contentsOf: Bundle(for: type(of: self)).url(forResource: "MetaDataSample-1", withExtension: "json")!,
                                               encoding: String.Encoding.utf8) {
            let book = Book(jsonString: jsonString)
            return book
        }
        return Book(jsonString: "")
    }()
    lazy var dummyBook2: Book = {
        if let jsonString = try? String.init(contentsOf: Bundle(for: type(of: self)).url(forResource: "MetaDataSample-2", withExtension: "json")!,
                                             encoding: String.Encoding.utf8) {
            let book = Book(jsonString: jsonString)
            return book
        }
        return Book(jsonString: "")
    }()

    
    override func setUp() {
        super.setUp()
        // Always add 2 books to the singleton favourite manager.
        FavouriteManager.singleton.addBookToFavourite(dummyBook)
        FavouriteManager.singleton.addBookToFavourite(dummyBook2)
        XCTAssert(FavouriteManager.singleton.favouriteBooks.count >= 2)
        XCTAssert(FavouriteManager.singleton.isBookInFavourite(dummyBook))
        XCTAssert(FavouriteManager.singleton.isBookInFavourite(dummyBook2))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testFavouriteFolderURL() {
        if let folderURL = FavouriteManager.singleton.favouriteFolderURL {
            let fileManager = FileManager.default
            XCTAssert(fileManager.fileExists(atPath: folderURL.path))
        }
    }
    
    func testSaveAndRestore() {
        // Save the current favourite books to disk.
        let fileManager = FileManager.default
        FavouriteManager.singleton.save()
        do {
            let count = try fileManager.contentsOfDirectory(atPath: FavouriteManager.singleton.favouriteFolderURL!.path).count
            XCTAssert(count >= 2)
        } catch {
            DLog(error)
        }
        // Test restore by initialing a new FavouriteManager object.
        let tempFavouriteManager = FavouriteManager()
        // It should have the same content as the singleton manager.
        XCTAssert(tempFavouriteManager.favouriteBooks.count == FavouriteManager.singleton.favouriteBooks.count,
                  "\(tempFavouriteManager.favouriteBooks.count) != \(FavouriteManager.singleton.favouriteBooks.count)")
    }
    
    func testRemoveFavourite() {
        let count = FavouriteManager.singleton.favouriteBooks.count
        FavouriteManager.singleton.removeBookFromFavourite(dummyBook)
        FavouriteManager.singleton.removeBookFromFavourite(dummyBook2)
        if count > 0 {
            XCTAssert(FavouriteManager.singleton.favouriteBooks.count == count - 2)
        } else {
            XCTAssert(false, "Count is equal to 0")
        }
    }
}
