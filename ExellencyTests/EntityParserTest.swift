//
//  EntityParserTest.swift
//  Exellency
//
//  Created by Craig Zheng on 12/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

@testable import Exellency
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}

// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func > <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l > r
  default:
    return rhs < lhs
  }
}


class EntityParserTest: XCTestCase {
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testMetaData() {
        let expectation = self.expectation(description: "Full filled")
        EntityParser.metaDataWithURLs([URL(string: "http://g.e-hentai.org/g/890273/8079b34da7/")!]) { (success, htmlString, error) -> Void in
            XCTAssert(success)
            XCTAssert(htmlString?.lengthOfBytes(using: String.Encoding.utf8) > 1000)
            XCTAssert(error == nil)
            expectation.fulfill()
        }

        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPageLinks() {
        let expectation = self.expectation(description: "Full filled")
        EntityParser.pagesWithURL(URL(string: "http://g.e-hentai.org/g/893415/a8c3907d31/")) { (success, pages, error) -> Void in
            XCTAssert(success)
            XCTAssert(pages?.count == 39)
            for page in pages! {
                XCTAssert(page.pageURL != nil)
            }
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testPageLinksWithHtml() {
        let htmlString = try! String(contentsOfFile: Bundle(for: EntityParserTest.self).path(forResource: "Entity-1", ofType: "html")!, encoding: String.Encoding.utf8)
        let pageLinks = EntityParser.pageLinksWithHtml(htmlString)
        XCTAssert(pageLinks.count == 23)
        for page in pageLinks {
            XCTAssertTrue((page.pageURL?.absoluteString as String!).contains("http://g.e-hentai.org/s/"), (page.pageURL?.absoluteString)!)
            XCTAssertTrue(page.thumbnailURL != nil)
        }
    }
    
    func testFullSizeImageWithURL() {
        let expectation = self.expectation(description: "Full filled")
        EntityParser.imageWithURL(URL(string: "http://g.e-hentai.org/s/de2bf73e57/893415-6")) { (success, pages, error) -> Void in
            XCTAssert(success)
            XCTAssert(pages?.count == 1)
            if pages != nil {
                for page in pages! {
                    XCTAssert(page.imageURL != nil)
                }
            }
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testFullSizeImagesWithHtml() {
        let htmlString = try! String(contentsOfFile: Bundle(for: EntityParserTest.self).path(forResource: "Page-1", ofType: "html")!, encoding: String.Encoding.utf8)
        let pages = EntityParser.imagesWithHtml(htmlString)
        XCTAssert(pages.count == 1)
        for page in pages {
            XCTAssert(page.imageURL != nil)
        }
    }
    
    func testThumbnailsWithHtml() {
        let htmlString = try! String(contentsOfFile: Bundle(for: EntityParserTest.self).path(forResource: "Entity-1", ofType: "html")!, encoding: String.Encoding.utf8)
        let links = EntityParser.thumbnailsWithHtml(htmlString)
        XCTAssert(links.count >= 1)
    }
    
    func testCommentsWithURL() {
        let url = URL(string: "http://g.e-hentai.org/g/893415/a8c3907d31/")
        let expectation = self.expectation(description: "")
        EntityParser.commentsWithURL(url!) { (success, comments, error) -> Void in
            XCTAssert(success)
            XCTAssert(comments!.count > 0)
            DLog(error)
            
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 10, handler: nil)
    }
    
    func testCommentsWithHtml() {
        let htmlString = try! String(contentsOfFile: Bundle(for: EntityParserTest.self).path(forResource: "Comments-1", ofType: "html")!, encoding: String.Encoding.utf8)
        let comments = EntityParser.commentsWithHtml(htmlString)
        XCTAssert(comments.count > 0)
    }
    
    func testWarningWithHtml() {
        let warningURLString = "http://g.e-hentai.org/g/950395/b2f9815621/"
        let expectation = self.expectation(description: "")
        EntityParser.pagesWithURL(URL(string: warningURLString)!) { (success, pages, error) in
            XCTAssert(success)
            XCTAssert(pages?.count > 0)
            expectation.fulfill()
        }
        waitForExpectations(timeout: 30, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            self.testPageLinksWithHtml()
        }
    }
    
}
