//
//  PendingDownloadManagerTest.swift
//  Exellency
//
//  Created by Craig on 21/07/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

class PendingDownloadManagerTest: XCTestCase {
    
    lazy var dummyBook: Book = {
        if let jsonString = try? String.init(contentsOf: Bundle(for: type(of: self)).url(forResource: "MetaDataSample-1", withExtension: "json")!,
                                             encoding: String.Encoding.utf8) {
            let book = Book(jsonString: jsonString)
            return book
        }
        return Book(jsonString: "")
    }()

    lazy var dummyBook2: Book = {
        if let jsonString = try? String.init(contentsOf: Bundle(for: type(of: self)).url(forResource: "MetaDataSample-2", withExtension: "json")!,
                                             encoding: String.Encoding.utf8) {
            let book = Book(jsonString: jsonString)
            return book
        }
        return Book(jsonString: "")
    }()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAddBook() {
        XCTAssert(dummyBook.isEqual(dummyBook))
        
        PendingDownloadManager.singleton.pendingDownloads.removeAll()
        PendingDownloadManager.singleton.addBookToPending(dummyBook)
        PendingDownloadManager.singleton.addBookToPending(dummyBook2)
        PendingDownloadManager.singleton.addBookToPending(dummyBook)
        XCTAssert(PendingDownloadManager.singleton.pendingDownloads.count == 2, "\(PendingDownloadManager.singleton.pendingDownloads.count) books.")
        
        PendingDownloadManager.singleton.removePendingBook(dummyBook)
        XCTAssert(PendingDownloadManager.singleton.pendingDownloads.count == 1, "\(PendingDownloadManager.singleton.pendingDownloads.count) books.")
    }
    
    func testPopBook() {
        PendingDownloadManager.singleton.pendingDownloads.removeAll()
        PendingDownloadManager.singleton.addBookToPending(dummyBook)
        PendingDownloadManager.singleton.addBookToPending(dummyBook2)
        XCTAssert(PendingDownloadManager.singleton.pendingDownloads.count == 2, "\(PendingDownloadManager.singleton.pendingDownloads.count) books.")
        let _ = PendingDownloadManager.singleton.nextBook()
        XCTAssert(PendingDownloadManager.singleton.pendingDownloads.count == 1, "\(PendingDownloadManager.singleton.pendingDownloads.count) books.")
        let _ = PendingDownloadManager.singleton.nextBook()
        XCTAssert(PendingDownloadManager.singleton.pendingDownloads.count == 0, "\(PendingDownloadManager.singleton.pendingDownloads.count) books.")
    }
    
    func testSaveAndRestore() {
        PendingDownloadManager.singleton.pendingDownloads.removeAll()
        PendingDownloadManager.singleton.addBookToPending(dummyBook)
        PendingDownloadManager.singleton.addBookToPending(dummyBook2)
        XCTAssert(PendingDownloadManager.singleton.pendingDownloads.count == 2)
        // The number of json files in the designated folder.
        let contents = try? FileManager.default.contentsOfDirectory(atPath: PendingDownloadManager.singleton.pendingDownloadsFolderURL!.path)
        XCTAssert(contents!.count == 2)
        // Restore with a new instance.
        let newPendingDownloadManager = PendingDownloadManager()
        let originalSet = PendingDownloadManager.singleton.pendingDownloads
        let restoredSet = newPendingDownloadManager.pendingDownloads
        XCTAssert(originalSet == restoredSet)
    }
    
}
