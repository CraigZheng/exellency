//
//  ConfigurationTest.swift
//  Exellency
//
//  Created by Craig Zheng on 30/06/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

class ConfigurationTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDefaultInit() {
        let configuration = Configuration.singleton
        XCTAssert(configuration.entityListURL?.contains("http://g.e-hentai.org/") ?? false)
        XCTAssert(configuration.entityDetailsURL?.contains("http://g.e-hentai.org/g/") ?? false)
        XCTAssert(configuration.entityPageURL?.contains("http://g.e-hentai.org/s/") ?? false)
        XCTAssert(configuration.searchIndexURL == "http://g.e-hentai.org/?f_doujinshi=0&f_manga=0&f_artistcg=0&f_gamecg=0&f_western=0&f_non-h=1&f_imageset=0&f_cosplay=0&f_asianporn=0&f_misc=0&f_search=[KEYWORD]&f_apply=Apply+Filter")
        XCTAssert(configuration.debugSearchIndexURL == "http://g.e-hentai.org/?f_doujinshi=1&f_manga=1&f_artistcg=1&f_gamecg=1&f_western=1&f_non-h=1&f_imageset=1&f_cosplay=1&f_asianporn=1&f_misc=1&f_search=[KEYWORD]&f_apply=Apply+Filter")
        XCTAssert(configuration.debugSearchURL == "http://g.e-hentai.org/?page=[PAGE]&f_doujinshi=on&f_manga=on&f_artistcg=on&f_gamecg=on&f_western=on&f_non-h=on&f_imageset=on&f_cosplay=on&f_asianporn=on&f_misc=on&f_search=[KEYWORD]&f_apply=Apply+Filter")
        XCTAssert(!configuration.searchURL!.isEmpty)
        XCTAssert(configuration.metaDataURL == "http://g.e-hentai.org/api.php")
        XCTAssert(configuration.timeout == 20)
        XCTAssert(configuration.imagePerPage == 40)
        XCTAssert(configuration.thumbnailWidth == 50)
        XCTAssert(configuration.imageKeyID == "keystamp")
        XCTAssert(configuration.remoteDebug == false)
        XCTAssert(configuration.allowedCategory.count > 0)
        XCTAssert(configuration.warningKeyword == "Offensive For Everyone")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
