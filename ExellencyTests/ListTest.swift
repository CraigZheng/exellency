//
//  ListTest.swift
//  Exellency
//
//  Created by Craig Zheng on 21/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

@testable import Exellency

class ListTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testNonHCategory() {
        let expectation = self.expectation(description: "")
        
        let list = List()
        list.updateWithCategory(Category.nonh, page: 1) { (success, error) -> Void in
            XCTAssert(success)
            XCTAssert(list.category == Category.nonh)
            XCTAssert(list.books.count > 0)
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 20, handler: nil)
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
}
