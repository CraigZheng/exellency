//
//  BookCacheManagerTest.swift
//  Exellency
//
//  Created by Craig Zheng on 9/05/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

class BookCacheManagerTest: XCTestCase, BookDownloaderManagerDelegate {
    var expectation: XCTestExpectation?
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testGetPartiallyCompletedBook() {
        XCTAssert(BookCacheManager.partiallyDownloadedBooks().count > 0)
    }
    
    func testFolders() {
        let fileManager = FileManager.default
        // Favourite folder.
        XCTAssert(fileManager.fileExists(atPath: FavouriteManager.favouriteFolderURL!.path))
        // Downloaded books folder.
        expectation = self.expectation(description: "")
        if let url = URL(string: "http://g.e-hentai.org/g/932906/b4873b4193/") {
            ListParser.bookWithURL(url, completionHandler: { (success, book, error) in
                XCTAssert(success)
                if success, let book = book {
                    BookDownloaderManager.singleton.delegate = self
                    BookDownloaderManager.singleton.downloadBook(book)
                }
            })
        }
        self.waitForExpectations(timeout: 20, handler: nil)
    }
    
    func bookDownloaderManager(_ manager: BookDownloaderManager, bookUpdated: Book, percentage: Double) {
        let fileManager = FileManager.default
        // Test if the folder has been created successfully.
        XCTAssert(fileManager.fileExists(atPath: BookCache.downloadedFolderForBook(bookUpdated)!.path))
        // Test if the exclude from backup key has been set to true.
        var resource: AnyObject?
        _ = try? (BookCache.downloadedBooksFolderURL! as NSURL).getResourceValue(&resource, forKey: URLResourceKey.isExcludedFromBackupKey)
        XCTAssert(resource as? Bool ?? false)
        self.expectation?.fulfill()
    }
    
    func bookDownloaderManager(_ manager: BookDownloaderManager, bookCompleted: Book, success: Bool, error: NSError?) {
        // Do nothing.
    }
}
