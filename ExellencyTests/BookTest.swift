//
//  BookTest.swift
//  Exellency
//
//  Created by Craig Zheng on 19/01/2016.
//  Copyright © 2016 cz. All rights reserved.
//

import XCTest

@testable import Exellency

class BookTest: XCTestCase {
    let jsonDictString1: String = {
        let jsonString = try! String(contentsOfFile: Bundle(for: EntityParserTest.self).path(forResource: "MetaDataSample-1", ofType: "json")!, encoding: String.Encoding.utf8)
        let jsonDict = (try! JSONSerialization.jsonObject(with: jsonString.data(using: String.Encoding.utf8)!, options: .allowFragments) as! [String: AnyObject])["gmetadata"]?.firstObject
        let jsonDictString = String(data: try! JSONSerialization.data(withJSONObject: jsonDict!!, options: .prettyPrinted), encoding: String.Encoding.utf8)
        return jsonDictString!
    }()
    let jsonDictString2: String = {
        let jsonString = try! String(contentsOfFile: Bundle(for: EntityParserTest.self).path(forResource: "MetaDataSample-2", ofType: "json")!, encoding: String.Encoding.utf8)
        let jsonDict = (try! JSONSerialization.jsonObject(with: jsonString.data(using: String.Encoding.utf8)!, options: .allowFragments) as! [String: AnyObject])["gmetadata"]?.firstObject
        let jsonDictString = String(data: try! JSONSerialization.data(withJSONObject: jsonDict!!, options: .prettyPrinted), encoding: String.Encoding.utf8)
        return jsonDictString!
    }()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testInitWithJson1() {
        let newBook = Book(jsonString: jsonDictString1)
        XCTAssert(newBook.gid == 890273)
        XCTAssert(newBook.token == "8079b34da7")
        XCTAssert(newBook.archiverKey == "403500--117f8285e71b280cdb6b7e25c6c73cd0a318baf6")
        XCTAssert(newBook.title == "(C89) [Shinjugai (Takeda Hiromitsu)] Mana Tama Plus 3 (Love Plus) [Chinese] [空気系☆漢化]")
        XCTAssert(newBook.titleJpn == "(C89) [真珠貝 (武田弘光)] マナタマプラス3 (ラブプラス) [中国翻訳]")
        XCTAssert(newBook.category?.rawValue == "Doujinshi".lowercased())
        XCTAssert(newBook.category == Category.doujinshi)
        XCTAssert(newBook.thumb != nil)
        XCTAssert(newBook.uploader == "NEET☆遥")
        XCTAssert(newBook.posted?.timeIntervalSince1970 == 1451882882)
        XCTAssert(!newBook.expunged)
        XCTAssert(newBook.rating == 4.92)
        XCTAssert(newBook.tags?.count == 23)
    }
    
    func testInitWithJson2() {
        let newBook = Book(jsonString: jsonDictString2)
        XCTAssert(newBook.gid == 893415)
        XCTAssert(newBook.token == "a8c3907d31")
        XCTAssert(newBook.archiverKey == "403646--0dc9f039f2181efce928a52497f5f68f03ab9111")
        XCTAssert(newBook.title == "(C85) [Circle Nuruma-ya (Tsukiwani)] Ganbare no Otome-tachi | The Cheering Maidens (Touhou Project) [English] {Onichan}")
        XCTAssert(newBook.titleJpn == "(C85) [サークルぬるま屋 (月わに)] ガンバレの乙女たち♪ (東方Project) [英訳]")
        XCTAssert(newBook.category?.rawValue == "Non-H".lowercased())
        XCTAssert(newBook.category == Category.nonh)
        XCTAssert(newBook.thumb != nil)
        XCTAssert(newBook.uploader == "mathillean")
        XCTAssert(newBook.posted?.timeIntervalSince1970 == 1452581377)
        XCTAssert(!newBook.expunged)
        XCTAssert(newBook.fileCount == 39)
        XCTAssert(newBook.fileSize == 133794779)
        XCTAssert(newBook.rating == 4.59)
        XCTAssert(newBook.tags?.count == 14)
        let url = newBook.sourceURL
        XCTAssert(url?.absoluteString == "http://g.e-hentai.org/g/893415/a8c3907d31")
    }
    
    func testGetPages() {
        let expectation = self.expectation(description: "Full filled")
        let book = Book(jsonString: jsonDictString1)
        book.updatePages { (success, error) -> Void in
            XCTAssert(success)
            XCTAssert(book.pages.count == 57)
            expectation.fulfill()
        }

        self.waitForExpectations(timeout: 60, handler: nil)
    }
    
    func testGetImages() {
        let expectation = self.expectation(description: "Full filled")
        
        let newBook = Book(jsonString: jsonDictString2)
        newBook.updatePages { (success, error) -> Void in
            XCTAssert(success)
            XCTAssert(newBook.pages.count == 39)
            
            let firstPage = newBook.pages.first
            newBook.updateImageForPage(firstPage, completionHandler: { (success, error) -> Void in
                XCTAssert(firstPage?.imageURL != nil)
                expectation.fulfill()
            })
        }
        self.waitForExpectations(timeout: 20, handler: nil)
    }
    
    func testGetThumbnails() {
        let expectation = self.expectation(description: "Full filled")
        
        let newBook = Book(jsonString: jsonDictString2)
        newBook.updatePages { (success, error) -> Void in
            XCTAssert(success)
            for page in newBook.pages {
                XCTAssertTrue(page.thumbnail != nil)
                XCTAssertTrue(page.thumbnail!.isKind(of: UIImage.self))
            }
            expectation.fulfill()
        }
        self.waitForExpectations(timeout: 10, handler: nil)
    }
}
